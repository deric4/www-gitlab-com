---
layout: job_family_page
title: Revenue Accounting
---

The Revenue Accounting job family is responsible for the timely recognition of revenue, allocation of revenue, billing, and collection.

## Revenue Accountant

The Revenue Accountant role will support all day-to-day revenue accounting activities and month-end financial reporting requirements while ensuring compliance with revenue recognition policies, procedures and internal controls. The Revenue Accountant will report to the Director of Revenue and help develop best practices and process improvements as the business grows. 

### Job Grade 

The Revenue Accountant is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades). 

### Responsibilities

- Review and summarize customer contracts for proper revenue recognition in accordance with company policy and revenue recognition accounting standards
- Research and document accounting issues and recommend solutions for complex transactions as needed
- Key contributor in the monthly financial close, ensuring accurate and timely recording of transactions and the completeness of financial statements
- Work closely and help manage transactions with the billing and deal desk teams
- Prepare monthly account reconciliations and journal entries and contribute to the financial reporting process
- Ensure compliance with internals controls and processes 
- Analysis of standalone selling price 
- Work on ad hoc projects including implementation of new systems and standards
- Experience with ASC 606 and multi-element revenue recognition

### Requirements

- Strong working knowledge of US GAAP principles and financial statements, including ASC 606 and multi-element experience
- 2+ years of related accounting experience, public company accounting experience is a plus
- Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results
- Experience with Netsuite, Zuora or Revpro considered an advantage
- Proficient with excel and google sheets
- International experience preferred.
- Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
- You share our values, and work in accordance with those values.
- Successful completion of a background check
- Ability to use GitLab

## Senior Revenue Accountant

The Senior Revenue Accountant role will take a leading role in all day-to-day revenue accounting activities and month-end financial reporting requirements while ensuring compliance with revenue recognition policies, procedures and internal controls. The Senior Revenue Accountant will report to the Director of Revenue and help develop best practices and process improvements as the business grows. 

### Job Grade 

The Senior Revenue Accountant is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades). 

### Responsibilities

- Review and summarize customer contracts for proper revenue recognition in accordance with company policy and revenue recognition accounting standards
- Research and document accounting issues and recommend solutions for complex transactions as needed
- Key contributor in the monthly financial close, ensuring accurate and timely recording of transactions and the completeness of financial statements
- Work closely and help manage transactions with the billing and deal desk teams
- Prepare monthly account reconciliations and journal entries and contribute to the financial reporting process
- Ensure compliance with internals controls and processes 
- Analysis of standalone selling price 
- Work on ad hoc projects including implementation of new systems and standards
- Experience with ASC 606 and multi-element revenue recognition

### Requirements

- Strong working knowledge of US GAAP principles and financial statements, including ASC 606 and multi-element experience
- 3-5 years of related accounting experience, public company accounting experience is a plus
- Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results
- Experience with Netsuite, Zuora or Revpro considered an advantage
- Proficient with excel and google sheets
- International experience preferred.
- Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
- You share our values, and work in accordance with those values.
- Successful completion of a background check
- Ability to use GitLab

## Manager, Revenue Accounting

The Manager, Revenue Accounting is responsible for all day-to-day revenue accounting operations and month-end financial reporting requirements while ensuring compliance with revenue recognition policies, procedures and internal controls.
The Revenue Manager reports to the Director of Revenue and helps drive process improvement projects as GitLab continues to grow.

### Job Grade

The Manager, Revenue Accounting is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades). 

### Responsibilities

- Review and summarize customer contracts for proper revenue recognition in accordance with company policy and revenue recognition accounting standards
- Partner with Legal department and business teams to interpret complex or unique contract provisions or new revenue initiatives
- Research and document accounting issues and recommend solutions for complex transactions as needed
- Key contributor in the monthly financial close, ensuring accurate and timely recording of transactions and the completeness of financial statements
- Work closely and help manage transactions with the billing and deal desk teams
- Prepare monthly account reconciliations, entries and contribute to the financial reporting process
- Develop and document processes and controls, and identify opportunities for streamlining processes and increasing automation to improve the speed and efficiency of the monthly revenue close
- Support external and internal audit requirements
- Work as liaison with project teams to develop or improve finance solutions, including the design, implementation and review of systems
- Assist with and manage various ad hoc projects as needed

### Requirements

- 2+ years experience in revenue accounting with a Big 4 accounting firm or SaaS company
- Experience in applying revenue accounting guidance (ASC 606).  Strong knowledge of US GAAP, Sarbanes-Oxley, risk and controls standards and business process best practices skills
- Experience managing critical projects such as system implementations, ERP system enhancements, and system upgrades
- Strong understanding of process and operations in a fast-paced business environment
- Ability to work dynamically with a variety of internal business partners (financial and non-financial personnel both within the US and abroad)
- Be a self-starter, have a positive attitude, maintain a solution-oriented work ethic
- Zuora, RevPro, Netsuite system knowledge
- Ability to use GitLab

### Performance Indicators

- [Average days to close](/handbook/finance/accounting/#month-end-review--close)
- [Number of material audit adjustments](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)
- [Percentage of ineffective Sox Controls](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)

## Director, Revenue Accounting

The Director, Revenue Accounting is responsible for ensuring the integrity of world-wide revenue by providing leadership, technical expertise, business sense, and a strong desire to improve processes and controls.
They have an in-depth knowledge of accounting/GAAP including expertise in ASC Topic 606, and manage the billing and commission groups.
Strong organization and communication skills are necessary as well as the ability to be detail-oriented and hands-on.
The Director of Revenue reports to the Director of Revenue.

### Job Grade

The Director, Revenue Accounting is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Guide, mentor and manage the world-wide revenue, billing and commission teams
* Matrix management capabilities to communicate with and educate finance and non-finance personnel on revenue accounting guidance, Gitlab specific policies and procedures as well as inputs on structuring of customer arrangements
* Review customer arrangements for proper revenue recognition in accordance with company policy and revenue recognition accounting standards
* Ensure the integrity of the world-wide license, support, service and training revenue and assist in new product offerings that may be developed over time
* Review license and service revenue related balance sheet account reconciliations
* Be a global partner with subject matter expertise in revenue and work diligently on finding solutions to critical issues
* Develop and maintain strong collaborative working relationships across Customer Service, Legal, Sales, FP&A, Accounting and Tax teams
* Research and review technical revenue accounting memos that addresses the issue, and provide analysis and conclusions related to various customer contracts or new technical revenue pronouncements
* Ensure the integrity of key processes by understanding systems, flow of transactions, internal controls, and recommending efficiency and effectiveness improvements
* Stay current on new revenue accounting rules and help develop appropriate policies and processes
* Provide revenue training to other groups within GitLab
* Liaison with External auditors, present significant item updates, supporting documentation for the quarterly reviews and year-end audits
* Lead the ongoing revenue automation process, and work with IT to prioritize further potential system enhancements and development projects related to revenue

### Requirements

* CPA is required, with at least 12 years of professional experience including significant management, and technology industry experience
* A combination of public company experience in a revenue leadership role and experience with public accounting firms
* Polished communication skills, including an ability to listen to the needs of the business units, research and comprehend complex matters, articulate issues in a clear and simplified manner, and present findings and recommendations in both oral and written presentations
* Strong ability to work well within a team structure and an ability to influence, train, mentor and leverage the skills of others to achieve objectives
* Experience with ERP systems and processes and with commission and billing systems
* Ability to use GitLab

## Career Ladder

The next step in the Revenue Accounting job family is to move to the TODO.

### Performance Indicators

* [Deals reviewed for revenue recognition = 100%](/handbook/finance/accounting/#deals-reviewed-for-revenue-recognition--100)
* [Non GAAP Revenue (Ratable Recognition)](/handbook/finance/accounting/#non-gaap-revenue-ratable-recognition)
* [Revenue recognition and accounting for other quote to cash transactions in Net Suite](/handbook/finance/accounting/#8-revenue-recognition-and-accounting-for-other-quote-to-cash-transactions-in-net-suite)

### Hiring Process

Candidates for the Revenue Accounting team can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).
- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to meet with the Director of Revenue
- Next, candidates will be invited to schedule a 45 minute interview with our Controller and one other member of the Accounting team
- Finally, candidates may be asked to interview with the Principal Accounting Officer 
- Successful candidates will subsequently be made an offer via email or phone call

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing)
