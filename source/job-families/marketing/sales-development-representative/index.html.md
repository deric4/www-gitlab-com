---
layout: job_family_page
title: "Sales Development Representative"
---
## Job Description
GitLab is looking for an enthusiastic and strategic Sales Development Representative (SDR), to join our growing Revenue Marketing team. As a SDR at GitLab, you get the opportunity to lead the initial outreach to targeted (Commercial or Enterprise) accounts working in conjunction with the Field and Digital Marketing teams.
In this role you will leverage creative marketing and sales tactics to prospect and engage with multiple buyer personas and roles to introduce GitLab’s value. You will be responsible for generating qualified meetings and pipeline for the GitLab’s Sales organization.

We have an extensive onboarding and training program at GitLab and you will be provided with necessary DevOps and GitLab knowledge to fulfill your role.


### Responsibilities
* Effectively manage inbound lead flow as well as executing outbound prospecting initiatives
* Conduct high-level discovery conversations in target accounts
* Meet or exceed SDR sourced Sales Accepted Opportunity (SAO) volume targets
* Collaborate with and leverage teammates to develop targeted lists, call strategies, and messaging to drive opportunities
* Utilize business and industry knowledge to research accounts, identify key players, generate interest, create/identify compelling events, and develop accounts
* Work to have a variety of touches (call, email, social, etc.) on all leads in your assigned territory using Outreach.io
* Manage, track, and report on all activities and results using Salesforce
* Participate in documenting all processes in the GitLab handbook and update as needed with your Sales Development Manager
* Work in collaboration with Field and Corporate Marketing to drive attendance at regional marketing events
* Attend field marketing events to engage with participants identify opportunities, and to schedule meetings
* Act as a mentor for new SDR hires in helping them navigate their key accounts
* Work in collaboration with Digital Marketing to develop targeted marketing tactics against your assigned target accounts
* We’re a diverse team and we’re looking for you to bring your own unique flavor to the SDR role!

### Requirements

* Excited by the prospect of working cross-functionally with sales and different marketing departments. You'll have exposure to different departments like Sales, Marketing, Finance, Recruiting, Enablement, Engineering, etc. which will help you determine your career path at GitLab.
* Positive and energetic phone skills, excellent listening skills, strong writing skills
* A self-starter with a track record of successful, credible achievements
* You share our values, and work in accordance with those values
* Knowledge of business process, roles, and organizational structure
* Determined personality with a desire to grow and win
* Passionate about being a part of GitLab’s journey
* Proficient in using Salesforce and LinkedIn
* 2+ years work experience in a professional environment
* Previous tech industry experience or experience in sales development, marketing and/or sales is a plus
* Outbound prospecting experience is a plus
* Globally we require excellent written and spoken English which is our company language
* If in EMEA, fluency in spoken and written German or French or other European languages will be an advantage
* If in LATAM, fluency in Portuguese and Spanish is required
* Ability to use GitLab


## Levels

### SDR 2
After attaining an SDR 2 role, you can expect an increase in your compensation and your pipeline generating targets. You will also discuss your ideal career path with your manager and set action steps to achieve it. Topics of discussion should consist of functionalities of interest and steps to be taken by both SDR and Manager to get there. For a minimum of 6 months in the SDR 2 role, you will continue improving your sales skills while also aiding in the development of these skills with other members of your teams through training and coaching. Upon reaching target goals and acquiring certification on these skills, the next step is an SDR 3 role.  All the while the SDR and Manager should have clear conversations and setting milestones around career progression at GitLab. Promotion timing is not guaranteed at the 6 month mark and SDR's should expect to have career development conversations for a 6-12 month period in role.  All promotions will be approved by the Sr. Director, Revenue Marketing in line with business objectives and budget.

#### Requirements
* 1-2 years proven SDR experience (Either at GitLab or a past role)
* Consistently hits/exceed quota
* Will present at an SDR Quarterly Business Review (QBR) or all hands
* Networked at local DevOps events (non-GitLab)
* Understands, executes and presents an Account Based Marketing (ABM) approach

#### Responsibilities
* Attends GitLab sponsored events
* Participates and owns projects benefiting the SDR organization
* Assists with campaigns - lead pulls and feedback
* Attend field marketing events to engage with participants identify opportunities, and to schedule meetings
* Work in collaboration with Digital Marketing to develop targeted marketing tactics against your assigned target accounts

### SDR 3
After attaining an SDR 3 role, you can expect an increase in compensation and a reduction in your Sales targets as the SDR 3 role is a team coach and mentor for more junior members of the team. You will receive substantial Sales training to expand your current knowledge into pricing conversations and negotiations. You will be expected to pass this knowledge along to your team and set them up for success in their individual performance. Upon reaching target goals and acquiring certification on these skills, the next career step is either an SDR Management role or a career branch into another function in GitLab. The next step should be driven by the SDR and Manager together. Promotion timing is not guaranteed at the 6 month mark and SDR's should expect to have career development conversations for a 6-12 month period in role.  All promotions will be approved by the Sr. Director, Revenue Marketing in line with business objectives and budget.

#### Requirements
* Six (6) months in an SDR 2 role at GitLab with successful quota attainment
* Demonstrates thought leadership and training that benefits the SDR team
* Demonstrates exemplary account strategy/process acumen
* GitLab leadership recommends for promotion
* Demonstrates dedication to continuous improvement by investing time to read, study, and share learnings from books, Udemy classes, Toastmasters, Certifications, Approved Sales Training, CEO Shadow Program, etc.

#### Responsibilities
* Leads territory team role play calls
* Regularly performs call coaching evaluations for SDR team members
* Helps craft Outreach sequences for events/campaigns and outbound. Conduct A/B Testing of sequences to determine effectiveness and share best practices with team.
* Assists new hires as an onboarding buddy and acts as a mentor for new SDR hires in helping them navigate their key accounts.

### Team Lead
The SDR Team Lead is a step in the career path geared towards SDRs getting exposed to what it’s like to manage a team at GitLab. You’ll need to have stellar organizational and time management skills because you will be taking point on various SDR leadership tasks such as: on-boarding new employees, managing event followup, interviewing, etc. all while continuing to exceed your quota targets. The SDR Team Lead will also be included in some management level meetings. This concept is to help foster those who have expressed interest in leading a team and progressing to management within GitLab.

#### Requirements
* 18 months of SDR experience (minimum six (6) months at GitLab)
* Proficient in the use of Command of Message (CoM) framework and SDR tools
* In-depth product knowledge
* Stellar time management and organizational skills
* Is seen as a leader/coach by peers as well as the SDR leadership team

#### Responsibilities
* Will be assigned as a Dedicated Onboarding Buddy
* Assists in interviewing process
* Leads team role play and SDR weekly team calls
* Covers when SDR's are out of office (OOO)

### SDR, Acceleration

As a member of the Sales Development Representative (SDR), Acceleration Team, you will work closely with the Field & Digital Marketing teams to accelerate prospect time-to-value. Proven outbound SDR or marketing experience is a must. You will also need to be well-versed with the DevOps conversations, GitLab value drivers, and account-based marketing (ABM) strategies. In addition to thinking creatively, you’ll need to have exceptional organizational and time management skills. Acceleration SDRs will be responsible for executing while planning and maintaining a roadmap for future campaigns.

### Responsibilities

* Works with the GitLab field, digital, and product marketing teams to create targeted regional campaigns
* Creates outbound campaign content based on the Command of Message (CoM) Framework and GitLab Value Drivers
* Be able to identify where a prospect is in the sales and marketing funnel and nurture appropriately
* Builds Account & Prospect Target Lists based on our ideal customer profile attributes
* Executes targeted campaigns in specified regions
* Schedule qualified meetings for Regional Sales and Sales Development Teams

### Requirements

* 24 months of sales, outbound SDR, or related experience
* Proficient with the CoM framework, GitLab value drivers, and SDR tools
* Understanding of Account-based (ABM), Digital, and Field Marketing concepts
* Exceptional time management and organizational skills
* “Always-on” customer focus


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the SDR Manager for the region they have applied for
* Candidates will perform a mock role play call with either the SDR Manager or Team Lead.
* Should the candidiates move forward after the mock role play, they will then be invited to schedule an interview with our Regional Sr. Manager or Director of Revenue Marketing
* Successful candidates will subsequently be made an offer

Additional details about our process can be found on our [hiring page](/handbook/hiring).
