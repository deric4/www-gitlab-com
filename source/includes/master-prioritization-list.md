| Priority | Description | Issue label(s) |
| ------ | ------ | ------ |
| 1 | <a href="/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security fixes</a> | `security` |
| 2 | Data-loss prevention | `data loss` | 
| 3 | <a href="/handbook/engineering/performance/index.html#availability">Availability</a> | `availability`  | 
| 4 | Fixing regressions (things that worked before) | `regression` |
| 5 | Promised to Customers | `planning-priority`, `customer`, `customer+` |
| 6 | <a href="/handbook/engineering/workflow/#availability-and-performance-refinement">Infra/Dev and Performance Grooming</a> | `infradev`, `performance-refinement` |
| 7 | Efficiency Initiatives | |
| 8 | IACV Drivers | |
| 9 | Identified for Dogfooding | `Dogfooding::Build in GitLab`, `Dogfooding::Rebuild in GitLab` |
| 10 | Velocity of new features, user experience improvements, technical debt, community contributions, and all other improvements | `direction`, `feature`, `enhancement`, `technical debt` |
| 11 | Behaviors that yield higher predictability (because this inevitably slows us down) | `predictability` |
