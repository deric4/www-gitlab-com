---
layout: handbook-page-toc
title: "Values Check In"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Values Check In

GitLab's [values](https://about.gitlab.com/handbook/values/) are a crucial part of our [competencies list](https://about.gitlab.com/handbook/competencies/#list) and are considered essential for team members to learn and live out, irrespective of role or team. To ensure all team members are living out and practicing GitLab's values, we have implemented a Values Check-In survey for new team members and their managers after 8-10 weeks with the company. The purpose of the check-in is to ensure alignment from a performance perspective and understanding of expectations with regard to the values.

The aim of this check-in is to ensure continuous improvment through analysis of trends and feedback, with a positive side-effect that will attribute to great retention and values alignment. 

## Process

We will take [probation periods](https://about.gitlab.com/handbook/contracts/#probation-period) into account when considering the timing of the check in. 

| ***Probation Period Length***           | ***Check In Timeline***  |
|-----------------------------------|-----------------|
| 1 month | 10 weeks* |
| 3 months | 8 weeks |
| 6 months | 10 weeks  |
| _No probationary period_ | 10 weeks  |

_*Considering that team members are still [onboarding](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#overview) during their first 30 days with GitLab, team members in countries that follow a 1-month probation period will not have their Values Check-In until after 10 weeks at the company._

### Surveys

- [Team Member](https://docs.google.com/forms/d/e/1FAIpQLSd71MxvRaBjhaxSiFW0qo0blULu9jQ0ypkU7zPEU3p-IimpIQ/viewform) 
- [Manager](https://docs.google.com/forms/d/e/1FAIpQLSfo1OVq-sg2mGu19Nd_fylegKe0068CWfIFN9B8ILjZzlPqow/viewform) 



