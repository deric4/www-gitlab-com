---
layout: markdown_page
title: "Nadia Vatalidis's README"
job: "[Senior Manager, People Operations](https://about.gitlab.com/job-families/people-ops/people-operations/) "
---

### My Job
My job is to manage, support and coach the [People Operations Specialist team](https://about.gitlab.com/job-families/people-ops/people-operations/) and the [People Experience Team](https://about.gitlab.com/job-families/people-ops/people-experience-associate/).
Along with our Senior Director, People, I'm working towards our [OKR's](https://about.gitlab.com/company/okrs/fy20-q1/#ceo-great-team-employer-brand-effective-hiring-increase-decision-making-effectiveness). 

I'm incredibly excited about automation and dogfooding GitLab within the overall People Group. You'll often hear me say: "Can we solve this async?"; "Lets make sure we are handbook first" and "What is the smallest change we can make live today?" aka [iteration](https://about.gitlab.com/handbook/values/#iteration). 

### My Availability
My calendar is viewable to anyone at GitLab, anyone in my team can schedule a 1:1 or a skip-level over a "work block", everyone else is welcome to grab a free slot in my calendar without asking. I prefer having an agenda 24 hours before the call, if it is a coffee chat we don't need an agenda, I love talking about all things nature, hiking, mountain biking, and innovative ways to recycle. My slack is always updated with what I'm doing and my availability will show there. I sometimes work weird hours, to accomodate my family's needs but not long hours. I do not work on weekends and would encourage my team to never work weekends (I find it incredibly unhealthy to not have breaks from work and spend time on your favourite hobbies, family, interest is extremely important to me).

### My Flaws
1. I sometimes struggle to be emphathetic when someone is under performing and not willing to work on it. This ties nicely in with developing my active listening skills and I'm working on this daily. 
1. I'm an extrovert and don't struggle to speak in calls, stop me when you see me speak over someone. I am working on this, by carefully looking other's on the call and when they unmute and then mute, I ask if they have something to add. 
1. I love working for GitLab, I have been a super fan since I joined the first time in 2016, if you see that I am still thinking at a small company size, please give me feedback. 
1. You can read more about my areas of development in my [Individual Growth Plan](https://docs.google.com/document/d/1o9RvOE4PAW9MPoPKKXVlNumnkesQ7HmsudYHWHjPyn0/edit)).
1. I don't give up easily, unless we are going in circles with a topic for too long. I might then help steer the conversation into a healthy direction, if that doesn't work, I escalate and work with my [Manager](https://about.gitlab.com/job-families/people-ops/people-leadership/) or my People Business Partner to get back in sync. 

### What am I good at:
1. I love solving problems (for everyone). 
1. I love collaborating across all teams and get ideas on what is working in other teams. I take most of my inspiration from our Product and Engineering teams. 
1. Reminding others to stop working, taking time off and encouring others to remain health and family first. 
1. I can take direct feedback and don't get discouraged easily. 
1. Taking ownership 
1. I have always been incredibly driven by [Efficiency](https://about.gitlab.com/handbook/values/#efficiency) and [Results](https://about.gitlab.com/handbook/values/#results) (the side-effect of this can be negative, which is why I am working on my active listening skills right now).
1. Mountain biking and anything that involves movement ;-) 
1. I trust everyone I hire instantly. 
1. I take metrics and OKR's very seriously and find healthy ways to achieve them, but never to the detrement of my team.

### What am I working on right now
I've created an [Individual Growth Plan](https://docs.google.com/document/d/1o9RvOE4PAW9MPoPKKXVlNumnkesQ7HmsudYHWHjPyn0/edit) that is publicly available. This is a living document, that I will be using to develop a few skills further. I welcome anyone in my team, my peers and colleagues to continue to provide feedback with me. This will then be considered to be added to this plan. If you prefer sharing feedback with my manager, that is 100% valued too. 
I'm also working on continiously improving the Team Member Experience at GitLab, if you would like us to work on something specific or have a problem we can help resolve, please open an issue in the [People Group Issue Tracker](https://gitlab.com/gitlab-com/people-group/General). 
If you want to share an improvement we can make from a People Group engineering perspective, I am responsible for triaging issues in our [People Operations Engineering Issue Tracker](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training), so feel free to open an issue there too!

### Your development is more important than mine
If I can support your career growth, I won't give up until you are successful. I enjoy talking about your career and what you really want to do with your career live. I enjoy discussing this formally (by using a compa ratio sheet, or a career development plan). I am very direct, if I share feedback with you, my intentions are always positive (this can be a flaw too, if someone is not ready to hear this feedback).

### I'm a total remote ambassador
I thoroughly enjoy talking to companies (big and small), universities and individuals about remote work and how we work at GitLab. 

My bio on the team page](https://about.gitlab.com/company/team/#Vatalidis) sums up what I do when I'm not working. 


If you want to contribute to my read.me, please create an MR and assign it to me (this is a living page and continues to evolve, like all of us).
