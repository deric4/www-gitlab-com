---
layout: handbook-page-toc
title: "Hiring"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Hiring pages

- [Greenhouse](/handbook/hiring/greenhouse/)
- [Principles](/handbook/hiring/principles/)
- [Job families](/handbook/hiring/job-families/)
- [Vacancies](/handbook/hiring/vacancies/)
- [Recruiting Alignment](/handbook/hiring/recruiting-alignment/)
- [Interviewing](/handbook/hiring/interviewing/)
- [Sourcing](/handbook/hiring/sourcing/)
- [Job offers and post-interview processes](/handbook/hiring/offers/)
- [Recruiting Process Framework](/handbook/hiring/recruiting-framework/)
- [Recruiting Process Framework, Talent Community](/handbook/hiring/recruiting-framework/talent-community/)
- [Referral Process](/handbook/hiring/referral-process/)
- [Referral Operations](/handbook/hiring/referral-operations/)
- [Recruiting Metrics Process](/handbook/hiring/metrics/)
- [Hiring Charts](/handbook/hiring/charts/)
- [Preferred Companies to Recruit from](/handbook/hiring/preferred-companies/)
- [Data Driven Recruiting](/handbook/hiring/data-driven-recruiting)
- [Diversity & Inclusion Recruiting Initiatives](/handbook/hiring/d-&-i-recruiting-initiatives)

Potential applicants should refer to the [jobs FAQ page](/jobs/faq/).

## Related to hiring

- [GitLab talent ambassador](/handbook/hiring/gitlab-ambassadors/)
- [Current vacancies](/jobs/apply/)
- [Contracts](/handbook/contracts)
- [Benefits](/handbook/benefits/)
- [Compensation](/handbook/total-rewards/compensation/)
- [Stock options](/handbook/stock-options)
- [Visas](/handbook/people-group/visas/)
- [Background checks](/handbook/people-group/code-of-conduct/#background-checks)
- [Onboarding](/handbook/general-onboarding)

