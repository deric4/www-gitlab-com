layout: handbook-page-toc
title: Customer Rating Feedback for Managers
category: General
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##### Overview

When a customer responds to a survey request once their ticket has been solved, they can rate their experience as either __"Good"__ or __"Bad"__ and leave any comments to explain their decision. Once the feedback has been submitted, an Issue is created and the Support Engineering Managers are notified. Below is the process we will follow for all Issues created as a result of a __"Bad"__ rating with comments.

---
##### Workflow
It is the responsibility of the Support Engineering Managers to investigate the feedback and ensure appropriate actions are taken to either resolve the root cause of the negative experience or reduce the likelihood of it recurring.

When reviewing the issue:
1. Review the comments left by the customer


2. Review the ticket and any associated Issues


3. Categorise and apply the most appropriate label/s using the following scoped labels:
   * feedback::process
     - Support process not followed/does not exist
   * feedback::tech-skills
     - Technical skills lacking to resolve/work around problem
   * feedback::soft-skills
     - Wording of responses and/or mishandling of the customer 
   * feedback::docs-issue
     - Documentation not helpful or missing
   * feedback::product-issue
     - Bug and/or feature not working as expected
   * feedback::known-issue
     - Known issue with Issue already created  
   * feedback::lacking-info
     - Not enough information supplied to progress investigation in ticket

4. Post a comment using the following template:
   * ___Summary of ticket/feedback:___  

   * ___Action to be taken:___

   * ___Contact customer to discuss feedback? (Y/N)___

5. Close the Issue once follow up actions have been completed