---
layout: handbook-page-toc
title: "GitLab Values"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## CREDIT

GitLab's six values are
[**🤝 Collaboration**](#collaboration),
[**📈 Results** ](#results),
[**⏱️ Efficiency**](#efficiency),
[**🌐 Diversity & Inclusion**](#diversity-inclusion),
[**👣 Iteration**](#iteration), and
[**👁️ Transparency**](#transparency),
and together they spell the **CREDIT** we give each other by assuming
good intent. We react to them [with values
emoji](/handbook/communication/#values-emoji) and they are made
actionable below.

### About our values
{:.no_toc}

We take inspiration from other companies, and we always go for the boring solutions.
Just like the rest of our work, we continually adjust our values and strive to make them better.
GitLab values are a living document.
In many instances, they have been documented, refined, and revised based on lessons learned (and scars earned) in the course of doing business.

We used to have more values, but it was difficult to remember them all, so we condensed them and gave sub-values and created an acronym.

Everyone is welcome to suggest improvements.

#### Sub-values as substantiators
{:.no_toc}

The "sub" in sub-value is not in reference to "subordinate," but rather, "**substantiate**."
Sub-values define top-level values, and are most easily lived out.
You may see other companies with some of our top-level values,
though our unique collection of sub-values are critical to add context and remove ambiguity.

Sub-values clarify what a given value means and looks like *at GitLab*.
Understanding this distinction is critical to thriving at GitLab,
particularly for [newer team members](/company/culture/all-remote/getting-started/) who may be familiar with a prior organization's interpretation of iteration or collaboration (as examples).

### 🤝 Collaboration
{:#collaboration .gitlab-purple}

Helping others is a priority, even when it is not immediately related to the goals that you are trying to achieve.
Similarly, you can rely on others for help and advice—in fact, you're expected to do so.
Anyone can chime in on any subject, including people who don't work at GitLab.
The person who's responsible for the work decides how to do it,
but they should always take each suggestion seriously and try to respond and explain why it may or may not have been implemented.

##### Kindness
{:.no_toc}
We value caring for others.
Demonstrating we care for people provides an effective framework for challenging directly and delivering feedback.
We disagree with companies that say [Evaluate People Accurately, Not "Kindly"](https://inside.bwater.com/publications/principles_excerpt).
We're all for accurate assessment, but we think it must be done in a kind way.
Give as much positive feedback as you can, and do it in a public way.

##### Share
{:.no_toc}
There are aspects of GitLab culture, such as extreme transparency, that are unintuitive to outsiders and new team members.
Be willing to invest in people and engage in open dialogue.
For example, consider making private issues public wherever possible so that we can all learn from the experience.

Everyone can **remind** anyone in the company about our values.
If there is a disagreement about the interpretations, the discussion can be escalated to more people within the company without repercussions.

Share problems you run into, ask for help, be forthcoming with information and **speak up**.

##### Negative feedback is 1-1
{:.no_toc}
Give negative feedback in the smallest setting possible.
One-on-one video calls are preferred.
If you are unhappy with anything (your duties, your colleague, your boss, your salary, your location, your computer), please let your boss, or the CEO, know as soon as you realize it.
We want to solve problems while they are **small**.

Negative *feedback* is distinct from negativity and disagreement. If there is no direct feedback involved, strive to discuss disagreement [in a public channel](/handbook/communication/#use-public-channels), respectfully and [transparently](/handbook/values/#transparency).

In a [GitLab Unfiltered interview on values](https://youtu.be/7kMQj4O4ZGU), GitLab co-founder and CEO Sid Sijbrandij offers the following context.

> We deal with negative all the time at GitLab. If it's not a problem, then why are we discussing it? We deal with negativity a lot, and that's also part of our ambition.
>
> If you want to get better, you talk about what you can improve. We're allowed to publicly discuss negative things; we're not allowed to give negative feedback in a large setting if it could be feasibly administered in a smaller setting.

##### Say thanks
{:.no_toc}
Recognize the people that helped you publicly, for example in our [#thanks chat channel](/handbook/communication/#internal-communication).

##### Give feedback effectively
{:.no_toc}
Giving feedback is challenging, but it's important to deliver it effectively.
When providing feedback, always make it about the work itself;
focus on the business impact and not the person.
Make sure to provide at least one clear and recent example.
If a person is going through a hard time in their personal life, then take that into account.
An example of giving positive feedback is our [thanks chat channel](/handbook/communication/#internal-communication).
For managers, it's important to realize that employees react to a negative incident with their managers [six times more strongly](https://hbr.org/2013/03/the-delicate-art-of-giving-fee) than they do to a positive one.
Keeping that in mind, if an error is so inconsequential that the value gained from providing criticism is low, it might make sense to keep that feedback to yourself.
In the situations where negative feedback must be given, focus on the purpose for that feedback: to improve the employee’s performance going forward.
Give recognition generously, in the open, and often to [generate more engagement](http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?infotype=SA&subtype=WH&htmlfid=LOW14298USEN) from your team.

##### Get to know each other
{:.no_toc}
We use a lot of [text-based communication](/company/culture/all-remote/effective-communication/), and if you know the person behind the text, it will be easier to prevent conflicts.
So we encourage people to get to know each other on a personal level through our [company calls](/handbook/communication/#company-call), virtual [coffee chats](/company/culture/all-remote/tips/#coffee-chats), and during [GitLab Contribute](/company/culture/contribute/).

##### Don't pull rank
{:.no_toc}
If you have to remind someone of the position you have in the company, you're doing something wrong.
People already know [our decision-making process](/handbook/leadership/#making-decisions).
Explain why you're making the decision, and respect everyone irrespective of their function.
This includes using the rank of another person - [including the CEO](https://twitter.com/EmiliejayG/status/1198731054162432000) - to sell an idea or decision.

##### Assume positive intent
{:.no_toc}
We naturally have a double standard when it comes to the actions of others.
We blame circumstances for our own mistakes, but individuals for theirs.
This double standard is called the [Fundamental Attribution Error](https://en.wikipedia.org/wiki/Fundamental_attribution_error).
In order to mitigate this bias, you should always [assume positive intent](https://www.collaborativeway.com/general/a-ceos-advice-assume-positive-intent/) in your interactions with others, respecting their expertise and giving them grace in the face of what you might perceive as mistakes.
When [disagreeing](/handbook/values/#disagree-commit-and-disagree), folks sometimes argue against the weakest points of argument, or sometimes argue against a "straw man".
Assume the points are presented in good faith, and instead try to [argue the "steel man" (or the "strong man")](https://desert.glass/newsletter/week-46/):
> That’s when you articulate the absolute strongest version of your opponent’s position—potentially even stronger than the one they presented.
A good steel-man argument is one where the other person feels you've represented their argument well, even if they still disagree with your assumptions or conclusion.

##### Address behavior, but don't label people
{:.no_toc}
There is a lot of good in [this article](http://bobsutton.typepad.com/my_weblog/2006/10/the_no_asshole_.html) about not wanting jerks on our team,
but we believe that **jerk** is a label for behavior rather than an inherent classification of a person.  We avoid classifications.

##### Say sorry
{:.no_toc}
If you made a mistake, apologize as soon as possible.
Saying sorry is not a sign of weakness but one of strength.
The people that do the most work will likely make the most mistakes.
Additionally, when we share our mistakes and bring attention to them, others can learn from us, and the same mistake is less likely to be repeated by someone else.
Mistakes can include when you have not been kind to someone. In order to reinforce our values, it is important, and takes more courage, to apologize publicly when you have been unkind publicly (e.g., when you have said something unkind or unprofessional to an individual or group in a Slack channel).

##### No ego
{:.no_toc}
Don't defend a point to win an argument or double-down on a mistake.
You are not your work; you don't have to defend your point.
You do have to search for the right answer with help from others.

In a GitLab Unfiltered [interview](https://youtu.be/n9Gfe9p1tmA), GitLab Head of Remote Darren M. adds context on this sub-value.

> In many organizations, there's a subtle, low-level, persistent pressure to continually prove your worth.
> And I believe that this fuels imposter syndrome and wreaks havoc on [mental health](/company/culture/all-remote/mental-health/).
>
> What's so troubling to me is how often perception is reality.
> In other words, those who have mastered the art of being perceived as elite reap benefits, though this has nothing to do with actual results.
>
> At GitLab, "no ego" means that we foster and support an environment where results matter, and you're given agency to approach your work in the way that makes sense to you.
> Instead of judging people for not approaching work in an agreed-upon way, "no ego" encourages people to glean inspiration from watching others approach work in new and different ways.

Being no ego is a standard we hold ourselves as people to but is not one that applies to GitLab as a company or product.
We want to celebrate and highlight GitLab's accomplishments, including being the [largest all-remote company](/company/culture/all-remote/).
This doesn't mean we don't recognize our mistakes, including how we handled [telemetry](/blog/2019/10/10/update-free-software-and-telemetry/).

##### See others succeed
{:.no_toc}
A candidate who has talked to a lot of people inside GitLab said that, compared to other companies, one thing stood out the most: everyone here mentioned wanting to see each other succeed.

##### Don't let each other fail
{:.no_toc}
Keep an eye out for others who may be struggling or stuck.
If you see someone who needs help, reach out and assist, or connect them with someone else who can provide expertise or assistance.
We succeed and shine together!

##### People are not their work
{:.no_toc}
Always make suggestions about examples of work, not the person.
Say "You didn't respond to my feedback about the design" instead of "You never listen".
And, when receiving feedback, keep in mind that feedback is the best way to improve, and that others giving you feedback want to see you succeed.

##### Do it yourself
{:.no_toc}
Our collaboration value is about helping each other when we have questions, need critique, or need help.
No need to brainstorm, wait for consensus, or [do with two what you can do yourself](https://www.inc.com/geoffrey-james/collaboration-is-the-enemy-of-innovation.html).

##### Blameless problem solving
{:.no_toc}
Investigate mistakes in a way that focuses on the situational aspects of a failure’s mechanism and the decision-making process that led to the failure, rather than cast blame on a person or team.
We hold blameless [root cause analyses](https://codeascraft.com/2012/05/22/blameless-postmortems/) and [retrospectives](/handbook/engineering/management/team-retrospectives/) for stakeholders to speak up without fear of punishment or retribution.

##### Short toes
{:.no_toc}
People joining the company frequently say, "I don't want to step on anyone's toes."
At GitLab, we should be more accepting of people taking initiative in trying to improve things.
As companies grow, their speed of decision-making goes down since there are more people involved.
We should counteract that by having short toes and feeling comfortable letting others contribute to our domain.
For example, pointed, respectful feedback to a [proposal](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24447) by GitLab's CEO led to his own merge request being closed.

##### It's impossible to know everything
{:.no_toc}
We know we must rely on others for the expertise they have that we don't.
It's OK to admit you don't know something and to ask for help, even if doing so makes you feel vulnerable.
It is never too late to ask a question, and by doing so, you can get the information you need to produce results and to strengthen your own skills as well as GitLab as a whole.
After your question is answered, [please document the answer so that it can be shared](/handbook/handbook-usage/#how-we-use-the-guide-every-day).

Don't display surprise when people say they don't know something, as it is important that everyone feels comfortable saying "I don't know" and "I don't understand."
(As inspired by [Recurse](https://www.recurse.com/manual#sub-sec-social-rules).)

##### Collaboration is not consensus
{:.no_toc}
When collaborating, it is always important to stay above radar and work [transparently](/handbook/values/#transparency), but collaboration is [not consensus](/handbook/leadership/#making-decisions).
You don't need to ask people for their input, and they shouldn't ask you "Why didn't you ask me?"
You don't have to wait for people to provide input, if you did ask them.
We believe in permissionless innovation—you don't need to involve people, but everyone can contribute.
This is core to how we [iterate](/handbook/values/#iteration), since we want smaller teams moving quickly rather than large teams achieving consensus slowly.

##### Collaboration is not playing politics
{:.no_toc}
We don't want people to play politics at GitLab.
One way to spot when this is happening is when people discussing a proposal focus overly on whose proposal it is.
This is a manifestation of the [Belief Bias](https://twitter.com/g_s_bhogal/status/1225561210139049984?), where we judge an argument’s strength not by how strongly it supports the conclusion but by how strongly *we* support the conclusion.
Proposals should be weighed on their merits and not on who proposed them.
The other thing to observe is whether people are being promoted based on others liking them or having a lot of alliances.
We want people to be promoted based on their results.
We do value collaboration, but that's different than being promoted just because people like you.

<figure class="video_container">
    <iframe src="https://www.youtube.com/embed/vEk9z5vumB8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

##### Collaboration Competency
{:#collaboration-competency .no_toc}
[Competencies](/handbook/competencies/) are the Single Source of Truth (SSoT) framework for things we need team members to learn.
We demonstrate collaboration when we take action to help others and include other's (both internal and external) input (both help and feedback) to achieve the best possible outcome.

| Level | Demonstrates Collaboration Competency by... | Certification |
|-------|---------------------------------------------|---------------|
| Junior/Associate | Develops collaboration skills by learning from other team members | thing |
| Intermediate | Grows collaboration skills by using different types of communication; files issues appropriately, asks in the right Slack channels and uses the right labels | thing |
| Senior | Models collaborative behavior for fellow team members and others within the group | thing |
| Staff/Manager | Coaches team members on how to collaborate more effectively and pointing team members to the right channels to collaborate | thing|
| Senior Manager | Fosters collaborative decision making and problem solving across the departments | thing |
| Director | Drives team collaboration across divisions/departments, silos, and division boundaries | thing |
| Senior Director | Develops networks and builds partnerships, engages in cross-functional activities; collaborates across boundaries, and finds common ground with a widening range of stakeholders.  Utilizes contacts to build and strengthen internal support base | thing |
| VP | Leads collaboration and teamwork in daily routines, prioritizing interactions, information sharing, and real time decision making across divisions/departments. Encourages greater cross-functional collaboration among e-team leaders   | thing |
| EVP/CXO | Champions collaboration and teamwork into daily routines, prioritizing interactions, information sharing, and real time decision making across divisions/departments. Champions cross-functional collaboration among e-team leaders and GitLab | thing

### 📈 Results
{:#results .gitlab-purple}

We do what we promised to each other, customers, users, and investors.

##### Dogfooding
{:.no_toc}
1.  We [use our own product](https://en.wikipedia.org/wiki/Eating_your_own_dog_food).
Our development organization uses GitLab.com to manage the DevOps lifecycle of the GitLab.
1.  Our entire company uses GitLab to collaborate on this handbook.
We also capture other content and processes in Git repos and manage them with GitLab.
1.  When something breaks, doesn't work well, or needs improvement, we are more likely to notice it internally and address it before it impacts our larger community.

##### Measure results not hours
{:.no_toc}
We care about what you achieve: the code you shipped, the user you made happy, and the team member you helped. Someone who took the afternoon
off shouldn't feel like they did something wrong. You don't have to defend how you spend your day. We trust team members to do the right thing instead of having rigid rules. Do not incite competition by proclaiming how many hours you worked yesterday. If you are working too many hours, talk to your manager to discuss solutions.

##### Give agency
{:.no_toc}
We give people agency to focus on what they think is most beneficial. If a meeting doesn't seem interesting and someone's active participation is not critical to the outcome of the meeting, they can always opt to not attend, or during a video call they can work on other things if they want. Staying in the call may still make sense even if you are working on other tasks, so other peers can ping you and get fast answers when needed. This is particularly useful in multi-purpose meetings where you may be involved for just a few minutes.

##### Write promises down
{:.no_toc}
Agree in writing on measurable goals. Within the company we use [public OKRs](/company/okrs/) for that.

##### Growth mindset
{:.no_toc}
You don't always get results and this will result in criticism from yourself and/or others. We believe our talents can be developed through hard work, good strategies, and input from others. We try to hire people based on [their trajectory, not their pedigree](https://hbr.org/2016/01/what-having-a-growth-mindset-actually-means).

##### Global optimization
{:.no_toc}
This name comes from the [quick guide to Stripe's culture](https://stripe.com/us/jobs/candidate-info?a=1#culture). Our definition of global optimization is that you do what is best for the organization as a whole. Don't optimize for the goals of your team when it negatively impacts the goals of other teams, our users, and/or the company. Those goals are also your problem and your job. Keep your team as lean as possible, and help other teams achieve their goals.
In the context of [collaboration](#collaboration), this means that if anyone is blocked by you on a question, your approval, or a merge request review, your top priority is always to unblock them, either directly or through helping them find someone else who can, even if this takes time away from your own or your team's priorities.

##### Tenacity
{:.no_toc}
We refer to this as "persistence of purpose". As talked about in [The Influence Blog](https://www.learntoinfluence.com/developing-tenacity-when-facing-opposition/), tenacity is the ability to display commitment to what you believe in. You keep picking yourself up, dusting yourself off, and quickly get going again having learned a little more.

##### Ownership
{:.no_toc}
We expect team members to complete tasks that they are assigned. Having a task means you are responsible for anticipating and solving problems. As an owner, you are responsible for overcoming challenges, not suppliers or other team members. Take initiative and proactively inform stakeholders when there is something you might not be able to solve.

##### Sense of urgency
{:.no_toc}
At an exponentially-scaling startup, time gained or lost has compounding effects. Try to get the results as fast as possible, but without compromising our other values and [ways we communicate](/handbook/communication), so the compounding of results can begin and we can focus on the next improvement.

##### Ambitious
{:.no_toc}
While we iterate with small changes, we strive for large, ambitious results.

##### Perseverance
{:.no_toc}
Working at GitLab will expose you to situations of various levels of difficulty and complexity. This requires focus and the ability to defer gratification.
We value the ability to maintain focus and motivation when work is tough and asking for help when needed.

##### Bias for Action
{:.no_toc}
It's important that we keep our focus on action, and don't fall into the trap of analysis paralysis or sticking to a slow, quiet path without risk. Decisions should be thoughtful, but delivering fast results requires the fearless acceptance of occasionally making mistakes; our bias for action also allows us to course correct quickly. Everyone will make mistakes, but it's the relative number of mistakes against all decisions made (i.e. percentage of mistakes), and the swift correction or resolution of that mistake, which is important. A key to success with transparency is to always combine an observation with questions to ensure understanding and suggestions for solutions / improvement to the group that can take action.  We don't take the easy path of general complaints without including and supporting the groups that can affect change. Success with transparency almost always requires effective [collaboration](#collaboration).

##### Accepting Uncertainty
{:.no_toc}
We should strive to accept that there are things that we don’t know about the work we’re trying to do, and that the best way to drive out that uncertainty is not by layering analysis and conjecture over it, but rather accepting it and moving forward, driving it out as we go along. Wrong solutions can be fixed, but non-existent ones aren’t adjustable at all. [The Clever PM Blog](https://www.cleverpm.com/2018/08/23/accepting-uncertainty-is-the-key-to-agility/)

##### Customer results
{:.no_toc}
Our focus is to improve the results that customers achieve, which requires being aware of [the Concur effect](https://twitter.com/ryanfalor/status/1182647229414166528?s=12), see [the Hacker News discussion](https://news.ycombinator.com/item?id=21224209) for a specific UX example.
**Customer results are more important** than:

1. **What we plan to make**. If we focus only on our own plans, we would have only GitLab.com and no self-hosted delivery of GitLab.
1. **Large customers**. This leads to the [innovator's dilemma](https://en.wikipedia.org/wiki/The_Innovator%27s_Dilemma), so we should also focus on small customers and future customers (users).
1. **What customers ask for**. This means we don't use the phrase "customer focus", because it tempts us to prioritize what the customer _says_ they want over what we discover they actually need through our product development process. Often, it’s easier for a customer to think in terms of a specific solution than to think about the core problem that needs to be solved. But a solution that works well for one customer isn’t always relevant to other customers, and it may not align with our overall product strategy. When a customer asks for something specific, we should strive to understand why, work to understand the broader impact, and then create a solution that scales.
1. **Our existing scope**. For example, when customers asked for better integrations and complained about integration costs and effort, we responded by expanding our scope to create a [single application](/handbook/product/single-application/) for the DevOps lifecycle.
1. **Our assumptions**. Every company works differently, so we can’t assume that what works well for us will support our customers’ needs. When we have an idea, we must directly validate our assumptions with customers to ensure we create scalable, highly relevant solutions.
1. **What we control**. We should take responsibility for what the **customer experiences**, even when it isn’t entirely in our control. We aim to treat every customer-managed instance downtime as a [$1M a day problem](https://canary.gitlab.com/gitlab-com/www-gitlab-com/commit/8b7857c1f7c59c53e4a9c0d6008830459d817497).

##### Results Competency
{:#results-competency .no_toc}
[Competencies](/handbook/competencies/) are the Single Source of Truth (SSoT) framework for things we need team members to learn.
We demonstrate results when we do what we promised to each other, customers, users, and investors.

| Level | Demonstrates Results Competency by... | Certification |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|
| Junior/Associate | Develops the skills needed to commit and execute on agreed actions | thing |
| Intermediate | Applies commitment to results and demonstrates ability to execute on agreed actions | thing |
| Senior | Models a sense of urgency and commitment to deliver results | thing |
| Staff/Manager | Coaches team members to collaborate and work iteratively towards results with the focus on the outcome and not hours worked. | thing |
| Senior Manager | Fosters a culture of ownership for own performance | thing |
| Director | Drives efficient execution of results ensuring collaboration between team members | thing |
| Senior Director | Develops quarterly OKR's ensuring the performance and results of one or more teams | thing |
| VP | Leads the achievement of results while driving the continued alignment to our values of collaboration, efficiency, diversity, iteration and transparency | thing |
| EVP/CXO | Leads the achievement of results while driving the continued alignment to our values of collaboration, efficiency, diversity, iteration and transparency | thing |

### ⏱️ Efficiency
{:#efficiency .gitlab-purple}

We care about working on the right things, not doing more than needed, and not duplicating work. This enables us to achieve more progress, which makes our work more fulfilling.

##### Write things down
{:.no_toc}
We document everything: in the handbook, in meeting notes, in issues.
We do that because "[the faintest pencil is better than the sharpest memory](https://www.quora.com/What-does-The-faintest-pencil-is-better-than-the-sharpest-memory-mean)."
It is far more efficient to read a document at your convenience than to have to ask and explain. Having something in version control also lets everyone contribute suggestions to improve it.

##### Boring solutions
{:.no_toc}
Use the simplest and most boring solution for a problem, and remember that [“boring” should not be conflated with “bad” or “technical debt.”](http://mcfunley.com/choose-boring-technology)
The speed of innovation for our organization and product is constrained by the total complexity we have added so far, so every little reduction in complexity helps.
Don’t pick an interesting technology just to make your work more fun;
using established, popular tech will ensure a more stable and more familiar experience for you and other contributors.

Make a conscious effort to **recognize** the constraints of others within the team.
For example, sales is hard because you are dependent on another organization, and development is hard because you have to preserve the ability to quickly improve the product in the future.

##### Self-service and self-learning
{:.no_toc}
Team members should first [search for their own answers](/company/culture/all-remote/self-service/) and, if an answer is not readily found or the answer is not clear, ask in public as we all should have a [low level of shame](/handbook/values/#low-level-of-shame). [Write down any new information discovered](/handbook/values/#write-things-down) and pay it forward so that those coming after will have better efficiency built on top of practicing collaboration, inclusion, and documenting the results.

##### Efficiency for the right group
{:.no_toc}
Optimize solutions globally for the broader GitLab community. Making a process efficient for one person or a small group may not be the efficient outcome for the whole GitLab community. As an example, it may be best to discard a renewal process that requires thousands of customers to each spend two hours in favor of one that only takes sixty seconds, even when it may make a monthly report less efficient internally! In a decision, ask yourself "For whom does this need to be most efficient?" Quite often, the answer may be your users, contributors, customers, or team members that are dependent upon your decision.

##### Be respectful of others' time
{:.no_toc}
Consider the time investment you are asking others to make with meetings and a permission process. Try to avoid meetings, and if one is necessary, try to make attendance optional for as many people as possible. Any meeting should have an agenda linked from the invite, and you should document the outcome. Instead of having people ask permission, trust their judgment and offer a consultation process if they have questions.

##### Spend company money like it's your own
{:.no_toc}
Every dollar we spend will have to be earned back; be as frugal with company money as you are with your own.

##### Frugality
{:.no_toc}
[Amazon states it best](http://www.amazon.jobs/principles) with: "Accomplish more with less. Constraints breed resourcefulness, self-sufficiency, and invention. There are no extra points for growing headcount, budget size, or fixed expense."

##### ConvDev
{:.no_toc}
We work according to the principles of [conversational development](http://conversationaldevelopment.com/).

##### Freedom
{:.no_toc}
You should have clear objectives and the freedom to work on them as you see fit.

##### Short verbal answers
{:.no_toc}
Give short answers to verbal questions so the other party has the opportunity to ask more or move on.

##### Keep broadcasts short
{:.no_toc}
Keep one-to-many written communication short, as mentioned in [this HBR study](https://hbr.org/2016/09/bad-writing-is-destroying-your-companys-productivity): "A majority say that what they read is frequently ineffective because it’s too long, poorly organized, unclear, filled with jargon, and imprecise."

##### Managers of one
{:.no_toc}
We want each team member to be [a manager of one](https://signalvnoise.com/posts/1430-hire-managers-of-one) who doesn't need daily check-ins to achieve their goals.

##### Responsibility over rigidity
{:.no_toc}
When possible, we give people the responsibility to make a decision and hold them accountable for that, instead of imposing rules and approval processes.

##### Accept mistakes
{:.no_toc}
Not every problem should lead to a new process to prevent them. Additional processes make all actions more inefficient; a mistake only affects one.

##### Move fast by shipping the minimum viable change
{:.no_toc}
We value constant improvement by iterating quickly, month after month. If a task is not the [smallest thing possible](/handbook/values/#iteration), cut the scope.

##### Efficiency Competency
{:#efficiency-competency .no_toc}
[Competencies](/handbook/competencies/) are the Single Source of Truth (SSoT) framework for things we need team members to learn.
We demonstrate efficiency when we work on the right things, not doing more than needed, and not duplicating work.

| Level | Demonstrates Efficiency Competency by... | Certification |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|
| Junior/Associate | * Developing an understanding of being a manager of 1: Take responsibility for your own tasks and deliver on commitments <br> * Brings up ideas for process improvements to 1:1s. * Learns to write everything down as it is far more efficient to read a document at your convenience than to have to ask and explain | thing |
| Intermediate | * Has a growing understanding of efficiency and is acting by surfacing process inefficiencies in the team <br> * Seeks out ways to be more effective in their role, while also starting to mentor others in ways to work efficiently. | thing |
| Senior | * Models a culture of efficiency within the team where people make good, timely decisions using available data and assessing multiple alternatives <br> * Models using boring solutions for increasing the speed of innovation for our organization and product | thing |
| Staff/Manager | * Takes ownership of own team process inefficiencies, implements cross team efforts in ensuring things are running smoothly <br> Implements a way of working in the team where team members first search for their own answers and, if an answer is not readily found or the answer is not clear, ask in public as we all should have a low level of shame | thing |
| Senior Manager | * Takes ownership of group level process inefficiencies, guides cross sub-departments in ensuring things are running smoothly <br> Fosters a culture in the sub-departments where you respect others' time and promote self-service and self-learning | thing |
| Director | * Drives the framework of frugality on a department level and owns departments efforts in ensuring things are running smoothly <br> * Drives efficient resolution of highly complex or unusual business problems that impact the department / team. Holds their managers and peers accountable for upholding this value | thing |
| Senior Director | * Develops the framework and strategy of frugality cross division.  Resulting in efforts ensuring things are running smoothly <br> * Develops leaders to action on division/department/team inefficiencies. Hold their management teams accountable for upholding this value.  | thing |
| VP | * Leads with efficiency across the company. Ensures efficient resource allocation decisions across the company <br> * Leads across company strategy and policy improvements that move the business towards more efficiency. They hold their senior management and the e-group accountable for upholding this value | thing |
| EVP/CXO | Champions GitLab's strategy for efficiency internally and externally. Constantly looking for efficiency improvements cross company and holding other e-group members accountable for upholding efficiency too. They are comfortable leading through frugality and accepting of mistakes| thing |

### 🌐 Diversity & Inclusion
{:#diversity-inclusion .gitlab-purple}

Diversity and inclusion are fundamental to the success of GitLab. We aim to make a significant impact in our efforts to [foster an environment where everyone can thrive](/handbook/people-group/code-of-conduct/). We are designing a multidimensional approach to ensure that GitLab is a place where people from every background and circumstance feel like they belong and can contribute. We actively chose to [build and institutionalize](http://www.russellreynolds.com/en/Insights/thought-leadership/Documents/Diversity%20and%20Inclusion%20GameChangers%20FINAL.PDF) a culture that is [inclusive](/company/culture/inclusion/) and supports all employees equally in the process of achieving their professional goals. We hire globally and encourage hiring in a diverse set of countries. We work to make everyone feel welcome and to increase the participation of underrepresented minorities and nationalities in our community and company. For example, we celebrate our sponsorship of [diversity events](/blog/2016/03/24/sponsorship-update/) and offer a [double referral bonus](/handbook/incentives/#referral-bonuses).

##### Bias towards asynchronous communication
{:.no_toc}
Take initiative to operate [asynchronously](/company/culture/all-remote/asynchronous/) whenever possible. This shows care and consideration for those who may not be in the same time zone, are traveling outside of their usual time zone, or are [structuring their day](/company/culture/all-remote/people/#creating-a-more-efficient-day) around pressing commitments at home or in their community.

This is demonstrated by communicating recordings of [meetings](/company/culture/all-remote/meetings/), using GitLab Issues and Merge Requests rather than texts, calls, or Slack messages, and being sensitive to local holidays and vacation statuses. Encourage others to default to [documentation](/handbook/documentation/) rather than pressuring others to be online outside of their working hours.

##### Reach across company departments
{:.no_toc}
While it's wise to seek advice from experts within your function, we encourage GitLab team members to seek and provide feedback across departments. This enables the team to iterate more quickly, taking a more diverse perspective into account.

##### Make family feel welcome
{:.no_toc}
One of the unique elements to an [all-remote culture](/company/culture/all-remote/) is the ability to visit a person's home while collaborating. If the tenor of the meeting allows, feel welcome to invite your family members or pets to drop by and greet your colleagues.

##### Shift working hours for a cause
{:.no_toc}
Caregiving, outreach programs, and community service do not conveniently wait for regular business hours to conclude. If there's a cause or community effort taking place, feel welcome to work with your manager and shift your working hours to be available during a period where you'll have the greatest impact for good. For colleagues supporting others during these causes, document everything and strive to post recordings so it's easy for them to catch up.

##### Be a mentor
{:.no_toc}
People feel more included when they're supported. To encourage this, and to support diversified learning across departments, consider GitLab's [Interning for Learning](/handbook/people-group/promotions-transfers/#interning-for-learning) program.

##### Culture fit is a bad excuse
{:.no_toc}
We don't hire based on culture or select candidates because we'd like to have a drink with them. We hire and reward employees based on our shared values as detailed on this page. We want a **values fit**, not a culture fit.
We want cultural diversity instead of cultural conformity, such as a [brogrammer](https://en.wikipedia.org/wiki/Brogrammer) atmosphere. Said differently: ["culture add" > "culture fit"](https://twitter.com/Una/status/846808949672366080) or "hire for culture contribution" since our [mission is that everyone can contribute](/company/strategy/#mission).

##### Religion and politics at work <a name="dont-bring-religion-or-politics-to-work"></a>
{:.no_toc}
We generally avoid discussing politics or religion in public forums because it is easy to alienate people that have a minority opinion. This doesn’t mean we never discuss these topics. Because we value diversity and inclusion, and want all team members to feel welcome and contribute equally, we encourage free discussion of operational decisions that can move us toward being a more inclusive company. GitLab also publicly supports pro-diversity activities and events.

It's also acceptable for individuals to bring up politics and religion in social contexts such as coffee chats and real-life meetups with other coworkers (with the goal to understand and not judge), but always be aware of sensitivities, exercise your best judgment, and make sure you stay within the boundaries of our [Code of Conduct](/handbook/people-group/code-of-conduct/).

##### Quirkiness
{:.no_toc}
Unexpected and unconventional things make life more interesting.
Celebrate and encourage quirky gifts, habits, behavior, and points of view. An example
is our [company call](/handbook/communication/#company-call) where we spend most of our time talking about what we've done in our private lives, from fire-throwing to
knitting. Open source is a great way to interact with interesting
people. We try to hire people who think work is a great way to express themselves.

##### Building a safe community
{:.no_toc}
Do **not** make jokes or unfriendly remarks about race, ethnic origin, skin color, gender, or sexual orientation.
Everyone has the right to feel safe when working for GitLab and/or as a part of the GitLab community.
We do not tolerate abuse, [harassment](/handbook/anti-harassment/), exclusion, discrimination, or retaliation by/of any community members, including our employees.
You can always **refuse** to deal with people who treat you badly and get out of situations that make you feel uncomfortable.

##### Unconscious bias
{:.no_toc}
We recognize that unconscious bias is something that affects everyone and that the
effect it has on us as humans and our company is large.
We are responsible for understanding our own implicit biases and helping others
understand theirs. We are continuously [working on getting better at this topic](/handbook/communication/unconscious-bias/).

##### Inclusive benefits
{:.no_toc}
We list our [Transgender Medical Services](/handbook/benefits/inc-benefits-us/#transgender-medical-services) and [Parental Leave](/handbook/benefits/#parental-leave) publicly so people don't have to ask for them during interviews.

##### Inclusive language & pronouns
{:.no_toc}
Use **inclusive** language.
For example, prefer "Hi everybody" or "Hi people" to "Hi guys", and "they" instead of "he/she". While there are several good guides from folks like [18f](https://content-guide.18f.gov/inclusive-language/), [University of Calgary](https://www.ucalgary.ca/news/ucalgary-guidelines-updated-communicating-about-gender-and-sexual-diversity), and [Buffer](https://open.buffer.com/inclusive-language-tech/) on using inclusive language, we don't keep an exhaustive list.
When new possibly non-inclusive words arise, we prefer to be proactive and look for an alternative.
If your goal is to be inclusive, it is more effective to make a small adjustment in the vocabulary when some people have a problem with it, rather than making a decision to not change it because some people don’t think it is a problem.
And if you make a mistake (e.g. accidentally using the wrong pronoun or an outdated phrase), acknowledge it, **apologize gracefully and move on**; there is no need to dwell on it, and you can work to avoid making that mistake in the future.
Please also visit our [Gender and Sexual-orientation Identity Definitions and FAQ](/handbook/people-group/gender-pronouns/) page if you have questions around pronouns and other topics related to gender / sexual orientation.

##### Inclusive interviewing
{:.no_toc}
This is documented on our page about [interviewing](/handbook/hiring/interviewing/#inclusive-interviewing).

##### Inclusive meetings
{:.no_toc}
Be consciously inclusive in [meetings](/company/culture/all-remote/meetings/) by giving everyone present an opportunity to talk and present their points of view. This can be especially important in a remote setting.

With internal meetings, consider using an agenda document for questions. For example, with GitLab [Group Conversations](https://about.gitlab.com/handbook/people-group/group-conversations/), every meeting has a numbered list that GitLab team members can add questions to. During the meeting, questions are answered in turn and discussions noted in the same document.

Customers are not used to working in this way. To promote inclusion with customers: ask participants for their goals; make sure during demos that you pause for question; leave time for discussion.

##### See Something, Say Something
{:.no_toc}
As a globally-dispersed company, we have employees from many different backgrounds and cultures. That means it is important for each of us to use great judgment in being respectful and inclusive of our teammates. At the same time, we may sometimes not fully realize we have said or done something to offend someone. It is important that our teammates hold each other accountable and let them know if they have unintentionally or intentionally done something so they can learn and gain additional understanding of perspectives different from our own. It is also important that our teammates don't feel excluded or minimized by the words we use or the things we do. Thus, we all need to speak up when we see something that isn't respectful or inclusive.

##### Neurodiversity
{:.no_toc}
[Neurodiversity](http://neurocosmopolitanism.com/neurodiversity-some-basic-terms-definitions/) is a type of diversity that includes: autism, ADHD, dyslexia, dyscalculia, dyspraxia, and other styles of neurodivergent functioning. While neurodivergent individuals often bring [unique skills and abilities](https://adhdatwork.add.org/potential-benefits-of-having-an-adhd-employee/) which can be harnessed for [competitive advantage](https://hbr.org/2017/05/neurodiversity-as-a-competitive-advantage) in many fields including [cybersecurity](https://www.forbes.com/sites/samcurry/2019/05/13/neurodiversity-a-competitive-advantage-in-cybersecurity/), neurodivergent individuals are often discriminated against, and sometimes have trouble making it through traditional hiring processes. These individuals should be able to contribute as GitLab team members. The handbook, values, strategy, and interviewing process should never discriminate against the neurodivergent.

##### Family and friends first, work second
{:.no_toc}
Long-lasting relationships [are the rocks of life](https://www.youtube.com/watch?v=6_N_uvq41Pg), and come before work. As someone said in our #thanks channel after helping a family member for five days after a hurricane: "THANK YOU to GitLab for providing a culture where "family first" is truly meant".  Use the hashtag: **#FamilyAndFriends1st**

##### Knowledge Assessment
{:.no_toc}
[Competencies](/handbook/competencies/) are the Single Source of Truth (SSoT) framework for things we need team members to learn.
We demonstrate diversity and inclusions when we foster an environment where everyone can thrive and ensuring that GitLab is a place where people from every background and circumstance feel like they belong and can contribute.

<table class="tg">
  <tr>
    <th class="tg-0lax">Level</th>
    <th class="tg-0lax">Demonstrates Diversity &amp; Inclusion Competency by…</th>
    <th class="tg-0lax">Knowledge Assessment<br></th>
  </tr>
  <tr>
    <td class="tg-0lax">Junior/Associate</td>
    <td class="tg-0lax">Develops an understanding of the impact of biases; seeks to learn more about their own biases. Is accountable for their actions, apologizes and learns from their mistakes.</td>
    <td class="tg-0lax" rowspan="3"><a href="https://docs.google.com/forms/d/e/1FAIpQLScwmN71M4BR3wHgHP7niVllo70BY2HT1nM2uxsxHc4Mz7cRcA/viewform">Knowledge Assessment for Individual Contributors</a></td>
  </tr>
  <tr>
    <td class="tg-0lax">Intermediate</td>
    <td class="tg-0lax">Has a growing understanding of the impact of biases; fosters a sense of inclusion and belonging on their team. Holds themselves and peers accountable for upholding this value by kindly pointing out when mistakes might be made. Encourages an inclusive team environment where differences are encouraged and everyone can contribute.</td>
  </tr>
  <tr>
    <td class="tg-0lax"><br>Senior</td>
    <td class="tg-0lax">Actively aware of how bias or exclusion might occur on a team and helps to facilitate a team environment where team members belong and feel safe. Models empathy with their interactions with customers and cross functional team members.</td>
  </tr>
  <tr>
    <td class="tg-0lax">Staff/Manager</td>
    <td class="tg-0lax">Implements best practices to limit bias on their team. They ensure blameless accountability is practiced throughout their team. Creates an environment where team members feel safe to share ideas and welcomes individual differences.</td>
    <td class="tg-0lax" rowspan="6"><a href="https://docs.google.com/forms/d/e/1FAIpQLSel_U-audrhPvDlHFAbxF7Jvn5prDns2p8iGzWOTbL1oGraxg/viewform">Knowledge Assessment for People Leaders</a></td>
  </tr>
  <tr>
    <td class="tg-0lax">Senior Manager</td>
    <td class="tg-0lax">Proactively finds ways of facilitating an inclusive team environment and assesses processes to protect against unconscious bias. They hold their team members accountable including cross functional stakeholders. Promotes individual differences across their team and another departments.</td>
  </tr>
  <tr>
    <td class="tg-0lax">Director</td>
    <td class="tg-0lax">Drives diversity, inclusion and sense of belonging across their department. They hold their managers and peers accountable for upholding this value. They are actively involved in the execution of D&amp;I strategies and encourage others to participate.</td>
  </tr>
  <tr>
    <td class="tg-0lax">Senior Director</td>
    <td class="tg-0lax">Embeds the value of Diversity &amp; Inclusion across their division and finds opportunities to limit the impact of bias on decision making processes. Uses feedback and data to formulate a strategy on how to make improvements. They hold their management teams accountable for upholding the value.</td>
  </tr>
  <tr>
    <td class="tg-0lax">VP</td>
    <td class="tg-0lax">Leads with the value of Diversity &amp; Inclusion across the company and finds opportunities to limit the impact of bias on decision making processes. They sponsor internal initiatives to increase trust, psychological safety and inclusion. They hold their senior management and the e group accountable for upholding this value.</td>
  </tr>
  <tr>
    <td class="tg-0lax">EVP/CXO</td>
    <td class="tg-0lax">Champions the value of Diversity and Inclusion into the company's strategy. They champion and sponsor internal and external D&amp;I initiatives. They speak to the importance of this value in company-wide meetings. They hold their leaders and other e group members accountable for upholding this value. They continuously seek for ways to increase trust, psychological safety and inclusion across the broader company.</td>
  </tr>
</table>


### 👣 Iteration
{:#iteration .gitlab-purple}

We do the [smallest thing possible and get it out as quickly as possible](/blog/2017/01/04/behind-the-scenes-how-we-built-review-apps/). If you make suggestions that can be excluded from the first iteration, turn them into a separate issue that you link. Don't write a large plan; only write the first step. Trust that you'll know better how to proceed after something is released. You're doing it right if you're slightly embarrassed by the minimal feature set shipped in the first iteration. This value is the one people most underestimate when they join GitLab. The impact both on your work process and on how much you achieve is greater than anticipated. In the beginning, it hurts to make decisions fast and to see that things are changed with less consultation. But frequently, the simplest version turns out to be the best one.

People that join GitLab all say they already practice this iteration. But this is the value that they have the hardest time adopting. People are trained that if you don't deliver a perfect or polished thing, you get dinged for it. If you do just one piece of something, you have to come back to it. Doing the whole thing seems more efficient, even though it isn't. If the complete picture is not clear, your work might not be perceived as you want it to be perceived. It seems better to make a comprehensive product. They see other people in the GitLab organization being really effective with iteration but don't know how to make the transition, and it's hard to shake the fear that constant iteration can lead to shipping lower-quality work or a worse product.

The way to resolve this is to write down only what you can do with the time you have for this project right now. That might be 5 minutes or 2 hours. Think of what you can complete in that time that would improve the current situation. Iteration can be uncomfortable, even painful. If you're doing iteration correctly, it should be.

However, if we take smaller steps and ship smaller, simpler features, we get feedback sooner. Instead of spending time working on the wrong feature or going in the wrong direction, we can ship the smallest product, receive fast feedback, and course correct. People might ask why something was not perfect. In that case, mention that it was an iteration, you spent only "x" amount of time on it, and that the next iteration will contain "y" and be ready on "z".

<figure class="video_container">
    <iframe src="https://www.youtube.com/embed/tPTweQlBS54" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

##### Don't wait
{:.no_toc}
Don’t wait. When you have something of value like a potential blog post or a small fix, implement it straight away. Right now, everything is fresh in your head and you have the motivation. Inspiration is perishable. Don’t wait until you have a better version. Don’t wait until you record a better video. Don’t wait for an event (like Contribute). Inventory that isn’t released is a liability since it has to be managed, becomes outdated, and you miss out on the feedback you would have received had you implemented it straight away.

##### Set a Due Date
{:.no_toc}
We always try to set a due date. If needed, we cut scope.
If we have something planned for a specific date, we make that date. For example we [shipped over 100 releases on the 22nd of the month](https://about.gitlab.com/blog/2018/11/21/why-gitlab-uses-a-monthly-release-cycle/). But every one of them doesn't contain all the features we planned.
If we planned an announcement for a certain date, we might announce less or indicate what is still uncertain.
But we set a due date because having something out there builds trust and gives us better feedback.

##### Cleanup over sign-off
{:.no_toc}
As discussed in [Sid's interview on iteration](https://youtu.be/tPTweQlBS54?t=1030), waiting for approval can slow things down. We can prevent this with automation (e.g. tests of database migration performance) or clean-up after the fact (refactor a Pajamas if something was added that isn't coherent), but we try to ensure that people don't need to wait for signoff.

##### Reduce cycle time
{:.no_toc}
Short iterations reduce [our cycle time](/product/cycle-analytics/).

##### Work as part of the community
{:.no_toc}
Small iterations make it easier to work with the wider community. Their work looks more like our work, and our work is also quicker to receive feedback.

##### Minimum Viable Change (MVC)
{:.no_toc}
We encourage MVCs to be as small as possible. Always look to make the quickest change possible to improve the user's outcome. If you validate that the change adds more value than what is there now, then do it. No need to wait for something more robust. More information is in the [product handbook](/handbook/product/#the-minimally-viable-change-mvc), but this applies to everything we do in all functions. Specifically for product MVCs, there is additional responsibility to validate with customers that we're adding useful functionality without obvious bugs or usability issues.

##### Make a proposal
{:.no_toc}
If you need to decide something as a team, make a concrete proposal instead of calling a meeting to get everyone's input. Having a proposal will be a much more effective use of everyone's time. Every meeting should be a review of a proposal. We should be [brainwriting on our own instead of brainstorming out loud](https://www.fastcompany.com/3033567/brainstorming-doesnt-work-try-this-technique-instead). State the underlying problem so that people have enough context to propose reasonable alternatives. The people that receive the proposal should not feel left out and the person making it should not feel bad if a completely different proposal is implemented. Don't let your desire to be involved early or to see your solution implemented stand in the way of getting to the best outcome. If you don't have a proposal, don't let that stop you from highlighting a problem, but please state that you couldn't think of a good solution and list any solutions you considered.

##### Everything is in draft
{:.no_toc}
At GitLab, we rarely mark any content or proposals as drafts. Everything is always in draft and subject to change.

##### Under construction
{:.no_toc}
As we get more users, they will ask for stability, especially in our UX. We should always optimize for the long term. This means that users will be inconvenienced in the short term, but current and future users will enjoy a better product in the end.

##### Low level of shame
{:.no_toc}
When we talked to Nat Friedman, he said: "A low level of shame is intrinsic to your culture.". This captures the pain we feel by shipping something that isn't where we want it to be yet.

In a GitLab Unfiltered [interview](https://youtu.be/n9Gfe9p1tmA), GitLab Head of Remote Darren M. adds context on this sub-value.

> In many organizations, you take a risk when you put forth any work that's not perfect — where you haven't spent endless cycles planning for contingencies or counterpoints. Because of this, you're incentivized to invest a lot of time and effort into preparing for 'What if?' scenarios before any work is presented.
>
> The downside to that is clear. If you do eventually put forth the work, but it needed to be course corrected a long time ago, you've now squandered a lot of time that you could have spent improving it via iteration.
>
> Having a low level of shame requires you to combat a natural inclination to conceal work until it's perfect, and instead celebrate the small changes.

##### Focus on Improvement
{:.no_toc}
We believe great companies sound negative because they focus on what they can improve, not on what is working.
Our first question in every conversation with someone outside the company should be: What do you think we can improve?
This doesn't mean we don't recognize our successes; for example, see our [Say Thanks](#say-thanks) value.
We are positive about the future of the company; we are present-day pessimists and long-term optimists.

##### Do things that don't scale
{:.no_toc}
First, optimize for speed and results; when it is a success, figure out how to scale it. Great examples are in [this article by Paul Graham](http://paulgraham.com/ds.html).

##### Make two-way door decisions
{:.no_toc}
Most decisions are easy to reverse. In these cases, the [directly responsible individual](/handbook/people-group/directly-responsible-individuals/) should go ahead and make them without approval. As [Jeff Bezos describes](http://minimumviablestrategy.com/lessons/leadership/one-way-and-two-way-door-decisions/) only when you can't reverse them should there be a more thorough discussion.

##### Changing proposals isn't iteration
{:.no_toc}
Changing a proposal isn't iterating. Only when the change is rolled out to users can you learn from feedback. When you're changing a proposal based on different opinions, you're frequently wasting time; it would be better to roll out a small change quickly and get real world feedback.

A few challenges have arisen with how we approach iteration. The best example may be the proposal of a two-month release cycle. The argument was that a longer release cycle would buy us time for bug fixes and feature development, but we don’t believe that is the case. As detailed above, we aim to make the absolute smallest thing possible, and that doing otherwise will only slow us down.

That said, we would love to work on a two-week release cycle, but that should be another conversation.

##### Make small merge requests
{:.no_toc}

When you are submitting a merge request for a code change, or a process change in
the handbook, keep it as small as possible. If you are adding a new page to the
handbook, create the new page with a small amount of initial content, get it merged
quickly, and then add additional sections iteratively with subsequent merge requests.
Similarly, if you are adding a large feature to the software, create small, iterative merge requests for the different aspects. These merge requests might not provide immediate value, as long as they do not break anything and have appropriate tests for new functionality. If you aren't sure how to split something into small, iterative merge requests, consider kindly asking your team and/or maintainers for suggestions.
If you are asked to review a merge request that is too big, consider kindly asking
the author to split it into smaller merge requests before reviewing.

##### When we iterate slowly
{:.no_toc}
In some cases, rapid iteration can get in the way of [results](#results); for example, when
adjusting our marketing messaging (where consistency is key), product categories (where we've
set development plans), sales methodologies (where we've trained our teams) and this values page (where
we use the values to guide all GitLab team-members). In those instances, we add additional review to the approval
process; not to prohibit, but to be more deliberate in our iteration. The change process is documented on the page and takes place via merge request approvals.

#### See it in action
{:.no_toc}
Iteration is so important to GitLab that the CEO hosts [Iteration Office Hours](/handbook/ceo/#iteration-office-hours) to provide guidance and assist in breaking large, complex topics into MVCs and smaller iterations of work. 

1. [2019-11-19](https://www.youtube.com/watch?v=zwoFDSb__yM)
1. [2020-01-15](https://www.youtube.com/watch?v=FAuwri0vsts)
1. [2020-01-17](https://www.youtube.com/watch?v=DoDGlRrDwJc)
1. [2020-02-18](https://www.youtube.com/watch?v=liI2RKqh-KA)

You can view these on our [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg).

### 👁️ Transparency
{:#transparency .gitlab-purple}

Be open about as many things as possible. By making information
public, we can reduce the threshold to contribution and make collaboration easier.
Use public issue trackers, projects, and repositories when possible.

An example is the [public repository of this website](https://gitlab.com/gitlab-com/www-gitlab-com/)
that also contains this [company handbook](/handbook/). Everything we do is public by default, such as the [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/issues) and [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee/issues) issue trackers, as well as [marketing](https://gitlab.com/groups/gitlab-com/marketing/-/issues) and [infrastructure](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues). Transparency creates awareness for GitLab, allows us to recruit people that care about our values, gets us more and faster feedback from people outside the company, and makes it easier to collaborate with them. It is also about sharing great software, documentation, examples, lessons,
and processes with the **whole community** and the world in the spirit of open source, which we believe creates more value than it captures.

There are exceptions. Material that is [not public by default is documented](/handbook/communication/#not-public). We are above average at keeping things confidential that need to be. On a personal level, you should tell it like it is instead of putting up a poker face. Don't be afraid to admit you made a mistake or were wrong. When something goes wrong, it is a great opportunity to say "What’s the [kaizen](https://en.wikipedia.org/wiki/Kaizen) moment here?" and find a better way without hurt feelings.

Even as we [move towards becoming a public company](/company/strategy) and beyond, we know that our value of transparency will be key to our success. Often, company values get diluted as they grow, most likely because they do not write anything down. But we will make sure our values scale with the company. When we go public, we can declare everyone in the company as an insider, which will allow us to remain transparent internally about our numbers, etc. Everything else that can be transparent will continue to be so.

##### Public by default
{:.no_toc}
Everything at GitLab is public by default.
If something is not public, there should be a reference in the handbook that states a confidential decision was taken with a link to our Not Public guidelines, unless legal feels it carries undue risk.
The public process does two things: allows others to benefit from the conversation and acts as a filter. Since there is only a limited amount of time, we prioritize conversations that a wider audience can benefit from.

If you believe something shouldn't be public that currently is (or vice versa), then [make a merge request](/handbook/communication/#everything-starts-with-a-merge-request) to the relevant page(s) suggesting the change so that you can collaborate with others and discuss with the [DRI](/handbook/people-group/directly-responsible-individuals/).

##### Not public
{:.no_toc}
We make information public by default because [transparency is one of our values](/handbook/values/#transparency).
However it is [most important to focus on results](/handbook/values/#hierarchy).
Therefore, a category of information is **public** unless there is a reason for it not to be.

When information is not public, it may also be treated as limited access, only shared with certain GitLab roles, teams, or team members due to privacy considerations, contractual obligation, or other reasons that the author or DRI can specify.
Certain kinds of information default to limited access, including details about employees or customers who did not give permission to share the information.

We document what is [not public by default](/handbook/communication/#not-public) on our communication page. 

##### Directness
{:.no_toc}
Being direct is about being transparent with each other. We try to channel our inner [Ben Horowitz](https://en.wikipedia.org/wiki/Ben_Horowitz) by being [both straightforward
and kind, an uncommon cocktail of no-bullshit and no-asshole](https://medium.com/@producthunt/ben-horowitz-s-best-startup-advice-7e8c09c8de1b). Feedback is always about your work and not your person. That doesn't mean it will be easy to give or receive it.

##### Surface issues constructively
{:.no_toc}
Be transparent to the right people (up) at the right time (when still actionable). If you make a mistake, don't worry; correct it and **proactively** let the affected party, your team, and the CEO know what happened, how you corrected it, and how—if needed—you changed the process to prevent future mistakes.

##### Anyone and anything can be questioned
{:.no_toc}
Any past decisions and guidelines are open to questioning as long as you act in accordance with them until they are changed.

##### Disagree, commit, and disagree
{:.no_toc}
Everything can be questioned, but as long as a decision is in place, we expect people to commit to executing it, which is [a common principle](http://ryanestis.com/leadership/disagree-and-commit-to-get-things-done/).
Every decision can be changed;
our [best decision was one that changed an earlier one](https://youtu.be/4BIsON95fl8?t=2034).
In a manager-report circumstance, usually the report is the DRI.
The manager may disagree with the final decision, but they still commit to the decision of the DRI.

When you want to reopen the conversation on something, show that your argument is informed by previous conversations and [assume the decision was made with the best intent](/handbook/values/#assume-positive-intent).
You have to achieve results on every decision while it stands, even when you are trying to have it changed.
You should communicate with the [DRI](/handbook/people-group/directly-responsible-individuals/) who can change the decision instead of someone who can't.

##### Transparency is only a value if you do it when it is hard
{:.no_toc}
We practice transparency even when hiding the facts would be easier. For example, many companies do not give you the real reason why they declined your application because it increases the chance of legal action. We want to only reject people for the right reasons and we want to give them the opportunity to grow by getting this feedback. Therefore, we'll accept the increased risk of holding ourselves to a high standard of making decisions and do the right thing by telling them what we thought. Other examples are being transparent about [security incidents](http://disq.us/p/1r9gceh) and participating in and contributing to Live Broadcasts.

##### Single Source of Truth
{:.no_toc}
By having most company communications and work artifacts be public to the Internet, we have one single source of truth for all GitLab team members, users, customers, and other community members. We don‘t need separate artifacts with different permissions for different people.

##### Find the Limit
{:.no_toc}
We accept that we occasionally make mistakes in the direction of transparency. In other words, we accept it if we're sometimes publicizing information that should have remained confidential in retrospect. Most companies become non-transparent over time because they don't accept any mistakes. Making mistakes and reflecting on them means we know where the limit of transparency is.

##### Say why, not just what
{:.no_toc}
Transparent changes have the reasons for the change laid out clearly along with the change itself. This leads to fewer questions later on because people already have some understanding. A change with no public explanation can lead to a lot of extra rounds of questioning, which is less efficient. Avoid using terms such as "industry standard" or "best practices" as they are vague, opaque, and don't provide enough context as a reason for a change.

Similarly, merely stating a single value isn't a great explanation for why we are making a particular decision. Many things could be considered "iteration" or "efficiency" that don't match our definition of those values. Try to link to a sub-value of the value or provide more context, instead of just saying a single value's name.

Saying why and not just what enables discussion around topics that may impact more than one value; for instance, when weighing the [efficiency of boring solutions](/handbook/values/#boring-solutions) with the focus on [customer results](/handbook/values/#customer-results). When decisions align with all of our values, they are easy to discuss and decide. When there are multiple values involved, using our [values hierarchy](/handbook/values/#hierarchy) and [directly](/handbook/values/#directness) discussing the tradeoffs is easier with more context.

##### Reproducibility
{:.no_toc}
Enable everybody involved to come to the same conclusion as you. This not only involves [reasoning](#say-why-not-just-what), but also providing, for example: raw data and not just plots; scripts to automate tasks and not just the work they have done; and documenting steps while analyzing a problem. Do your best to make the line of thinking transparent to others, even [if they may disagree](/handbook/leadership/#making-decisions).

##### Accountability
{:.no_toc}
Increases accountability when making decisions and difficult choices.

##### Transparency Competency
{:#transparency-competency .no_toc}
[Competencies](/handbook/competencies/) are the Single Source of Truth (SSoT) framework for things we need team members to learn.
We demonstrate transparency when we are open with as many things as possible reducing the threshold to contribution and make collaboration easier.

| Level | Demonstrates Transparency Competency by... | Certification |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|
| Junior/Associate | Uses public issue trackers, projects, and repositories when possible. Looks for opportunities to publicly share the things that they are working on.  | thing |
| Intermediate | Provides context and background on projects and issues so that those with no prior knowledge are able to contribute to the discussion. They welcome feedback and new ideas as they know that will lead to a better solution.  | thing |
| Senior | Continually surfaces improvements across their functional area of expertise. They share feedback with others and understand how to disagree and commit to final solutions. They model what it means to be as open as possible. | thing |
| Staff/Manager | Implements open processes across their team. They also track team issues and projects openly so their team members are aware of everything that is happening on a team at a given time. They leverage feedback to drive the best possible outcomes with the information they have available. They also share feedback with their team and their peers in a timely, kind manner so their position on a given topic is known. | thing |
| Senior Manager | Fosters and coaches openness across cross functional departments. They leads cross functional issues, projects and ideas inviting feedback to generate the best possible solution. They hold their teams accountable to continue to find opportunities to share things openly. They give feedback to their team members, peers and managers in a timely, kind manner so their position on a topic is known. | thing |
| Director | Drives their departmental strategy with openness as a key value. They holds their management team accountable to working openly and pushes them to make everything transparent even when it might be difficult to do so. They coach managers on the value that additional feedback can bring to the end solution.  | thing |
| Senior Director | Develops leaders that work openly and continue to provide timely, kind feedback across their division. They develop leaders that drive their teams with openness as a foundational part of the way that they operate.  | thing |
| VP | Leads the company by being open in all things. They are open with things that might be traditionally not be shared broadly. They communicate directly and provide feedback in a timely manner to initiatives happening within their department and across the company. They hold the e group and other leaders accountable for upholding this value. | thing |
| EVP/CXO | Champions transparency both internally, across the company and externally. They participate both internally and externally in events and share the value that being open can provide to increasing trust with team members and others that interact with our product. They provide timely, kind feedback with initiatives happening internally and externally. They hold the e group and other leaders accountable for upholding this value. | thing |


## Five dysfunctions

Our values help us to prevent the [five dysfunctions](https://en.wikipedia.org/wiki/The_Five_Dysfunctions_of_a_Team#Summary):

1. **Absence of trust** Unwilling to be vulnerable within the group => _prevented by collaboration, specifically_ [kindness](#kindness)
1. **Fear of conflict** Seeking artificial harmony over constructive passionate debate => _prevented by transparency, specifically_ [directness](#directness) _and collaboration, specifically_ [short toes](#short-toes)
1. **Lack of commitment** Feigning buy-in for group decisions creates ambiguity throughout the organization => _prevented by transparency, specifically_ [directness](#directness)
1. **Avoidance of accountability** Ducking the responsibility to call peers on counterproductive behavior which sets low standards => _prevented by results, iteration, and_ [transparency](#transparency)
1. **Inattention to results** Focusing on personal success, status, and ego before team success => _prevented by_ [results](#results)

Some dysfunctions are not addressed directly by our values; for example, trust is not one of our values.
Similar to happiness, trust is something that is an outcome, not something you can strive for directly.
We hope that the way we work and our values will instill trust, instead of mandating it from people; trust is earned, not given.

## Why have values

Our values should give guidelines on how to behave and must be actionable.
They help us describe the type of behavior that we expect from people we hire.
They help us to know how to behave in the organization and what to expect from others.
Values are a framework for distributed decision making; they allow you to determine what to do without asking your manager.

## Why our values are public

Companies are encouraged to copy and implement GitLab's values. They are Creative Commons and can be copied verbatim.

We make our values public for the same [reasons](/company/culture/all-remote/hiring/#make-your-strategy-public) we make our [OKRs](/company/okrs/) (Objectives and Key Results) and [strategy](/company/strategy/) public. There is great power and efficiency in teams who share company values. Concealing values until *after* someone is hired into an organization is not a wise strategy.

Not everyone will see our values and feel aligned with them, and that's OK. By making values public, it shows respect for the time of job seekers who conduct due diligence on prospective employers. When people who *are* aligned with GitLab's values apply for an [open vacancy](/jobs/), this allows our hiring teams to more efficiently move candidates through the [interview process](/handbook/hiring/interviewing/).

In a [GitLab Unfiltered interview on values](https://youtu.be/7kMQj4O4ZGU), GitLab co-founder and CEO Sid Sijbrandij offers the following context.

> Companies may ask you to write a blank check. They'll say, 'Come join our organization, and when you're here, you need to subscribe to our values, our way of working, and our strategy. It's very essential, and it's part of our identity!'
>
> But these companies don't give you the opportunity up front to evaluate it. It doesn't make any sense to me. If it's so important that people share your values, have them out there.

## Hierarchy

Occasionally, values can contradict each other. For instance, transparency would dictate we publish all security vulnerabilities the moment they are found, but this would jeopardize our users. It's useful to keep in mind this hierarchy to resolve confusion about what to do in a specific circumstance, while remaining consistent with our core values.

![Values](/handbook/values/images/values.png)

In a [GitLab Unfiltered interview on values](https://youtu.be/7kMQj4O4ZGU), GitLab co-founder and CEO Sid Sijbrandij offers the following context.

> It's an attempt to relieve at least some of the tension. It's not absolute. If you think of values as binary, that's not going to work. There will always be interpretation, and there's always magnitude to consider.
>
> We made a hierarchy so that it's clear, in the end, the result matters most. For instance, we're not going to be transparent for the sake of being transparent. We're not radical in our transparency. We do it because we think it will lead to better outcomes.
>
> Those hierarchies are really important. They won't preempt every debate, but it helps.

## Updating our values

Our values are updated frequently and as needed. Everyone is welcome to make a suggestion to improve them. To update: make a merge request and assign it to the CEO. If you're a [team member](/company/team/) or in the [core team](/community/core-team/) please post a link to the MR in the `#values` Slack channel. If you're not part of those groups, please send a direct Twitter message to [@sytses](https://twitter.com/sytses).

## How do we reinforce our values

Whatever behavior you reward will become your values. We reinforce our values by:

1. What we expect from all team members, as [ambassadors for our values](/handbook/leadership/#gitlab-team-members).
1. What we refer to when [making decisions](/handbook/leadership/#making-decisions).
1. The example the E-group sets for the company since [a fish rots from the head down](https://thoughtmanagement.org/2011/11/21/does-the-fish-rot-from-the-head-down-when-organisations-go-toxic/).
1. What we select for during [hiring](/handbook/hiring/).
1. What we emphasize during [on-boarding](/handbook/general-onboarding/).
1. Behavior we give each other [360 feedback](/handbook/people-group/360-feedback/) on.
1. Behavior we [compliment](/handbook/communication/#say-thanks).
1. Criteria we use for [discretionary bonuses](/handbook/incentives/#discretionary-bonuses).
1. Criteria we use for our [annual compensation review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review).
1. Criteria we use for [promotions](/handbook/people-group/promotions-transfers/).
1. Criteria we use to [manage underperformance](/handbook/underperformance/).
1. What we do when we [let people go](/handbook/offboarding/).
1. Giving value awards during [Contribute](/company/culture/contribute/).
1. Keeping them [up to date](/handbook/values/#updating-our-values) with a [stream of commits that add details](https://gitlab.com/gitlab-com/www-gitlab-com/-/commits/master/source/handbook/values/index.html.md).

In negative feedback, we should be specific about what the problem is. For example, saying someone is "[not living the values](https://hbr.org/2017/05/how-corporate-values-get-hijacked-and-misused)" isn't helpful.

> Your values are what you hire for, what you praise people for, and what you promote them for. By definition, what you do in those instances *are* your values. It's not what you say they are. Values should be explicitly part of our [hiring](/handbook/hiring/) process, our job profiles, and our [review process](/handbook/people-group/360-feedback/).
>
> When we give [bonuses](/handbook/incentives/#discretionary-bonuses) and [promotions](/handbook/people-group/promotions-transfers/), they are always linked to values. That's the crucial thing. If you reinforce them there, that's the most powerful thing you can do. — *Sid Sijbrandij, GitLab co-founder and CEO*

## What to do if values aren't being lived out

Value erosion can occur when indifference and apathy are tolerated. If you feel that values are not being lived out in a given scenario, speak up and ask for context in a respectful manner. Offer links to relevant values and/or sub-values when discussing the issue.

If there is confusion or disagreement about the interpretation of a value, please surface the discussion in GitLab's `#values` Slack channel (for GitLab team members) or @-mentioning  [@gitlab](https://twitter.com/gitlab) on Twitter (for those who do not work at GitLab).

In a [GitLab Unfiltered interview on values](https://youtu.be/7kMQj4O4ZGU), GitLab co-founder and CEO Sid Sijbrandij offers the following context.

> Almost every time we face a hard decision at GitLab, it's because values are in conflict. It's not binary logic. It requires conversation, and sometimes there is no obvious answer. We can only achieve resolution by respectfully talking with each other and trusting the DRI to make the ultimate decision.

## Permission to play

From our values we excluded some behaviors that are obvious; we call them our *permission to play* behavior:

1. Be truthful and honest.
1. Be dependable, reliable, fair, and respectful.
1. Be committed, creative, inspiring, and passionate.
1. Be deserving of the trust of our users and customers.
1. Act in the best interest of the company, our team members, our customers, users, and investors.
1. Act in accordance with the law.
1. Don't show favoritism as [it breeds resentment, destroys employee morale, and creates disincentives for good performance](https://www.employmentlawfirms.com/resources/employment/discrimination/laws-preventing-favoritism-in-the-workplace). Seek out ways to be fair to everyone.

## What is not a value

- [All-remote](/company/culture/all-remote/) isn't a value. It is something we do because it helps to [practice our values](/company/culture/all-remote/values/) of transparency, efficiency, diversity & inclusion, and results.

## Questions from new team members

During every [GitLab 101 session with new hires](/company/culture/gitlab-101/) we discuss our values. We document the questions and answers to [Frequently Asked Questions about the GitLab Culture](/company/culture/gitlab-101/#frequently-asked-questions-about-the-gitlab-culture).

New team members should read [GitLab's guide to starting a new remote role](/company/culture/all-remote/getting-started/), and reference [interviews](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc) centered on values within the [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/search?query=values).

## Mission

Our [mission](/company/strategy/#mission) is that **everyone can contribute**. This mission guides our path, and we live our values along that path.

## Biggest risks

We have a page which documents our [biggest risks](/handbook/leadership/biggest-risks/). Many of our [values](/handbook/values/) help to mitigate some of these risks.

## GitLab Values Certification

Anyone can become certified in our GitLab values. To obtain certification, you will need to complete this [quiz](https://docs.google.com/forms/d/e/1FAIpQLSfuIrMYDMS14OZo8zqMnkk6yiKDhzcnhH7f82aHTOMWHS5uvg/viewform) and earn at least an 80%. Once the quiz has been passed, you will receive an email with your certification that you can share on your personal LinkedIn or Twitter pages. If you have questions, please reach out to our L&D team at `learning@gitlab.com`.
