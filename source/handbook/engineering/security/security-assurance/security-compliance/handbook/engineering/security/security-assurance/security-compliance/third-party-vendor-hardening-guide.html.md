---
layout: markdown_page
title: "Third Party Vendor Hardening Guide"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Third Party Vendor Hardening Guide Overview
Data at GitLab is [classified](/handbook/engineering/security/data-classification-policy.html) 
based on its sensitivity, which is the level of confidentiality that information
requires to protect against unauthorized use. Customer data, personally identifiable
information of users and team members, and internal information are some examples 
of the more sensitive (i.e. Yellow, Orange, Red) and thus engagements involving 
these types undergo risk assessments and vendor security reviews to identify and
manage risks. This guide should be referenced any time non-public data will be 
handled, managed, stored, or transmitted to third parties or fourth parties.

## Best Practices

Data should be kept on GitLab systems whenever possible and professional service
contractors should only access public data and communication channels. For all 
else the following are steps that business owners can work with their vendors 
processing or storing senstitive data uphold GitLab's security standards:

✅ vendor should participate in independent third party technical assessments

✅ vendor ensures all risks are addressed within a timely fashion, prioritizing 
 critical and high risks

✅ The system(s) use the minimum encryption standards for data in transit and at rest as defined in our [security controls](hhandbook/engineering/security/sec-controls.html#data-management)

✅ protect from unauthorized access by following the [least privilege principle](hhandbook/engineering/security/#principle-of-least-privilege).
  For GitLab team members this means to minimize access to only those people who have 
  been previously authorized to access that data and have a legitimate need for 
  that access. As with regular team members, access granted to third parties should be at the lowest level possible to 
 accomplish the specific need

✅ sensitive data should be erased immediately upon our request or when a specific 
 need for that data no longer exists (whichever comes first)

✅ if vendor is providing contractor services that includes access 
 to sensitive data via email or Slack, and/or access to GitLab systems that houses
 sensitive data, the vendor must undergo equivalent onboarding activities as a 
 regular GitLab team member. This includes background checks, security awareness
 training and review the [code of conduct](/handbook/people-group/code-of-conduct/)
 
✅ when sharing information externally is absolutely necessary utilize the granular permissions provided by Google Drive 

✅ enable 2-Factor authentication when possible