---
layout: handbook-page-toc
title: "GitLab PCI Compliance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is PCI?

PCI is the shorthand for the Payment Card Industry's Data Security Standard (PCI-DSS) as defined by the [PCI security standards council](https://www.pcisecuritystandards.org/). The PCI-DSS defines the requirements of businesses that accept or facilitate credit card payments based on type or amount of transactions accepted or facilitated.

## Glossary of terms

A [full list](https://www.pcisecuritystandards.org/pci_security/glossary) of of the PCI DSS glossary, abbreviations, and acronyms.

| Term                                | Definition                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|-------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ASV                                 | Acronym for “Approved Scanning Vendor.” Company approved by the PCI SSC to conduct external vulnerability scanning services.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| Card Verification Code or Value     | Also known as Card Validation Code or Value, or Card Security Code. Refers to the printed security features.<br><br> * For Discover, JCB, MasterCard, and Visa payment cards, the second type of card verification value or code is the rightmost three-digit value printed in the signature panel area on the back of the card. For American Express payment cards the code is a four-digit unembossed number printed above the PAN on the face of the payment cards. The code is uniquely associated with each individual piece of plastic and ties the PAN to the plastic.<br><br> The following list provides the terms for each card brand:<br>    * CID - Card Identification Number (American Express and Discover payment cards)<br>    * CAV2 - Card Authentication Value 2 (JCB payment cards)<br>    * PAN CVC2 - Card Validation Code 2 (MasterCard payment cards)<br>    * CVV2 - Card Verification Value 2 (Visa payment cards) |
| Cardholder Data (CHD)               | Any personally identifiable information (PII) associated with a person who has a credit or debit card.<br><br>This data is, at a minimum:<br>* Primary account number (PAN) - `this is the credit card number`<br><br>And can additionally include:<br> * Card Verification Code<br> * Cardholder name<br> * Expiration date<br> * Magnetic Stripe Data/Track Data<br> * PIN<br> * Sensitive Authentication Data<br> * Service Code<br><br> If any combination of these are stored, processed, or transmitted with the PAN, they must be protected in compliance with PCI DSS requirements.                                                                                                                                                                                                                                                                                                                                                   |
| CDE                                 | Acronym for `cardholder data environment.` The people, processes and technology that store, process, or transmit cardholder data or sensitive authentication data                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| Cloud Service Provider ("Provider") | It is the entity providing the cloud service. It acquires and manages the infrastructure required for providing the services, runs the cloud software that provides the services and delivers the cloud services through network access                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| Cloud Service Customer ("Customer") | The entity subscribing to a service provided by a Provider. May include merchants, service providers, payment processors and other entities utilizing cloud services                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| Cloud Service User                  | Person, or entity acting on his or her behalf, associated with a Customer that uses cloud services.<br><br>**Note:**   _Examples of such entities include devices and applications._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| Magnetic-Stripe Data                | See Track Data below.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| PAN                                 | Acronym for `primary account number` and also referred to as `account number.` Unique payment card number (typically for credit or debit cards) that identifies the issuer and the particular cardholder account                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| PIN                                 | Acronym for `personal identification number.` Secret numeric password known only to the user and a system to authenticate the user to the system. The user is only granted access if the PIN the user provided matches the PIN in the system. Typical PINs are used for automated teller machines for cash advance transactions. Another type of PIN is one used in EMV chip cards where the PIN replaces the cardholder’s signature.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| Sensitive Authentication Data       | Security-related information (including but not limited to card validation codes/values, full track data (from the magnetic stripe or equivalent on a chip), PINs, and PIN blocks) used to authenticate cardholders and/or authorize payment card transactions                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| SAQ                                 | Acronym for `Self-Assessment Questionnaire.` Reporting tool used to document self-assessment results from an entity’s PCI DSS assessment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Scoping                             | Process of identifying all system components, people, and processes to be included in a PCI DSS assessment. The first step of a PCI DSS assessment is to accurately determine the scope of the review.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| Server                              | Computer that provides a service to other computers, such as processing communications, file storage, or accessing a printing facility. Servers include, but are not limited to web, database, application, authentication, DNS, mail, proxy, and NTP.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| Service Code                        | Three-digit or four-digit value in the magnetic-stripe that follows the expiration date of the payment card on the track data. It is used for various things such as defining service attributes, differentiating between international and national interchange, or identifying usage restrictions.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| Service Provider                    | Business entity that is not a payment brand, directly involved in the processing, storage, or transmission of cardholder data on behalf of another entity. This also includes companies that provide services that control or could impact the security of cardholder data. Examples include managed service providers that provide managed firewalls, IDS and other services as well as hosting providers and other entities. If an entity provides a service that involves only the provision of public network access—such as a telecommunications company providing just the communication link—the entity would not be considered a service provider for that service (although they may be considered a service provider for other services).                                                                                                                                                                                           |
| Track Data                          | lso referred to as `full track data` or `magnetic-stripe data.` Data encoded in the magnetic stripe or chip used for authentication and/or authorization during payment transactions. Can be the magnetic-stripe image on a chip or the data on the track 1 and/or track 2 portion of the magnetic stripe                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| Transaction Data                    | Data related to electronic payment card transaction.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| Truncation                          | Method of rendering the full PAN unreadable by permanently removing a segment of PAN data. Truncation relates to protection of PAN when stored in files, databases, etc. See Masking for protection of PAN when displayed on screens, paper receipts, etc.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| Trusted Network                     | Network of an organization that is within the organization’s ability to control or manage.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| Untrusted Network                   | Network that is external to the networks belonging to an organization and which is out of the organization’s ability to control or manage.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| VLAN                                | Abbreviation for `virtual LAN` or `virtual local area network.` Logical local area network that extends beyond a single traditional physical local area network.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |


## What is PCI (cardholder) data?

PCI data refers to the cardholder data, which is what the PCI DSS was created to protect. The following data values identify cardholder data in any combination:

* PAN
* Card Verification Code
* Magnetic Stripe Data
* PIN
* Sensitive Authentication Data
* Service Code

*Descriptions of each are available in the above Glossary section*

Wherever any of these data values are stored is in-scope for PCI compliance along with any system connected-to that storage location.

## PCI Storage Requirements

Please Note: `Storage of cardholder data (CHD) is permissable but will expand the number of requirements to be PCI compliant beyond what is listed here.`

<table>
  <tr>
    <th></th>
    <th></th>
    <th>Data Element</th>
    <th>Storage Permitted</th>
    <th>Render Stored Data Unreadable<br>per Req. 3.4</th>
  </tr>
  <tr>
    <td rowspan="7">Account<br>Data</td>
    <td rowspan="4">Cardholder<br>Data</td>
    <td>Primary Account Number (PAN)</td>
    <td>Yes</td>
    <td>Yes</td>
  </tr>
  <tr>
    <td>Cardholder Name</td>
    <td>Yes</td>
    <td>No</td>
  </tr>
  <tr>
    <td>Service Code</td>
    <td>Yes</td>
    <td>No</td>
  </tr>
  <tr>
    <td>Expiration Date</td>
    <td>Yes</td>
    <td>No</td>
  </tr>
  <tr>
    <td rowspan="3">Sensitive<br>Authentication<br>Data</td>
    <td>Full Track Data (data on chip)</td>
    <td>No</td>
    <td>Cannot store per Req. 3.2</td>
  </tr>
  <tr>
    <td>CAV2/CVC2/CVV2/CID</td>
    <td>No</td>
    <td>Cannot store per Req. 3.2</td>
  </tr>
  <tr>
    <td>PIN/PIN Block</td>
    <td>No</td>
    <td>Cannot store per Req. 3.2</td>
  </tr>
</table>

PCI Req. 3.4
* Render PAN unreadable anywhere it is stored (including on portable digital media, backup media, and in logs) by using any of the following approaches:
  * One-way hashes based on strong cryptography, (hash must be of the entire PAN)
  * Truncation (hashing cannot be used to replace the truncated segment of PAN)
  * Index tokens and pads (pads must be securely stored)
  * Strong cryptography with associated key-management processes and procedures

PCI Req. 3.2
* Do not store sensitive authentication data after authorization (even if encrypted). If sensitive authentication data is received, render all data unrecoverable upon completion of the authorization process.

## What PCI data is non-sensitive?

There are data values defined as part of CHD listed in sections above, that by themselves, are not considered CHD outside of being stored with the PAN:

* Cardholder name
* Email address
* Expiration date
* Address
* State
* Country
* Zip code
* Company name

Please Note: Even though they are not identified as cardholder data without the associated PAN, some data values are still PII and must be securely stored.

## Tokenization of PAN

One of the primary goals of a tokenization solution should be to replace sensitive PAN values with non-sensitive token values. For a token to be considered non-sensitive, and thus not require any security or protection, the token must have no value to an attacker.

Examples:

|  PAN | Token | Comment |
| --- | --- | --- |
|  3124 005917 23387 | 7aF1Zx118523mw4cwl5x2 | Token consists of alphabetic and<br/>numeric characters |
|  4959 0059 0172 3389 | 729129118523184663129 | Token consists of numeric<br/>characters only |
|  5994 0059 0172 3383 | 599400x18523mw4cw3383 | Token consists of truncated PAN<br/>(first 6, last 4 of PAN are retained)<br/>with alphabetic and numeric<br/>characters replacing middle digits. |

`To be considered out of scope for PCI DSS, both the tokens and the systems they reside on would need to have no value to an attacker attempting to retrieve PAN, nor should they in any way be able to influence the security of cardholder data or the CDE.`

At GitLab we fully outsource our payment processor to a third-party and will not retain or have access to customer PAN or tokens.

Additional information - [PCI DSS Tokenization Guidelines](https://www.pcisecuritystandards.org/documents/Tokenization_Guidelines_Info_Supplement.pdf)

## Utilizing APIs within PCI compliance

APIs and other public interfaces should be designed to prevent both accidental misuse and malicious attempts to bypass security controls.

Examples of controls that should be in place to protect these interfaces:

*  Resilient authentication and access controls
*  Strong cryptography
*  real-time monitoring

## What are GitLab's requirements for PCI compliance?

GitLab is an open core solution that provides both a free and tiered paid plans that primarily utilizes credit card payments for subscriptions. Because of the low number of credit card transactions GitLab is involved with and the fact that we use a third-party payment processor, GitLab is currently a level 4 merchant for PCI. Level 4 merchants are required to complete an annual self-attestation questionnaire (SAQ) and since we fully outsource payment processing, specifically the SAQ-A. Quarterly scanning of our PCI systems must also be performed by an approved scanning vendor (ASV), GitLab uses Tenable.io.

## What is GitLab's current state of PCI compliance?

GitLab has completed an SAQ-A form and regularly performs scans of our in-scope PCI systems by an ASV. The SAQ-A form (and associated Attestation of Compliance (AoC)) and the results of our ASV scans are available upon request. Please send an email to [security@gitlab.com](mailto:security@gitlab.com) to request these documents.

## What is considered a PCI system?

Determining how a system may interact with the payment processing system is important when defining which systems are in-scope for PCI compliance.  The following diagram demonstrates in-scope (connected-to) vs. out-of-scope (not connected-to) when defining the PCI environment:

```mermaid
graph TD
  subgraph "PCI Scoping"
  Y[In Scope]
  Z[Out-of-Scope]
  end
  
style Y fill:#F34949
style Z fill:#24CD19
```

```mermaid
graph TD
  subgraph "CDE Systems"
  C[System component stores,<br/>processes, or transmits<br/>CHD/SAD]
  D[System component is on<br/>the same network segment]
  end

 B[In Scope for PCI DSS]
  B --> C
  B --> D

style C fill:#F34949
style D fill:#F34949
```

```mermaid
graph TD
  subgraph "Connected-to or Security-impacting Systems"
  C[System component stores, processes, or transmits CHD/SAD] --> |or|D
  D[System component is on<br/>the same network segment]--> |or|E
  E[System component impacts configuration or security of the CDE]--> |or|F
  F[System component provides security services to the CDE]--> |or|G
  G[System component segments CDE systems for out-of-scope systems and networks]--> |or|H
  H[System component supports PCI DSS requirements]
  end

  B[In Scope for PCI DSS]
  B --> C

style C fill:#F34949
style D fill:#F34949
style E fill:#F34949
style F fill:#F34949
style G fill:#F34949
style H fill:#F34949

```

```mermaid
graph LR
subgraph "Out-of-Scope Systems"
  C[System component does<br/>NOT store, process,<br/>or transmit CHD/SAD] --> |and|D
  D[System component is NOT in<br/>the same network segment<br/>as systems that store,<br/>process, or transmit CHD/SAD]--> |and|E
  E[System component<br/>CANNOT connect ot any<br/>system in the CDE]--> |and|F
  F[System component does<br/>NOT meet any criteria<br/>described for connected-to<br/>or security-impacting<br/>systems, per above]
  end

  B[Out-of-Scope Systems]
  B --> C

  style C fill:#24CD19
  style D fill:#24CD19
  style E fill:#24CD19
  style F fill:#24CD19
```

### Example 1 - "Connected-to" Shared Services

```mermaid
graph TD
  subgraph "PCI Scoping"
  Y[In Scope]
  Z[Out-of-Scope]
  end

  subgraph "Shared Services"
  I[Directory<br/>Services] --- O
  J[Patching<br/>Server] --- O
  K[Logging<br/>Server] --- O
  L>Admin<br/>Workstation] --- O
  O(Switch) --- N{Firewall}
  end

  subgraph "CDE"
  R[Server] --- Q
  S>POS] --- Q
  T>POS] --- Q
  U>POS] --- Q
  V[POS Server] --- Q
  Q(Switch) --- P{Firewall}
  end
  
  P --> A
  N --> A
  A((Router)) --- B(Switch)
  B --> C(Switch)

  subgraph "Corporate LAN"
  C --- D[Server 1]
  C --- E>Laptop 1]
  C --- F>Laptop 2]
  C --- G>Laptop 3]
  C --- H[Server 2]
  end

style C fill:#24CD19
style D fill:#24CD19
style E fill:#24CD19
style F fill:#24CD19
style G fill:#24CD19
style H fill:#24CD19
style I fill:#F34949
style J fill:#F34949
style K fill:#F34949
style L fill:#F34949
style N fill:#F34949
style O fill:#F34949
style P fill:#F34949
style R fill:#F34949
style S fill:#F34949
style T fill:#F34949
style U fill:#F34949
style V fill:#F34949
style Q fill:#F34949
style Y fill:#F34949
style Z fill:#24CD19
```

### Example 2 - Administration of CDE Systems from a SecurityImpacting System in the Corporate LAN

```mermaid
graph TD
  subgraph "PCI Scoping"
  Y[In Scope]
  Z[Out-of-Scope]
  end

  subgraph "Shared Services with Jumpbox"
  I[Jumpbox<br/>Services] --- O
  J[Directory<br/>Services] --- O
  K[Logging<br/>Server] --- O
  L>Admin<br/>Workstation] --- O
  O(Switch) --- N{Firewall}
  end

  subgraph "CDE"
  R[Server] --- Q
  S>POS] --- Q
  T>POS] --- Q
  U>POS] --- Q
  V[POS Server] --- Q
  Q(Switch) --- P{Firewall}
  end
  
  P --- |no connection| C
  E === |Admin traffic from WS to Jumpbox| I
  Q === |Jumpbox to CDE| O
  C --- |Out-of-Scope Traffic| I

  subgraph "Corporate LAN"
  C(Switch) --- D[Server 1]
  C --- E>Admin WS<br/>Security-impacting<br/>system]
  C --- F>Laptop 2]
  C --- G>Laptop 3]
  C --- H[Server 2]
  end



style C fill:#24CD19
style D fill:#24CD19
style E fill:#F34949
style F fill:#24CD19
style G fill:#24CD19
style H fill:#24CD19
style I fill:#F34949
style J fill:#F34949
style K fill:#F34949
style L fill:#F34949
style N fill:#F34949
style O fill:#F34949
style P fill:#F34949
style R fill:#F34949
style S fill:#F34949
style T fill:#F34949
style U fill:#F34949
style V fill:#F34949
style Q fill:#F34949
style Y fill:#F34949
style Z fill:#24CD19
```

**Please Note:**

*  Connected-to systems create a buffer for the out-of-scope systems from directly connecting to the payment processing system.
*  It does not matter where a system resides (i.e. different cloud environment), if it communicates with the payment processing system, it is in-scope for PCI.

## PCI Cloud Segmentation

Once any layer of the cloud architecture is shared by CDE and non-CDE environments, segmentation becomes increasingly complex. This complexity is not limited to shared hypervisors; all layers of the infrastructure that could provide an entry point to a CDE must be included when verifying segmentation.

The Customer (GitLab) is responsible for the proper configuration of any segmentation controls implemented within its own environment (for example, using virtual firewalls to separate in-scope VMs from out-of-scope VMs), and for ensuring that effective isolation is maintained between in-scope and out-of-scope components.

## How to determine if a system or data is in-scope for PCI at GitLab

When determining if a system or data is in-scope for PCI, ask yourself the following questions: (refer to glossary section above for definitions)

1. Is the system directly connected to customers.gitlab.com?
2. Does the system store, process, or transmit cardholder data or sensitive authentication data?
3. Is the system used to support `shared services` (i.e. SIEM, security logging, directory services, NTP, or DHCP) of the cardholder data environment (CDE) or connected-to systems through a firewall and/or jump box?
4. Is the system a workstation utilized for system administration functionality on customers.gitlab.com?

If you answered yes to any of these questions, the system **is** in-scope for PCI compliance.

## GitLab's PCI Environment

GitLab's payment processing system is customers.gitlab.com which hosts a Zuora iFrame to collect payment information from a customer which creates/updates a subscription account managed by Zuora which passes the customers credit card information to Stripe to process the payment.  Once the payment is successful, the number of licenses a customer paid for will be generated to allow use of GitLab's feature tier selected.  customers.gitlab.com is the centerpoint of credit card payment processing where PCI begins at GitLab.

```mermaid
graph TB
  Node10[External Facing Host]
  Node11[Internal Facing Host]

  SubGraph4Flow
  subgraph "Out-of-Scope C"
  SubGraph4Flow(System E)
  end

  SubGraph2Flow
  subgraph "Out-of-Scope B"
  SubGraph2Flow(System D)
  end

  SubGraph1Flow
  subgraph "Out-of-Scope A"
  SubGraph1Flow(System C)
  end

  subgraph "In-Scope for PCI"
  SubGraph3Flow[customers.gitlab.com] -- GitLab Internal OAuth --- Node2[dev.gitlab.org]
  SubGraph3Flow[customers.gitlab.com] -- GitLab License Managmement --- Node17[license.gitlab.com]
  SubGraph3Flow[customers.gitlab.com] -- GitLab Configuration Management --- Node3[Chef Server]
  SubGraph3Flow[customers.gitlab.com] -- Security Logging --- Node7[Gitlab-PCI]
  Node2 --- SubGraph1Flow(System C)
  Node3 --- SubGraph2Flow(System D)
  Node17 --- SubGraph4Flow(System E)
  end
  

  subgraph "External PCI Vendors"
  Node4[Stripe] -- Zuora iFrame --- SubGraph3Flow[customers.gitlab.com]
  Node5[Zuora] -- Zuora iFrame --- SubGraph3Flow[customers.gitlab.com]
  Node20[SalesForce] --- SubGraph3Flow[customers.gitlab.com]
  sub[Shopify-swag.gitlab.com]
  end
  
  style SubGraph3Flow fill:#F34949
  style Node11 fill:#24CD19
  style Node10 fill:#F34949
  style Node17 fill:#F34949
  style Node20 fill:#F34949
	style Node7 fill:#24CD19 
  style Node5 fill:#F34949
  style Node4 fill:#F34949
  style Node3 fill:#F34949
  style Node2 fill:#F34949
  style sub fill:#F34949
  style SubGraph2Flow fill:#24CD19
  style SubGraph4Flow fill:#24CD19
  style SubGraph1Flow fill:#24CD19
```

### Systems Detail 

| System               	| Purpose                                                                                                                 	| Location      	| Scope            	| Reasoning                                                                                                 	|
|----------------------	|-------------------------------------------------------------------------------------------------------------------------	|---------------	|------------------	|-----------------------------------------------------------------------------------------------------------	|
| customers.gitlab.com 	| GitLab system for providing customers the ability to create and pay for tiered product subscriptions with a credit card 	| Azure         	| PCI              	| Displays the Zuora iFrame to create/manage subscriptions and to process credit card payments using Stripe 	|
| dev.gitlab.org       	| Gitlab system that hosts the GitLab team member OAuth                                                                   	| Azure         	| Connected-To PCI 	| Provides GitLab team members secure access to customers.gitlab.com to maintain and support                	|
| Chef Server          	| GitLab system for configuration management                                                                              	| Digital Ocean 	| Connected-To PCI 	| Provides configuration management for customers.gitlab.com                                                	|
| gitlab-pci          	| GitLab system for external logging of customers.gitlab.com (fully segmented GCP project)                                	| GCP           	| Connected-To PCI 	| Logs of customers.gitlab.com must be shipped off the system in a secured environment                      	|
| license.gitlab.com   	| GitLab license management system                                 	| AWS           	| Connected-To PCI 	| Generates licenses that allows customers to run GitLab instances                      	|
| SalesForce               	| Third-party customer resourcee management platform                                                                                          	| SalesForce        	| Connected-To PCI              	| Ingests customer subscription information from customers.gitlab.com                                                                                         	|
| Stripe               	| Third-party payment processor                                                                                           	| Stripe        	| PCI              	| Payment processor                                                                                         	|
| Zuora                	| Third-party subscription management platform                                                                            	| Zuora         	| PCI              	| Subscription management platform                                                                          	|
| Shopify              	| Third-party payment processor and swag.gitlab.com host                                                                  	| AWS           	| PCI              	| Hosts swag.gitlab.com                                                                                     	|

## Service Provider Matrix

This is a list of service providers who handle credit card transactions on behalf of GitLab:

|  Company/Product Name | Service Provided | Risk Assessment | Data Shared | Data Encryption Used | Written Agreement | PCI Validation | Expiration Date | Requirements Responsible For |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  Shopify | E-Commerce Platform | Shopify Risk Acceptance | email address, first and last name, shipping address, phone number, and credit card information | in-transit: TLS 1.2+, at-rest: Merchant and customer data is encrypted at rest and sensitive information is further encrypted at the application layer. | [Shopify ToS](https://www.shopify.com/legal/terms) | Shopify AOC | 2019-7-21 | 6 and 7 |
|  Stripe | Payment Processor | Stripe DPIA |  | in-transit: TLS 1.2; at-rest: AES-256 | [Stripe SSA](https://stripe.com/ssa) | Stripe AOC | 2020-2-29 | 6, 7, 11 and 12 |
|  Zuora | E-Subscription Platform | Zuora DPIA |  | in-transit: AES 256 TLS; at-rest: server-side encryption (SSE) with AWS Key Management Service (SSE-KMS) | Zuora MSA (on file) | Zuora AOC | 2019-6-27 | 5, 6, 7, 10, 11 and 12 |


#### Matrix Key

The following table correlates each column of data back to its specific control:

<table class="tg">
  <tr>
    <th class="tg-uys7">Column</th>
    <th class="tg-uys7">Control</th>
  </tr>
  <tr>
    <td class="tg-baqh">Company/Product Name</td>
    <td class="tg-s6z2" rowspan="2"><a href="https://about.gitlab.com/handbook/engineering/security/guidance/TPM.1.01_third_party_assurance_review.html">TPM.1.01 - Third Party Assurance Review</a></td>
  </tr>
  <tr>
    <td class="tg-baqh">Service Provided</td>
  </tr>
  <tr>
    <td class="tg-uys7">Risk Assessment</td>
    <td class="tg-uys7" rowspan="3"><br><a href="https://about.gitlab.com/handbook/engineering/security/guidance/TPM.1.02_vendor_risk_management.html">TPM.1.02 - Vendor Risk Management</a></td>
  </tr>
  <tr>
    <td class="tg-uys7">Data Shared</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Data Encryption Used</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Written Agreement</td>
    <td class="tg-c3ow"><a href="https://about.gitlab.com/handbook/engineering/security/guidance/TPM.2.02_vendor_non-disclosure_agreements.html">TPM.2.02 - Vendor Non-Disclosure Agreement</a></td>
  </tr>
  <tr>
    <td class="tg-c3ow">PCI Validation<br></td>
    <td class="tg-uys7" rowspan="3"><a href="https://about.gitlab.com/handbook/engineering/security/guidance/TPM.1.04_vendor_compliance_monitoring.html">TPM.1.04 - Vendor Compliance Monitoring</a></td>
  </tr>
  <tr>
    <td class="tg-baqh">Expiration Date</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Requirements Responsible For</td>
  </tr>
</table>

## ASV Scanning

PCI Requirement 11.2 requires periodic unauthenticated scanning of a merchants externally-facing in-scope PCI environment to identify vulnerabilities that can potentially be exploited. The PCI-DSS council certifies Approved Scanning Vendors (`ASV`) to provide the ability to scan a merchants external PCI environment and provide an independent review of potential risk. ASV scanning is conducted on a quarterly basis and must demonstrate results that pass a third-party assessment which is completed by the ASV. For GitLab that is Tenable.io.

[PCI ASV Program Guide](https://www.pcisecuritystandards.org/documents/ASV_Program_Guide_v3.0.pdf)

### Vulnerability Reporting

#### Report Submission

Each quarter the merchant must submit to the ASV scan results to validate the external PCI environment is in compliance by the report not containing "High" or "Medium" severity vulnerabilities or any features or configurations that conflict with PCI-DSS.  Vulnerability severity levels are based on the National Vulnerability Database (`NVD`) and Common Vulnerability Scoring System (`CVSS`).

Report submissions for ASV attestation will occur on the following dates each year(coinciding with the GitLab fiscal calendar):
* February 1st
* May 1st
* October 1st
* January 1st

{-Please Note-}: First submission will be made by 2020-06-01 to allow for remediation timeframe to be completed.

The four previous passing scans will be submitted to requesting PCI acquirer with the latest PCI SAQ-A in the month of February.

#### Meeting External Scan Compliance

Vulnerability reports must be able to demonstrate compliance on two levels:

* Overall compliance of the merchant's external PCI environment
* No vulnerability with an assigned CVSS score of 4.0 or higher

#### Report Format

The scan report must have the following three sections:
1. Attestation of Scan Compliance
   * This is the overall summary that shows whether the scan customer’s infrastructure met the scan requirements and received a passing scan. 
2. ASV Scan Report Summary
   * This section of the scan report lists vulnerabilities by component (IP address, hosts/virtual hosts, domains, FQDN, etc.) and shows whether each component scanned received a passing score and met the scan requirement.
3. ASV Scan Vulnerability Details
   * This section of the scan report is the overall listing of vulnerabilities that shows compliance status (pass/fail) and details for *all* vulnerabilities detected during the scan.

#### Scan Results

A completed scan has one of four results:

1. A passing scan 👍`This is the only result that will be accepted to comply with quarterly external ASV requirement`
2. A failing scan for which the scan customer disputes the results ⚠️
3. A failing scan that the scan customer does not dispute 👎
4. A failing scan due to scan interference ❓

#### False Positives

The scan customer may dispute the findings in the ASV scan report including, but not limited to:
* Vulnerabilities that are incorrectly reported (false positives)
* Vulnerabilities that have a disputed CVSS Base score
* Vulnerabilities for which a compensating control is in place
* Exceptions in the scan report
* Conclusions of the scan report
* List of components designated by scan customer as segmented from the CDE
* Inconclusive ASV scans or ASV scans that cannot be completed due to scan interference

A dispute must be submitted in writing and can include screen captures, configuration files, system versions, file versions, list of installed patches, etc.

#### Compensating Controls

Another method of results dispute is demonstrating the use and implementation of compensating controls of identified scan results by the ASV. 

#### ASV Scanning Workflow Diagram

```mermaid
graph TD
	A(Determine Scan Scope) --> B[Provide list of identified<br/>external facing IPs to ASV]
	B --> C[Attest that scan scope includes<br/>all in-scope systems for PCI]
	C --> D[Configure active prevention systems<br/>from interfering with ASV scan]
  D --> E[Perform discovery scan]
  E --> F{Find web servers<br/>without domains or components<br/>not provided by scan customer?}
  F --> |Yes| G{Attest that component<br/>out of scope due to<br/>network segmentation}
  F --> |No| I
  G --> |No| B
  G --> |Yes| H[Attest that network<br/>segmentation is in place]
  H --> I[Perform ASV Scan]
  I --> J[ASV attests that<br/>PCI and ASV QA processes<br/>were followed]
  J --> K{PCI DSS<br/>compliant scan?}
  K --> |Yes| L>Compliant Scan Report]
  L --> M(Submit report<br/>as directed by acquirer)
  K --> |No| N{Dispute Scan Result?}
  N --> O>Non-Compliant Scan Report]
  O --> P[Fix issues and rescan<br/>until passing scan received]
  N --> |Yes| Q[Provide supporting evidence<br/>for dispute findings]
  Q --> R[Attest that all evidence submitted<br/>is accurate and complete]
  R --> S{Can ASV remotely validate evidence?}
  S --> |No| T{Is evidence sufficent?}
  T --> |No| P
  T --> |Yes| U[Any exceptions, special notes, etc.<br/>are included in the report]
  S --> |Yes| U
  U --> M

style A fill:#99ccff
style G fill:#ffff99
style F fill:#ffff99
style K fill:#ffff99
style N fill:#ffff99
style S fill:#ffff99
style T fill:#ffff99
style L fill:#24CD19
style O fill:#F34949
style U fill:#24CD19
style P fill:#F34949
style M fill:#99ff66
```

## Who can I reach out to if I have additional questions about PCI as it relates to GitLab?

The [GitLab security compliance team](/handbook/engineering/security/security-assurance/security-compliance/compliance.html#contact-the-compliance-team) can help with any questions relating to PCI.

## Additional PCI content:

* [PCI DSS v3.2.1](https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf)
* [PCI DSS v3.2.1 Quick Reference Guide](https://www.pcisecuritystandards.org/documents/PCI_DSS-QRG-v3_2_1.pdf)
* [PCI DSS Scoping and Network Segmentation Guidance](https://www.pcisecuritystandards.org/documents/Guidance-PCI-DSS-Scoping-and-Segmentation_v1_1.pdf)
* [PCI Data Storage Do's and Don'ts](https://www.pcisecuritystandards.org/pdfs/pci_fs_data_storage.pdf)
* [PCI Glossary of terms](https://www.pcisecuritystandards.org/pci_security/glossary)
* [PCI SAQ-A](https://www.pcisecuritystandards.org/documents/PCI-DSS-v3_2_1-SAQ-A.pdf)
* [PCI SAQ-A AOC](https://www.pcisecuritystandards.org/documents/AOC-SAQ_A-v3_2_1.pdf)
* [PCI DSS Tokenization Guidelines](https://www.pcisecuritystandards.org/documents/Tokenization_Guidelines_Info_Supplement.pdf)
* [PCI DSS Cloud Computing Guidelines](https://www.pcisecuritystandards.org/pdfs/PCI_SSC_Cloud_Guidelines_v3.pdf)
