---
layout: handbook-page-toc
title: "Code Review"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Code reviews are mandatory for every merge request, you should get familiar with and follow our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html). Because of the [recognized criticality of building a community of contributors](/handbook/product/product-management/process/#community-considerations) we put a high priority on ensuring community contributions receive a swift response to their submissions including a [first-response SLO](#first-response-slo).

These guidelines also describe who would need to review, approve and merge your, or a community member's, merge request.

## Reviewer

All GitLab engineers can, and are encouraged to, perform a code review on merge requests of colleagues and community contributors. If you want to review merge requests, you can wait until someone assigns you one, but you are also more than welcome to browse the list of open merge requests and leave any feedback or questions you may have.

You can find someone to review your merge requests by looking on the [team page](/company/team/), or on the list of [GitLab Engineering Projects](/handbook/engineering/projects/), both of which are fed by `data/team.yml`.

You can also help community contributors get their merge requests ready, by becoming a [Merge Request Coach](/job-families/expert/merge-request-coach/).

Note that while all engineers can review all merge requests, the ability to _accept_ merge requests is restricted to maintainers.

## Maintainer

Maintainers are GitLab engineers who are experts at code review, know the GitLab product and codebase very well, and are empowered to accept merge requests in one or several [GitLab Engineering Projects](/handbook/engineering/projects/).

Every project has at least one maintainer, but most have multiple. Some projects have separate maintainers for different specialties. For example, GitLab has separate maintainers for frontend, backend, and database.

Great engineers are often also great reviewers, but code review is a skill in and of itself and not every engineer, no matter their seniority, will have had the same opportunities to hone that skill. It's also important to note that a big part of being a good maintainer comes from knowing the existing product and codebase extremely well, which lets them spot inconsistencies, edge cases, or non-obvious interactions with other features that would otherwise be missed easily.

To protect and ensure the quality of the codebase and the product as a whole, people become maintainers only once they have convincingly demonstrated that their reviewing skills are at a comparable level to those of existing maintainers.

As with regular reviewers, maintainers can be found on the [team page](/company/team/), or on the list of [GitLab Engineering Projects](/handbook/engineering/projects/).

### Meeting the reviewer/maintainer

Communication happens easier when you are familiar with the person reviewing the code. Take opportunities (for example coffee chats) to get to know reviewers to break the ice and facilitate future communication.

### How to become a maintainer

**This applies specifically to backend, frontend and database maintainers. Other areas (docs, etc.) may have separate processes.**

As a reviewer, a great way to improve your reviewing skills is to participate in MRs. Add your review notes, pass them on to maintainers, and follow the conversation until the MR is closed. If a comment doesn't make sense to you, ask the commenter to explain further. If you missed something in your review, figure out why you didn't see it, and note it down for next time.

We have two guidelines for maintainership, but no concrete rules:

1. In general, the further along in their career someone is, the more we expect them to be capable of becoming a maintainer.
2. Maintainers should have an advanced understanding of the GitLab codebase. Before applying for maintainership, a person should get a good feel for the codebase, expertise in one or more domains, and deep understanding of our coding standards.

Apart from that, someone can be considered as a maintainer when both:

1. The MRs they've reviewed consistently make it through maintainer review without significant additionally required changes.
2. The MRs they've written consistently make it through reviewer and maintainer review without significant required changes.

Once those are done, they should:

1. Create a MR to add the maintainership to their team page entry.
2. Explain in the MR body why they are ready to take on that responsibility.
3. Use specific examples of recent "maintainer-level" reviews that they have performed. These include but are not limited to:
    1. MRs that introduce non-trivial changes (architectural changes, refactors, developer experience upgrades)
    2. MRs that improve our quality of product (tests, performance)
    3. MRs that deliver full features (ideally this is done iteratively, so a good maintainer can guide someone to break down large MRs to smaller ones)
    4. **Note:** MRs that introduce very simple changes are good, but should not be the only source of reviews 
4. Assign the MR to their manager and mention the existing maintainers of the relevant product (GitLab, GitLab Shell, etc) and area (backend, frontend, etc.).

If the existing maintainers of the relevant engineering group e.g., backend, *do not have significant objections*, and if at least half of them agree that the reviewer is indeed ready, we've got ourselves a new maintainer!

Since the manager of the new maintainer is the MR assignee, they should be the one merging the MR.

It is helpful if the person merging the MR also leaves a comment with a summary, see [example 1](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22212#note_166997960) or [example 2](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/29148#note_212342171) for reference.

The MR should be open for at least 24 hours to allow all available maintainers to raise their comments. When the manager merges the MR, they should announce this change in the applicable channels listed under [keeping yourself informed section of the engineering handbook](/handbook/engineering/#keeping-yourself-informed).

If *there are significant objections*, the maintainers who raise the objections should actively work with the maintainer nominee's manager to develop a plan on how to resolve the objections.

The existing maintainers of the relevant engineering group will also raise any areas for growth on the merge request. If there are many gaps, the reviewer will need to address these before asking for reconsideration.

#### Trainee maintainer

To help grow the maintainer base with the team, we allow for 'trainee maintainers'. These are reviewers who have shown a specific interest in becoming a maintainer, and are actively working towards that goal.

Anyone may nominate themselves as a trainee by opening a tracking issue using the [Trainee backend maintainer template], [Trainee frontend maintainer template], or [Trainee database maintainer template]. It's normally a good idea to check with at least one maintainer or your manager before creating the issue, but it's not required.

After opening your tracking issue, create a Merge Request for the [team page] proposing yourself as a trainee maintainer.

Most backend trainees, working full-time without significant interruptions (for example, parental leave) reach the point where they are ready to become a maintainer in five to seven months. If it takes longer, that's OK.

Trainees should feel free to discuss process or progress with their manager or any maintainer, at any time. We recommend that the managers of trainee maintainers arrange a check-in every six weeks or so during the process, to assess where they are and what remains to be done.

If you'd like to work towards becoming a maintainer, discuss it in your regular [1:1 meetings] with your manager. They will help you to identify areas to work on before following the process above.

#### After becoming a maintainer 

If you’ve become a new maintainer, follow these instructions to request relevant permissions that will allow you to fulfill your role:

- Join the maintainer’s group channel on Slack:  #frontend_maintainers, #backend maintainers, etc. 
- Ask the maintainers in your group to invite you to any maintainer-specific meeting if one exists. 
- Request access to the GitLab maintainer group you belong: [frontend](https://gitlab.com/gitlab-org/maintainers/frontend), [backend](https://gitlab.com/gitlab-org/maintainers/rails-backend), or [database](https://gitlab.com/gitlab-org/maintainers/database). 
- Request maintainer permissions on the projects you will act as a maintainer using the [Single Person Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single_Person_Access_Request) issue template. Once you’ve created the issue, request another maintainer to grant you those permissions. 

### Maintainer ratios

We aim to keep the engineer : maintainer ratio under 6, for both frontend and backend. We track this in the [Engineer : Maintainer Ratio dashboard][dashboard]:

<embed width="100%" height="850" src="<%= signed_periscope_url(dashboard: 475647, embed: 'v2') %>">

## Domain Experts

Our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html) states that we default to assigning reviews to team members with domain expertise.

### What makes a domain expert?

We currently don't provide rigid rules for what qualifies a team member as a domain expert and instead we use a boring solution of sensible defaults and self-identification.

Sensible defaults:
- Team members working in a specific stage/group (e.g. create: source code) are considered domain experts for that area of the app they work on
- Team members working on a specific feature (e.g. search) are considered domain experts for that feature

Self-identification:
- Team members can self-identify as a domain expert for a specific feature (e.g. file uploads)
- Team members can self-identify as a domain expert for a specific technology (e.g. GraphQL), product feature (e.g. file uploads) or area of the codebase (e.g. CI).

### How to self-identify as a domain expert

The only requirement to be considered a domain expert is to have substantial experience with a specific technology, product feature or area of the codebase. We leave it up to the team member to decide whether they meet this criteria.

1. Define a new, or use an existing domain expertise key, located in [`data/domain_expertise.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/domain_expertise.yml).
1. Update your entry in [`data/team.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/team.yml) with a new `domain_expertise` property and list all domain expertise keys.

Example:

**domain_expertise.yml**
```yaml
webpack:
  display_name: Webpack
  link: https://webpack.js.org/
frontend_architecture:
  display_name: Frontend Architecture
  link: https://docs.gitlab.com/ee/development/fe_guide/architecture.html
```

**team.yml**
```yaml
domain_expertise:
    - webpack
    - frontend_architecture
```

When self-identifying as a domain expert, it is recommended to assign the MR to be merged by an already established Domain Expert or a corresponding Engineering Manager.

### Where can I find a list of people with domain expertise?

The expertise of a team member can be seen on the [Engineering Projects](https://about.gitlab.com/handbook/engineering/projects/) page.

## First-response SLO

To ensure swift feedback to ready-to-review code, we maintain a `first-response` Service-level Objective (SLO). The SLO is defined as:

> * first-response SLO = (time when first response is provided) - (time MR is submitted and no longer marked WIP) < 2 business days

### Managing expectation

When you are assigned to review an MR and you are not able to get to it within the `First-response` SLO, you should leave a comment on the MR informing the author of your delayed response. If possible, you should also indicate when the author can expect your feedback or help them find an alternative reviewer.

As the author of an MR you should reassign to another reviewer or maintainer if the `First-response` SLO has not been met and you have been unable to contact the assignee.

[Trainee backend maintainer template]: https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=trainee-backend-maintainer
[Trainee frontend maintainer template]: https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=trainee-frontend-maintainer
[Trainee database maintainer template]: https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=trainee-database-maintainer
[1:1 meetings]: /handbook/leadership/1-1/
[dashboard]: https://app.periscopedata.com/app/gitlab/475647/Engineer-:-Maintainer-Ratio
[team page]: https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml
