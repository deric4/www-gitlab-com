---
layout: handbook-page-toc
title: "Reliability Engineering"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

If you're a GitLab team member and are looking to alert Reliability Engineering about an availability issue with GitLab.com, please email [gitlab-production-imoc@gitlab.pagerduty.com](mailto:gitlab-production-imoc@gitlab.pagerduty.com). Note that this will immediately page the Incident Manager OnCall.
{: .alert .alert-danger}

If you're a GitLab team member looking for assistance from Reliability Engineering, please see the [Getting Assistance](#getting-assistance) section.
{: .alert .alert-info}

## Who We Are

Reliability Engineering is responsible for all of GitLab's user-facing services, with their primary responsibility being GitLab.com. Site Reliability Engineers (SREs) ensure the availability of these services, building the tools and automation to monitor and enable this availability. These user-facing services include a multitude of environments, including staging, GitLab.com, and dev.GitLab.org, among others (see the [list of environments](/handbook/engineering/infrastructure/environments/)).

## Vision

**Reliability Engineering** ensures that GitLab's customers can rely on GitLab.com for their mission-critical workloads. We approach availabilty as an engineering challenge and empower our counterparts in Development to make the best possible infrastructure decisions.


## Tenets
1. [**Change Management**](/handbook/engineering/infrastructure/change-management/), [**Incident Management**](/handbook/engineering/infrastructure/incident-management/), and [**Delta Management**](/handbook/engineering/infrastructure/library/production/deltas/) are owned by Reliability Engineering.
1. Each team member is able to work on all team projects.
1. The team is able to reach conclusions independently all the time, consensus most of the time.
1. Career development paths are clear.
1. The team maintains a database of SRE knowledge through documentation, training sessions, and outreach.
1. We leverage the GitLab product where we can in our toolchain.


## Team

The Reliability Engineering team is composed of [DBRE](/job-families/engineering/database-reliability-engineer/)s and [SRE](/job-families/engineering/site-reliability-engineer/)s. As the role titles indicate, they have different areas of specialty but shared ownership of GitLab.com's availability. 

## Organizing Our Work

The Reliability Engineering team primarily organizes on the ~"team::Reliability" label in the [GitLab Infrastructure Team](https://gitlab.com/gitlab-com/gl-infra) group.

## Workflow / How we work

There are now [3 infrastructure teams](/company/team/org-chart/) reporting to
1. Datastores team - [Alberto Ramos](/company/team/#albertoramos) - This team owns and is responsible for our persistent storage platforms, with PostgreSQL on gitlab.com being the top priority.
1. Observability team - [Anthony Sandoval](/company/team/#ansdval) - This team owns and is responsible for building and maintaining our Monitoring amd Alerting infrastructure, along with our Caching Queuing infrastructure.
1. Core Infra team - [David Smith](/company/team/#dawsmith) - This team owns and is responsible for Network Ingress/Egress, 

Each team manages its own backlog and uses [Milestones](https://gitlab.com/groups/gitlab-com/gl-infra/-/milestones) as a mechanism to plan and timebox sprints.

### Boards: SRE On-call and Teams

The three teams share the [on-call](/handbook/on-call/) rotations for GitLab.com. The 3 SREs in the weekly rotation (EMEA/Americas/APAC) share responsibility for triaging issues and managing tasks on the [SRE On-call](https://gitlab.com/groups/gitlab-com/-/boards/962703) board. The board uses the group `SRE:On-call` label to identify issues across subgroups in `gitlab-com` and is not aligned with any single milestone.


### Getting Assistance

If you'd like our assistance, please use one of the two issue generation templates below and the work will be routed appropriately.

[Open a General Request Issue](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/new?issuable_template=request-general) - follow this link to create a general issue for Reliability Engineering.

[Open a Customer Questions and Sales Enablement Issue](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/new?issuable_template=request-sales-enablement) - follow this link to seek assistance in answering questions for prospects or current customers.


### Issue Trackers

#### Infrastructure

The [infrastructure issue tracker](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues) is the backlog for the infrastructure team and tracks all work that SRE teams are doing that is not related to an ongoing change or incident.

#### Production Issue Tracker

We have a [production issue tracker](https://gitlab.com/gitlab-com/gl-infra/production/issues).  Issues in this tracker are meant to track incidents and changes to production that need approval.  We can host discussion of proposed changes in linked infrastructure issues.  These issues should have ~incident or ~change and notes describing what happened or what is changing with relevant infrastructure team issues linked for supporting information.

* All [incidents](https://gitlab.com/gitlab-com/gl-infra/production/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=incident) affecting customers S1-S4
* All [changes](https://gitlab.com/gitlab-com/gl-infra/production/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=change) to infrastructure serving production traffic
* [Oncall handover reports](https://gitlab.com/gitlab-com/gl-infra/production/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=oncall%20report)

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com&ctz=America%2FChicago" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

#### Standups and Retros

Standups:
We do standups with [a bot](https://geekbot.io/dashboard/standup/25831/view) that will ask for updates from each team member at 11AM in their timezone.  Updates will go into our slack channel.

Retros:
We are testing async retros with [another bot](https://geekbot.io/dashboard/standup/26259/view) that happens the second Wednesday of our milestone.  Updates from that retro will again go to our slack channel.
A summary will also be made so that we can vote on important issues to talk about in more depth.  These can then help us update our themes for milestones.


### Boards

We use boards extensively to manage our work (see https://gitlab.com/groups/gitlab-com/gl-infra/-/boards).

##### [Reliability Engineering](https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/1436327?)
board. The board is groomed **daily** by the Reliability Managers.

The managers' priorities are to:

1. Ensure the `workflow-infra::Blocked` list is empty (i.e., unblocking issues is critical)
1. Maintain the board up to date with the help of issue assignees

##### [Production](https://gitlab.com/gitlab-com/gl-infra/production/-/boards/1204483)

keeps track of the state of Production, showing, at a glance, incidents, hotspots, changes and deltas related to production, and it also includes on-call reports.

There are four types of issues related to production, denoted by labels:

| Label      | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| `incident` | Incidents are anomalous conditions where GitLab.com is operating below established SLOs. |
| `hotspot`  | Hotspots identify threats that are likely to become incidents if not addressed but that we are unable to address right away. |
| `change`   | Changes are scheduled changes through mainatenance windows.  |
| `delta`    | Deltas reflect devitations from standard configuration that will eventually merge into the standard. |


### [Logistics]()

The Production Board is groomed by the IMOC/CMOC on a daily basis, and we strive to keep it both clean and lean.

##### [DBRE](https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/1232123)

The `Database` (*group label*) will automatically add issues to the board. 

##### [Observability](https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/1270688)

There are two labels that identify issues related to Observability efforts for GitLab.com. First, there is a `gitlab-com` group label that collects Observability related issues company wide&mdash;[~Observability](https://gitlab.com/groups/gitlab-com/-/issues?label_name%5B%5D=Observability). And then, there's the [~Board::Observability](https://gitlab.com/groups/gitlab-com/gl-infra/-/issues?label_name%5B%5D=Board%3A%3AObservability) scoped label in the `gl-infra` sub-group. We used the second label to distinguish issues that require the focus of the Site Reliability team responsible for observability, from other groups' properly identified Observability issues.

There is a name collision at the sub-group level&mdash;we have an `~Observability` label there, too. However, it's used primarily at the epic level to define our [Roadmap](https://gitlab.com/groups/gitlab-com/gl-infra/-/roadmap?label_name%5B%5D=Observability&scope=all&sort=end_date_asc&state=opened&utf8=✓&layout=QUARTERS).

If you need SRE attention on a GitLab.com Observability related issue, please add the `Board::Observability` label.

<%= partial "handbook/engineering/infrastructure/_labels.html" %>
