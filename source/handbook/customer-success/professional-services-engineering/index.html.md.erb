---
layout: handbook-page-toc
title: "Professional Services"
---
# Professional Services Handbook
{:.no_toc}

The Professional Services team at GitLab is a part of the [Customer Success](/handbook/customer-success) department. The team is dedicated to delivering implementation, migration, and education services to GitLab customers. Our service offerings are designed to ensure customer success by accelerating adoption, thus supporting GitLab product revenue growth.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Team overview

### Team functions and responsibilities

The Professional Services team is organized according to specialized functions and responsibilities. Click a Function link below to access details for specific team workflows and responsibilities.

| Functional Team | Responsibility | 
| :-------------- |:------------- |
| [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | Service delivery planning, scheduling, and execution through specialized engineering and instructor team members |
| [Engagement Management](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/engagement-mgmt/) | Opportunity and SOW scoping and closing in collaboration with GitLab Sales team members |
| [Instructional Design and Development](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/instruct-dev/) | Educational content creation, deployment, and maintenance |
| [Practice Management](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/practice-mgmt/) | Definition, planning, go-to-market, and performance for specific categories of professional services offerings |
| [Project Management](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/project-mgmt/) | Project planning and execution |

### Mission

Accelerate our customers' time to value in their DevOps transformation by delivering successful customer outcomes with tangible business value.

### Goals

* Deliver services to improve operational efficiency, accelerate time to market and reduce risk through GitLab product adoption.
* Provide consistency and quality through a repeatable methodology with increased focus on business value. 

### Benefits

Professional Services provides the following benefits to both GitLab customers and GitLab itself.

#### Benefits to our customers

Having a Professional Services team benefits our customers in the following ways:

* Faster value realization
* Improved customer experience
* Access to GitLab experts / best practices

#### Benefits to GitLab

* Improved retention and expansion
* Customer insights and feedback
* Experience and offers for partners

### Delivery approach

Today GitLab Professional Services offerings are delivered directly by GitLab team members as described in our [Professional Services product pages](https://about.gitlab.com/services/).

In the future we plan to enable selected channel partners to deliver Professional Services offerings to GitLab customers. This will allow us to accomplish the following operational objectives.

* Ensure we have local coverage globally
* Scale professional services in alignment with GitLab business growth
* Create a partner revenue stream

### Team members and roles

| Name | Functional Team | Role | 
| :--- | :-------------- | :---- |
| Arani Bhavadeep | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Robert Clark | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | Public Sector Manager, Customer Success |
| Syeda Firdaus | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Donita Farnsworth | Project Coordinator/ Operations | [Sr. Project Coordinator](/job-families/sales/professional-services-project-coordinator/) |
| Adriano Fonseca | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Sr. Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Michael Leopard | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) |  [Federal Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Mike Lindsey | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Michael Lutz | Professional Services Leadership | [Sr. Director, Global Professional Services](/job-families/sales/director-of-professional-services/) | 
| Kendra Marquart | [Instructional Design and Development](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/instruct-dev/) | [Sr. Technical Instructional Designer](/job-families/sales/technical-instructional-designer-customer-education/) |
| Jo Marquez | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Glenn Miller | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Jordan Ng | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Nicki Peric | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Petar Prokic | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Ed Slatt | [Engagement Management](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/engagement-mgmt/) | [Engagement Manager](/job-families/sales/source/job-professional-services-engagement-manager/) |
| David Tacheny | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) |  Principal Technical Architect |  
| Chris Timberlake | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Sr. Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Laci Videmsky | [Delivery](delivery) | [Sr. Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Kevin Vogt | [Delivery](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/delivery/) | [Professional Services Engineer](/job-families/sales/professional-services-engineer/) |
| Allison Walker | [Project Management](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/project-mgmt/) | [Sr. Project Manager (PMO)](/job-families/sales/professional-services-project-manager/) |
| Christine Yoshida | [Practice Management](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/practice-mgmt/) | [Manager, Practice Management, Education Services](/job-families/sales/professional-services-practice-manager/) |

### Team Metrics

GitLab Professional Services measures success through tracking business profitability and resource utilization. The targets for FY21 are as follows.

* **Long term profitability target**: 30% gross margin
* **Utilization target**: 70% billable hours

#### Utilization
GitLab Professional Services Engineers track and categorize the utilization of their working hours as either Billable utilization or Productive utilization. 

* **Billable utilization** is time worked on defined scope that will be charged to a customer according to a contractual SOW. 

* **Productive utilization** is time worked on assigned tasks that benefit GitLab and GitLab Customers (overall utilization including marketing, sales, general management, training, internal projects, etc.)

We use the following definitions to determine and track utilization.

 
| Metric | Formula/Description | Value |
| :--- | :--- | :--- |
| Utilization | (Working Hours ÷ Available Hours) × 100% | varies |
| Billable Utilization | (Billable Working Hours ÷ Utilization) × 100% | 70% target |
| Productive Utilization | (Productive Working Hours ÷ Utilization) × 100% | 30% target |
| Available Hours | Total Weekday Hours － Non-Working Hours | 1792 hrs |
| Total Weekday Hours | (8 Hours × 5 days × 52 weeks) | 2080 hrs |
| Non-Working Hours | (Holiday Hours + PTO Hours + Training Hours) | 288 hrs |
| Holiday Hours | 11 holidays × 8 hrs/day | 88 hrs |
| PTO Hours | 20 PTO days × 8 hrs/day | 160 hrs |
| Training Hours | 5 professional development days × 8 hrs/day | 40 hrs |

## Professional Services Offerings

GitLab offers a full catalog of professional services including implementation, migration, and education delivered by GitLab experts. Click the links to learn more about our framework and for a detailed listing of our standard SKU offerings.

* [PS Offerings Framework](framework)
* [PS Standard SKUs](SKUs)

## Working with Professional Services

Follow [these guidelines](working-with) for contacting us and ordering Professional Services.
