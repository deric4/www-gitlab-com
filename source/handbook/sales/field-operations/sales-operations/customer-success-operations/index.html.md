---
layout: markdown_page
title: "Customer Success Operations"
---

## Mission

Customer Success Operations exists to create trust, predictability, and excellent customer outcomes through clear and efficient processes, consistency, and analytics.

## What is our strategy?
Customer Success Operations creates and updates existing processes for the Customer Success organization. CS Operations oversees:

* Systems implementation and maintenance
* Operational reporting
* Systems enablement
* Operationalizing Customer Success Journeys

### Key Metrics [(from Field Ops)](/handbook/sales/field-operations/#key-tenants)

1. Clarity: for definitions, processes, and events 
2. Visibility: to processes, data and analytics 
3. Accountability: to uphold to expectations and SLAs

## Customer Success Operations Responsibilities

Customer Success Operations creates and updates existing processes for the Customer Success organization. CS Operations oversees:

* Systems implementation and maintenance
* Operational reporting
* Systems enablement
* Operationalizing Customer Success Journeys

## CS Ops Request Process

![CS Ops Issue Flowchart](https://www.lucidchart.com/publicSegments/view/42d94a0a-3a9c-4ffd-b483-51bd9009385f/image.jpeg "CS Ops Issue Flowchart")


### CS Ops Board, Groups, Projects, and Labels

### CS Operations Board

[Customer Success Operations Board](https://gitlab.com/groups/gitlab-com/-/boards/1498673?label_name[]=CSOps)

### Groups

* Use the GitLab.com group for epics that may include issues within and outside the Sales Team group
* Use the GitLab Sales Team group for epics that may include issues within and outside the Field Operations group

### Projects

* Create issues under the "Sales Operations" project


### Labels

The CS Ops team uses issues and issue boards to track projects and tasks. If you need help with a project, please open an issue and ad the ~CSOps label anywhere within the GitLab repo.

CS Operations uses a [global issue board](https://gitlab.com/groups/gitlab-com/-/boards/1498673?&label_name[]=CSOps) to capture and track issues in any group or sub-group in the repo. 

Labels to use:

* CSOps - Label to track and manage all CS Operations related issues
* CSOps::New - Issues that have not been evaluated
* CSOps::Planning - Issues that are currently being scoped or considered but are not being actively worked on
* CSOps::Backlog - Issues for future scheduling, sequencing, or implementation
* CSOps::In_Process - Issues that are actively being worked on in the current week or sprint
* CSOps::Blocked - Issues that are currently blocked by an internal or external prerequisite 
* CSOps::Transferred - Issues that have been transferred to another team for review and/or completion
* CSOps::Completed - Issues where the CS Ops team has completed their portion
