---
layout: handbook-page-toc
title: "Editorial team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What this page is for

This part of the handbook is for the blog editorial team. If this isn't you, you may find what you are looking for in the [blog handbook](/handbook/marketing/blog), which covers the process for opening an issue and merge request for your blog post, as well as getting reviewed and published.

## Other related pages
- [Blog handbook](/handbook/marketing/blog)
- [GitLab Unfiltered blog handbook](/handbook/marketing/blog/unfiltered/)
- [Brand personality](/handbook/marketing/corporate-marketing/#brand-personality)
 and [tone of voice guidelines](/handbook/marketing/corporate-marketing/#tone-of-voice)

## Reviewing blog post pitches

 Every Monday, a member of the Editorial team reviews [blog post pitches](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues?label_name=Blog%3A%3APitch). There is a recurring event on the [Editorial calendar](/handbook/marketing/corporate-marketing/content/#editorial-calendar) for every Monday. The Editorial team member reviewing pitches that week is invited to the event. 

### What to look for

- Has another member of the Editorial team already reviewed the pitch and responded? If so, no need to review.
- Should the pitch be an [announcement request](/handbook/marketing/corporate-marketing/#requests-for-announcements) instead?
- Does the pitch meet the requirements and answer the questions outlined in [How to get your pitch accepted](/handbook/marketing/blog/#how-to-get-your-blog-post-pitch-accepted)
  - What's in it for the audience?
- Does the pitch show potential to help us meet our [goals for the blog](/handbook/marketing/blog/#goal-grow-our-audience-engage-readers-and-convert-readers-into-subscribers)?
- Have we published anything on an overlapping topic recently? 
  - Does this pitch offer something new?
- Could we tweak the angle of the post to better align with our requirements and goals?
- How have similar posts performed in the past? 
  - If there are recent examples of similar topics or angles, pull the number of unique views from [Google Analytics](https://analytics.google.com/analytics/web/#/report/content-pages/a37019925w65271535p67064032/). A two-month window from date of publishing usually gives a good idea of whether something did well with our audience. 
- Is the pitch better suited to another channel? Sometimes the information is more appropriate for sharing in:
  - **The handbook** – This is great for evergreen content, documenting how we do things at GitLab or why we do things a certain way. Blog posts generally should have an element of "newness" or news to them. If the topic of a pitched blog post won't age, or the audience is primarily internal, the handbook is a better option. Because our handbook is public, pages from it can be shared in the same way as a blog post. 
    - Example: This page on [Why GitLab uses the term all-remote to describe its 100% remote workforce](/company/culture/all-remote/terminology/) was originally suggested as a blog post, but works better as a handbook page, because the content is evergreen and can be built on over time.
  - **The [Unfiltered blog](/handbook/marketing/blog/unfiltered/)** – This is a good choice for blog posts that are more personal in nature. Think of the Unfiltered blog like a peer-to-peer publishing platform. This can lend greater credibility to personal stories because they haven't gone through Marketing, and it relieves the burden of having to align with the blog strategy. The Unfiltered blog is also a good home for posts that may appeal to a smaller, more niche audience (including internal) than the branded blog. We have seen plenty of Unfiltered blog posts succeed because they resonate with readers, regardless of where they were published.
    - Example: [Contribute through the eyes of a new GitLabber](/blog/2020/02/25/contribute-through-the-eyes-of-a-new-gitlabber/) is a personal account of one team member's experience of their first Contribute, and the intended audience is primarily internal.
  - **The writer's personal blog** – Similar to the Unfiltered blog, this makes sense for personal stories that don't lend themselves to the branded blog.
    - Example: [First time all remote: My first week at GitLab](https://dnsmichi.at/2020/03/09/first-time-all-remote-my-first-week-at-gitlab/)
   
### Responding to pitches 

Following the review above, the pitch should fall into one of three categories:

- Aligns with blog goals and strategy: Approved and `Blog::Planning/in progress` label applied
- Needs adjusted angle/focus: Approved with recommendations, `Blog::Planning/in progress` label applied
- Better suited to another channel: Alternative suggested

When responding to the person who pitched, be sure to include your rationale for your response and any relevant data or examples to justify your decision.

## Blog style guide

We use American English by default.

### Acronyms

When using [acryonyms or initialisms](https://www.dailywritingtips.com/acronym-vs-initialism/),
ensure that your first mention uses the full term and includes the shortened
version in parentheses directly afterwards. From then on you
may use the acronym or initialism instead.

> Example: A Contributor License Agreement (CLA) is the industry standard for open
source contributions to other projects.

Below are some common acronyms and initialisms we use which don't need to be
defined first (but use sparingly, see [Tone of voice](/handbook/marketing/corporate-marketing/#tone-of-voice) above):

- AFAIK - as far as I know
- ICYMI - in case you missed it (for social only)
- IIRC - if I recall correctly
- IRL - in real life
- TL;DR - too long; didn't read

### Ampersands

Use ampersands only where they are part of a company's name or the title of a
publication or similar. Example: Barnes & Noble

Do not use as a substitute for "and."

### Capitalization

See [below](#word-list) for styling of specific words.

#### Case

Use [sentence case](https://www.thoughtco.com/sentence-case-titles-1691944) for
all titles and headings.

#### Feature names

All GitLab feature names must be capitalized. If referring to a GitLab feature as part of a workflow rather than speaking about the feature itself, use lower case.

> Examples: "GitLab Issue Boards are a powerful project management and collaboration tool." vs "The editorial team uses an issue board to track the progress of blog posts."

#### Job titles

We do not capitalize job titles, regardless of whether they appear before or after a person's name.

#### GitLab functions/departments/teams

These are elements that make up GitLab the company's [organizational structure](/company/team/structure/#organizational-chart). Capitalize the name of the element, but not the word after:

> Example: Engineering function, Security department

#### Brand and publication names

Ensure you style brand names consistently with how the company does.

> Example: WiFi Tribe, DigitalOcean

The only exception to this is for brand names that are in all upper or lower case. Always capitalize the first letter, regardless of how it is styled in a company's logo.

> Examples: Reddit, Lego

If the word "the" forms part of a brand or publication's name, capitalize it:

> Examples: The Wall Street Journal, The Times

You can drop the "The" entirely if used as follows:

> "We spoke to a Wall Street Journal reporter"

### Contractions

We favor contractions ("can't," "didn't," "it's," "we're") to sound more human and
less formal.

### Dates

#### Months

Spell out, unless using the full date (see below).

#### Specific dates

Jan. 3, 2019 (abbreviate month, no `rd` after 3)

### Headlines

Please see [headline advice](/handbook/marketing/blog/#title) in the blog handbook.

### Lists

Use `*` or `-` to create a bulleted list in Markdown. No period is necessary at the end of each bullet point.

### Numbers

Numbers with four or more digits should include a comma.

> Examples: 2,000; 100,000

#### In body copy

Spell out one to nine. Use numerals for 10 upwards. Try to avoid beginning a
sentence with a number, but if unavoidable, spell it out.

#### In headings/subheadings

Use numerals. If at the beginning of a heading, capitalize the first word following.

> Example: 3 Strategies for implementing a microservices architecture

### Pronouns

Unless referring to someone in particular, use gender-neutral pronouns ("they", "them").

### Punctuation

Only one space after a period is necessary.

Include one space after ellipses (... )

See [below](#word-list) for when to hyphenate specific words.

We use en dashes (–) rather than em dashes (—). Include a space before and after the dash.

Use % instead of "percent" at all times.

### Quotes

#### Quotation marks

Use double quotation marks for direct quotes, and single quotation marks for a quote within a quote. Single quotation marks may also be used for specialist terms or sayings.

- Include periods and commas inside ending quotation marks.
- Leave semi-colons, colons, and dashes outside of the quotation marks.
- Include other punctuation based on the sentence meaning.

Examples: 
> Recently, an article was published stating, "Software is eating the world."

> What do you think of the claim, "Software is eating the world"?

> "Do you agree that software is eating the world?" wrote the author.


#### Tense

When including direct quotations from interviewees in blog posts, we prefer to use the feature journalism style of present tense for verbs such as "said," "explained" etc.

> Example: "Ruby was optimized for the developer, not for running it in production," says Sid.

#### Referring to interviewees

For blog posts, prefer referring to interviewees by their first names as this is less formal and more in keeping with our [tone of voice](/handbook/marketing/corporate-marketing/#tone-of-voice).

### Voice

Active voice is preferred to passive voice in blog posts. *Voice* describes whether the subject of a sentence receives or performs the action of a verb.
> Example: "The GitLab community submitted 1 million merge requests in March 2019." (active) vs. "One million merge requests were submitted by the GitLab community in March 2019." (passive)

### Word choice

When in doubt, use the "future" styling of a word. So, "internet" is not capitalized,
"startup" is not hyphenated, etc.

### Word list

How to spell and style commonly used words.

- Agile
   - A is capitalized when referring to [Agile methodology](https://en.wikipedia.org/wiki/Agile_software_development)
- all remote
    - We refer to GitLab as an all-remote company (not remote friendly, remote first, or remote only)
    - hyphenated only when appearing before a noun ("GitLab is an all-remote organization"/"GitLab is all remote")
- Board
    - when in reference to the GitLab Board or Directors or our Board members, Board is capitalized
    - a board in general does not need to be capitalized 
- cloud native
    - not capitalized, and no hyphen, regardless of how it is used
- co-founder
    - hyphenated, not capitalized
- continuous delivery, deployment, integration
    - not capitalized
- developer
    - use the abbreviation "dev" sparingly; do not capitalize
- DevOps
    - D and O always capitalized
- E-Group
    - references to E-Group should alays include a capital E, hyphen, and a capital G
- Git
    - always capitalized
- GitHub
- GitLab
    - G and L are always capitalized, even in GitLab.com
- internet
    - not capitalized
- Kubernetes
    - always capitalized, never abbreviated to K8s (except on social)
- multicloud
    - one word, no hyphen, lowercase m, lowercase c
- open source
    - no hyphen, regardless of how it is used
- operations
    - use "ops" sparingly; do not capitalize
- set up/setup
    - two words for the verb, one for the noun ("How to set up a Kubernetes cluster"/"Let's walk through the Kubernetes cluster setup process")
- sign up/signup
    - two words for the verb, one for the noun ("Sign up for a GitLab.com account"/"Upon signup, you will be sent a confirmation email")
- startup
    - no hyphen
- web
    - not capitalized

### Appendix B: UK vs. American English

Use American spelling in your communications. Please consult [this list of
spelling differences](https://en.oxforddictionaries.com/spelling/british-and-spelling).

## SEO guidelines

Each blog post should be optimized for search engines. 

A primary keyword is a word/phrase that is repeated a few times in the blog post/headline (see below) and will help a search engine make our content "findable." A secondary keyword reinforces the first keyword by giving someone searching for content on the internet another way to find what they're looking for. In the spirit of MVC, let's focus on just making sure each blog post has a primary keyword (but you'll get bonus points if you can work a secondary keyword in!) A primary keyword might be "DevSecOps" while a secondary keyword might be "DevOps security tools" or "secure software development." Please note that while "keyword" is the commonly used term that does not mean it has to be a single word; it can certainly be a short phrase (also known as a long-tail keyword.)

We use [Moz Pro](https://www.moz.com), which is a straightforward way to search for keywords. You can enter a topic and then you'll get suggestions based on traffic volume. Choose a keyword and remember that often a long-tail version might work just as well and have less competition from other sources (example: DevSecOps vs. DevOps security tools). 

**In an ideal world, your keyword or keyword phrase should be in the URL, the headline and the first paragraph.** 

If some or all of that is not possible, alt-text for a photo, a caption or the tweet that appears on the bottom of the blog post are great ways to "sneak in" keywords.

If for some reason you don't have access to Moz or you're not finding anything that feels right, here's another more DIY SEO option.

1. Start with the main topic of the post (example: DevSecOps)
1. Visit [Google Trends](https://trends.google.com), enter your term and make sure to choose "worldwide." You can also enter other terms to compare it to (example: software development security). Scroll down and you'll see how popular your keyword is and you'll also be offered some alternate suggestions.
1. Consider your keyword in context. The term "DevOps" will be popular but that also means there will be a lot of competition for it. But a long-tail keyword like "DevOps security tools" will have less competition.
1. Once you've settled on a keyword it would be ideal, but not mandatory, to have it in the URL, headline and in the first paragraph.

Finally, digital marketing is putting together a searchable database of relevant keywords for GitLab that will be updated monthly. When that is ready a link will be available here.

## Editorial review checklist

### Style and language

- Does the post adhere to our style guide?
- Is it written in [American English](#appendix-b-uk-vs-american-english)?
- Are all titles and headlines in [sentence case](#case)?

### Formatting

- Is the filename all in lowercase, with no special characters or numbers other than the date?
- The filename will become the blog post URL when it is published. Does it make sense as the URL? Does it include the primary keyword for SEO?
- Does the filename reflect the publish date accurately? (final review only)
- Has all the relevant [frontmatter](/handbook/marketing/blog/#frontmatter) been included correctly?
- Has the correct [category](/handbook/marketing/blog/#categories) been entered? If not, the build will fail.
- Has `gitlab` been added to the `author_twitter` field if the author doesn't wish to use their own handle?
- Does the `Description` field copy fit on the tile on [/blog](/blog)?
- Is it appropriate to include a [trial CTA](/handbook/marketing/blog/#ee-trial-banner) on this post? Generally, if a post falls under `culture`, `engineering`, or `open source` it is best to remove it.
- Has the appropriate [merchandizing](/handbook/marketing/blog/#merch-banner-and-sidebar) been included and are the sidebar and banner rendering correctly?
- Are all [images formatted correctly](/handbook/marketing/blog#inline-images) and are they <1MB?
- Are any image filenames all in lower case, with no special characters or spaces?
- Does any of the images need a [caption](/handbook/marketing/blog#image-captions)?
- Check the file extension `.html.md.erb` and that the [newsletter sign-up form is included in the post](/handbook/marketing/blog/#newsletter-sign-up-form), if appropriate. Don't include it if there are other CTAs in the post (for example, directing people to sign up for a webcast or watch a video).
- Is there an [image credit](/handbook/marketing/blog#image-attribution), if needed?
- Does the click-to-tweet copy make sense? Could it include any other handles or hashtags? [Edit it](/handbook/marketing/blog/#twitter-text) if necessary.
- Do all links work?
- Are all about.gitlab.com [links relative](/handbook/markdown-guide/#relative-links)?
- [Wherever possible, does the text for links make sense out of context?](/handbook/markdown-guide/#links) Would someone only seeing that text know what to expect from the link if they click on it?

## Checklist for blog post issues 

If you are planning to write or even just considering writing a blog post: 

- Start by opening an issue in the [gitlab.com/gitlab-com/www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/issues) project, using the `Blog post` template 
- Under `Proposal`, describe your proposed blog post topic
- Include links to any relevant background information or resources in the issue description
- Review and check off any relevant checklist items in the blog post issue template
- Make sure the appropriate [labels](/handbook/marketing/blog#labels) are applied
- If you are definitely going to write the blog post, choose a publish date that is available on the [Editorial calendar](/handbook/marketing/blog#blog-calendar) and include that date in your issue title
- Give the issue a due date a minimum of two working days before your chosen publish date 
- Apply the appropriate milestone to the issue (e.g. `April blogs 2020`)
- Assign the issue to yourself
- [Add an item to the Editorial calendar](/handbook/marketing/corporate-marketing/content/#adding-an-event-to-the-editorial-calendar) for your blog post on your chosen publish date, using the appropriate emoji

### If you reschedule a blog post

Please ensure that you update: 
- Issue title
- Due date
- Milestone (if applicable) 
- Calendar entry

If the post is postponed and you don't yet have a specific reschedule date, please remove:
- Date in the title (replace with TBD)
- Due date 
- Milestone
- Calendar entry

## Choosing featured posts

Each week, one blog post is selected as the [featured post](/handbook/marketing/blog/#featured) on the blog homepage. 

To decide on the featured post for a given week, the managing editor will open an issue in the [Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/), listing the blog posts scheduled to go live in the coming week.

Editorial team members will nominate a post to be featured, based on the following criteria:

- Past performance of similar topics
- Supporting a key activity or campaign
- High-profile company/product announcement

When the featured post has been agreed on:

1. Update the `weekly featured post` issue description
1. Leave a comment to confirm in the issue for the featured post (or MR, if one exists already)
1. Link the blog post issue to the `weekly featured post` issue where this was decided. 
1. Close the `weekly featured post` issue. 

## Checklist for merging blog posts

When you are ready to merge a scheduled blog post, check the review app for the blog post:

![Link to review app](/images/handbook/marketing/link-to-review-app.png){: .shadow.medium.center}

- Is the date on the blog post correct?
- Does the filename (which will become the URL for the post) correspond with the headline?
- Should this blog post be [featured](#choosing-featured-posts)? Add or remove `featured: yes` from the frontmatter accordingly.
- Are all images and formatting rendering as expected?
- Has the blog post been reviewed by any other necessary parties (as indicated in issue/MR description)?
- Is the post likely to get a lot of attention/engagement/traffic? Is it about a hot-button topic like compensation or big industry news? Please give the community advocates a heads up in #community-advocates on Slack before you publish.

When you've checked all these, you can tick the boxes for `Delete source branch` and `Squash commits` but this isn't strictly necessary.

Go ahead and hit `Start merge train` or `Add to merge train`. If the blog post does not have to be live ASAP, feel free to join the train. Otherwise you can just choose "Merge immediately" from the dropdown menu to the right.

 ![Merge train options](/images/handbook/marketing/merge-train.png){: .shadow.medium.center}

You can find [more information about merge trains here](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/#immediately-merge-a-merge-request-with-a-merge-train).

Once you've started or joined a merge train, go to the [Pipelines page](https://gitlab.com/gitlab-com/www-gitlab-com/pipelines), which you can find in the menu on the left under CI/CD.

![Shortcut to pipelines page](/images/handbook/marketing/shortcut-to-pipelines-page.png){: .shadow.small.center}

Merging a blog post requires two pipelines to pass. Find your first merge pipeline, which will probably be near the top of the page. It will say `Merge branch 'your-branch-name' into 'master'`.

![Merge pipeline](/images/handbook/marketing/merge-pipeline.png){: .shadow.large.center}

You can watch its progress there. Depending on your notification settings, you may get an email when your pipeline passes, or if it fails.

If it passes, a final pipeline will kick off:

![Merge pipeline](/images/handbook/marketing/final-pipeline.png){: .shadow.large.center}

When this passes, proceed to the next step.

### When your pipeline passes

Go to the [blog homepage](/blog) and your post should be visible there. Sometimes this takes a few minutes. When you see it, grab the link and share it in the #content-updates channel in Slack.

### What happens if my pipeline fails?

If a pipeline fails when you try to merge something, it is usually not something you have done wrong! You can retry it, but if it still doesn't work, it's probably quickest to get an answer by sharing your MR link in #mr-buddies or #questions on Slack.

## MR buddies

If you run into issues with your MR, pipeline, or Terminal, it's usually quickest to ask for help in the #mr-buddies, #git-help, or #questions channel on Slack (be sure to include a link to your MR!). If you don't get an answer that way, you can reach out to a [Merge Request Buddy](/handbook/general-onboarding/mr-buddies/). The following team members have volunteered to be called on for assistance. You can also search for "Merge Request Buddy" on the [team page](/company/team).

- Charlie Ablett
- Mayra Cabrera
- Phil Calder
- William Chia
- John Coghlan
- Mario de la Ossa
- Jean du Plessis
- Dan Gordon
- Kenny Johnston
- Markus Koller
- Lyle Kozloff
- Jason Yavorska
- Cynthia Ng
- Brendan O’Leary
- Axil Pipinellis
- Emilie Schario
- Abubakar Siddiq Ango
- Ronald van Zon
- Thomas Woodham


## Featuring Unfiltered posts

The Editorial team will monitor the GitLab Unfiltered blog for posts that are suitable for featuring on the branded blog on a weekly basis.

### Process for reviewing and featuring Unfiltered posts

Every Monday, a member of the Editorial team will read new Unfiltered posts published during the previous week and select any posts that are likely to perform well on the branded blog for a full editorial review.

#### Criteria for a featured post

- Post has >500 sessions already, according to the [Content Marketing dashboard](https://datastudio.google.com/open/1NIxCW309H19eLqc4rz0S8WqcmqPMK4Qb), and/or
- Post has the [attributes of an historically successful blog post](/handbook/marketing/blog/#attributes-of-a-successful-blog-post)

It's an extra bonus if your post links to related posts either on the main blog or to other posts on Unfiltered. Cross-linking helps the reader and boosts your search engine optimization, meaning more people will see what you've written!

This is not an exhaustive list of criteria, as the Editorial team member will also use their best judgment regarding what tends to perform well on the branded blog.

#### Review process

##### Issue

If an Unfiltered blog post has been identified as a good candidate for featuring, the Editorial team member should open an issue in the [gitlab.com/gitlab-com/www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues) with the title "Feature Unfiltered post: [title of Unfiltered post]", using the `feature-unfiltered-post` issue template.

Include a link to the original post in the issue description, and @ mention the author of the post so that they are aware it is being reviewed for featuring on the branded blog.

##### Merge request review

Once the issue is open, the Editorial team member can create an MR for the full editorial review of the post, in the same way as net new posts that are submitted to the branded blog for review.

**Checklist for reviewer:**

- Add the following statement to the top of the blog post file:

```
This blog post was originally published on the [GitLab Unfiltered blog](/blog/categories/unfiltered/). It was reviewed and republished on yyyy-mm-dd.
{: .alert .alert-info .note}
```
- Remove Unfiltered [disclaimers](/handbook/marketing/blog/unfiltered/#disclaimers) from top and bottom of blog post
-  Change [category](/handbook/marketing/blog/#categories) from `unfiltered` to another
- Add links to related blog posts, where relevant
- Keep filename the same. Do not update to the current date (this will change the URL for the blog post and result in a 404 for existing links to the original post)
- Assign the MR to the post's original author for review of any changes

When the author has reviewed and all outstanding threads resolved, they should reassign the MR to the Editorial team member to merge the changes.

##### Merging

Featured Unfiltered posts can be merged straight away, and don't need to be scheduled like brand new posts. When the edited post is live, the Editorial team member who merged should share a link in the #content-updates channel on Slack so the social team is aware that the post is now on the branded blog and can be shared on GitLab social channels.

The team will aim to have selected posts featured within a week of creating the issue. This may change depending on the availability of the post's author to review changes.
