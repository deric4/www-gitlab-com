---
layout: handbook-page-toc
title: "Social Marketing Handbook"
description: Strategies, Workflows, and Emojis for Social Media at GitLab
twitter_image: "/images/opengraph/social-handbook.png"
twitter_image_alt: "GitLab's Social Media Handbook branded image"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Social Marketing Handbook
*The social marketing team is responsible for the stewardship of the GitLab brand social channels. We’re accountable for the organic editorial calendar and work with partners across the community advocacy, digital marketing, talent brand, and other teams to orchestrate the social media landscape for GitLab.*

## Video introduction to Social Media at GitLab

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/XZQ2Egrk7tI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

## Requesting Social Posts <a name="requesting social promotion"></a>

### Open a new issue to request social coverage
- Head to the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) and create a new issue.
- Select the appropriate issue template:
   - **social-event-request** for coverage of events (tradeshows, etc.)
   - **social-team-advocacy** when the project calls for GitLab Team Members to support our campaign efforts on their personal social channels
   - **social-general-request** for every other request
- Fill out as much information as you can above the first line in the issue.

### Please remember...
- For an anything to get promoted on social, **there must be a dedicated social issue**.
- If the need is urgent, send a message to the `#social_media` Slack Channel.
- If you have already requested or received images for social (paid) ads, please mark that issue as related in the organic social request issue.
- If you have not already requested or received images: Shortly after you open your social issue, the social team will assess whether there are existing assets we can use on social, or if new ones are needed. They will then request new images from the design team, or remove them from the issue. It is at the design teams' discretion whether they have time to create the images, particularly if you open your issue within ~ 1 week of the need to publish.
- Sometimes it is not possible to schedule posts when desired due to any number of reasons, but the social team will work with you to make sure you're supported.
- The social team reserves the right to not publish for a myriad of reasons including crisis moments, calendar priorities, and other elements. We'll do our best to explain why when asked.

## 🎟 Social Event Strategies
Coverage for events across GitLab brand social channels varies and is dependent on investment level, on-the-ground activations, and core messaging. Social coverage for an event is not guaranteed.

Sponsoring or creating an event is a way to extend GitLab to new audiences, so promoting these events to our existing audiences on brand social channels does not work in the same direction. In order for an event to be social-able, there needs to be something that social audiences can gain. In most cases, promoting events on organic brand social channels is a way to tell our audiences that we are out in the world, doing big things, and are taken seriously. This means that simply because GitLab is a sponsor of an event does not mean that we will promote the event on organic brand social channels. 

It's critical to fill out a **social-event-request** issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) as soon as there is enough information available to request organic social coverage. 

Once the social event request issue template is filled out, a member of the social team will determine what level of social coverage, if any, is appropriate. Levels of coverage and what each includes are listed below. Please note, that the descriptors of each tier are suggestions, and a specific event may be identified for a different tier for a number of factors. While most events fall into similar categories, specific coverage may need to be unique to the event.

#### Social Event Tiers and What They May Include

| Tier | Example Event |Equivalent | What's Included |Action|Example |
| ------ | ------ | ------ |------ |------ |------ |
| `1` | Any Integrated Corporate Marketing Event|On-Site Support, Channel Takeovers |  Everything in Tiers 2 and 4. Includes Tier 3 if appropriate. On-Site/At Event Support from a member of the social team. Social Channel "takeovers", e.g. creative focused on the event. |Social Team: Use existing issue to plan. Requester: Be sure to link event campaign epic and other comms/PR issues. It's critical for social + PR to be linked for Tier 1 events. If creative is necessary, open a **design-request-general** issue. |Consider Commit, AWS re:Invent, and others |
| `2` | Any event with a GitLab speaker, presentation or interactive element - *must be more than booth or attendance only* |Broadcasted via Posts on GitLab Brand Channels (Pre, During, Post) | Everything in Tier 4; Includes Tier 3 if appropriate. Organic social posts including custom copy. May include: scheduled posts in advance of the event, live coverage during, and/or post-event wrap-ups. | Social Team: Use existing issue to plan posts. Requester: You may need to include more info. If creative is necessary, open a **design-request-general** issue. |[Ex 1](https://twitter.com/gitlab/status/1228384465891790848?s=20) |
| `3` | Team Member Enablement for any event, when the event team makes this request |Advocacy, Team Members Sharing on Their Channels | Everything in Tier 4; Social team aiding to create copy for team members to share on their own social channels. *This tier is optional, only if the requester wants assistance with advocacy.* | Social Team: Open a **social-team-advocacy** issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues). If creative is necessary, open a **design-request-general** issue.|[Ex 1](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/1437) | 
| `4` | Booth or Attendance Only |Engagement Only | Ad-hoc retweets/engagement with social posts from other users and brands. No outbound broadcasts from brand social channels. The majority of events will fall under Tier 4. |Social Team: Set calendar reminders for event day(s) and open specific columns in Tweetdeck to support. Alert the advocates of live coverage as this impacts Zendesk activity. |[Ex 1](https://twitter.com/gitlab/status/1228340156769411072?s=20)|

#### Field Events + Social Media
Field events present a major challenge and a major opportunity across social channels. While we can geo-target our organic (existing followers) audience on some channels, the number of people represented in this audience may not warrant a social post on GitLab brand social channels.

The single best way to secure social support for field events is to request paid social advertising with the Digital Marketing Programs team. This allows for appropriate geo-targeting on any social channel. This does not need to be a major investment.

You can open a paid social advertising request by [opening a new issue in the Digital Marketing Programs Project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new#) and choosing the **mktg-promotion-template**. The core social team is not the DRI for paid social, however, we can assist you with questions or details along the way.

#### Booth Location-Only Requests for Social Media Coverage
If your only request is 
> "We have a booth at this event and we want people to know where to find us."

Unless the event in question is in an integrated corporate marketing campaign, there are only two appropriate ways to tell social audiences that GitLab has a booth. 
1.  **GitLab Team Members use their personal/professional social channels to share with their audiences.** This could be considered a `Tier 3` or `Tier 4` event, where the social team can aid the event team, however, an issue is required in advance of the event. 
1. **Paid social advertising presents the necessary opportunity** - geo-targeting users in a defined region to tell them where GitLab has a booth.

This means that booth location-only requests are not fulfillable with global organic social media posts.

### FAQ on Social Event Coverage
##### I don't understand why we can't get the support we've asked for.
Organic social media has a number of priorities to mix for success. First, our global audiences need to gain value from our posts or we need to extend GitLab's brand somehow. Every event isn't relevant from a global scale, and in most cases, doesn't provide a brand value. Furthermore, agreeing to publish posts that would not perform well against our performance indicators is a self-sabotage for the social team. It's not in our best interest, as a team or as a company, to publish posts that won't perform well or that do not extend the GitLab brand.

##### I'm worried that my event won't get enough attention on social media.
Organic brand social channels have to maximize fit, content, and distribution in order to meet performance metrics. This yields more attention to the right content from the right kind of audience. An integrated plan for event coverage should never rely solely on social media. If you are deeply concerned about marketing your event, please consider an alternative to organic social, including paid social advertising.

##### When should I ask for paid social advertising for an event?
Generally speaking, there are two points to hit for paid social advertising to work. If the event **has a GitLab-owned landing page** and **needs to be geo-targeted to the right regions** then your event may be a good candidate for paid social advertising. *E.g. Our roadshow is coming to Chicago and we have a landing page for registrations.*

You can open a paid social advertising request by [opening a new issue in the Digital Marketing Programs Project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new#) and choosing the **mktg-promotion-template**. The core social team is not the DRI for paid social, however, we can assist you with questions or details along the way.

##### Help! My event is in *insert short period of time*. Can the social team post?
If your event is in an appropriate tier for broadcasted posts on GitLab brand social channels, the social team will work with you to get these posts scheduled. Please keep in mind that the shorter the amount of time to prepare, the less likely there will be a comprehensive plan including the best copy, creative, and other elements. It's still best to get request issues open as soon as possible.

If last-minute requests become a frequent occurrence or there are other specifics preventing our cooperation, the social team reserves the right to decline the request.

## Giveaways <a name="giveaways"></a>

We use giveaways to encourage and thank our community for participating in marketing events such as surveys, user-generate-content campaigns, social programs, and more.

### Giveaways Process <a name="giveawaysprocess"></a>

**Pre-giveaway**
1. Create an issue and tag the Social Marketing Manager to determine the
rules of engagement and the Corporate Events Manager for prizes.
2. Create and publish an [Official Sweepstakes Rules page](#officialrules)

**Post-giveaway**
1. Winners must sign an Affidavit of Eligibility & Liability, Indemnity, and Publicity Release. Use the "Affidavit of Eligibility - Sweepstakes" template found on the google drive.
2. Announce the winners

### Social Support & Logistics for Giveaways

#### Creating the Campaign

- Set a launch date
- Ask for social image(s) with text (if organic posts only) explaining the offer/ask
- Set an initial deadline for submissions, so you can have multiple pushes at interval & ramp up energy  
- Finalize the delivery method: form vs. tweets vs. retweets, depending on the goals of the campaign
    - Pros of a form: Neat, uniform, easy for us to keep track of, no downsides of low engagement (i.e., responses not visible)
    - Pros of asking for submissions via Twitter: we could more easily RT cool responses, get more out of a hashtag, etc.
    - Pros of asking for RTs in exchange for swag: very little backend to do on social afterwards, except to announce the winners of swag
- Finalize the ask, making sure it's extremely clear what you want to happen (`Share your GitLab story!` `Tell us your favorite thing you made with GitLab` `tell us a time GitLab helped you out of a tight spot`)
    - Make sure the ask can be intuitively communicated via whichever delivery method you're using, i.e., the tweet doesn't need to explain everything if you're pointing to a form or blog post. If you're not pointing to anything, make sure the tweet plus possible image text must make sense by themselves. Use threads for more space!

#### Pre-launch

- Finalize the timeline for when the reminders/follow-ups will go out, add to social schedule and leave some space around them to RT/engage with responses
- Finalize copy for all pushes
- If swag is involved, create a google sheet with swag codes from the Event Marketing Manager
- Finalize hashtag
- Ask community advocates to review all copy (tweets, form, blog post) and adjust according to their suggestions
- Make sure the community advocates are aware of the campaign timeline/day-of
- Designate a social point person to be "on duty" for the day-of and one person who can serve as backup
- Let the broader GitLab team know that the social campaign is upcoming and ask for their support

#### Day of giveaway
- If you have entries for the giveaway in a spreadsheet, use [random.org](https://www.random.org/) to generate a random number. Match the number to the corresponding row in your spreadsheet to identify the winner. **Never enter email addresses or personal information of participants into a third-party site or system we do not control.**
- Try to schedule first push or ask a team member to tweet the first announcement early (ex: around 4 am PT) to try to have some overlap with all our timezones
- If you're asking for RTs in exchange for swag, make sure there's a clearly communicated cut-off to indicate that the giveaway will not stretch into perpetuity. One day-long is probably the longest you want a giveaway to stretch, or you can limit to number of items.
- Plan to engage live with people
   - If your promise was to give away one hoodie per 25 RTs, do it promptly after that milestone is crossed. It adds to the excitement and will get more people involved
- Announce each giveaway and use handles whenever possible, tell them to check their DMs
- DM the swag codes or whatever the item is
- In your copy, directly address the person/people like you are chatting with them irl
- RT and use gifs with abandon but also judgment

#### After the Giveaway
- Thank everyone promptly, internal & external
- Write in the logistics issue of any snags that came up or anything that could've gone better
- Amend hb as necessary for next time

### How to Create an Official Sweepstakes Rules Page <a name="officialrules"></a>

1. Create a new directory in `/source/community/sweepstakes` in the www-gitlab-com project. Name the directory the same as the giveaway `/source/community/sweepstakes/name-of-giveaway`
2. Add an index.html.md file to the `/name-of-giveaway/` folder
3. Add the content of [this template](https://gitlab.com/gitlab-com/marketing/general/blob/0252a95b6b3b5cd87d537dabf3d1675023f1d07d/sweepstakes-official-rules-template.md) to the `index.html.md` file.
4. Replace all bold text with relevant information.
5. Create merge request and publish.

## Social Media's place in an integrated marketing and communications strategy
The social team does not create our own core messaging for blogs or campaigns. We take the approved messaging (whether it's in an issue, a sheet, or from the blog itself) and "socialize" it for our GitLab brand social channels. This is a critical step in making our content genuine, as it's best to take guidance approved upstream (content marketing, MPM, communications, etc.) to best align with our messages. Because of this, it's important to bring the social team into a campaign prior to being activated. It's also critical for our integrated teams to confirm that key messaging has been reviewed, if not approved, by stakeholders for the topic or practice we're promoting.

### Approval of copy
We understand that while social media posts being published is usually the last step in an integrated marketing or communications process, it's usually the first time that all eyes are able to see what came out of the work. Because of this, there are times that stakeholders might not see what messaging or creative made it through the social media creation process. To avoid this situation, we've adapted to a more formal approval process.

During the social media coverage issue request process, the social team adds copy suggestions to the issue for approval. For many integrated campaigns as well as times that just feel best to have an additiona layer of approval, we will request a formal approval prior to scheduling social posts in Sprout. This process does not exist for retweeting/sharing with comment, social-first activations, or blog updates (blogs are vetted through an editorial process that presents a company-approved blog to use copy as desired). 

## Social-First content and campagins
The social media team will often create our own content and mini-campaigns for the sake of having fun and meeting our own objectives. This is also a critical part of a successful content strategy. If our social-first content is to align with a larger topic or talking point from GitLab, we will be sure to run our messaging by the DRI on the correct team. 

## Primary Social Channels, Audiences, and Calendaring <a name="primary social channels audiences and calendaring"></a>
GitLab Branded Social Channels include our company [Twitter](https://twitter.com/gitlab), [LinkedIn](https://www.linkedin.com/company/gitlab-com), and [Facebook](https://www.facebook.com/gitlab) which are managed in a more traditional manner. A potential [Instagram](https://www.instagram.com/gitlab) channel will take more of an editorial look at remote life and work at GitLab, this is currently on hold. A little less “marketing-y” and more about building community. And lastly, our [YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg) channel is a testament to the core of GitLab where everyone can contribute.

### Twitter <a name="twitter"></a>

##### ✍️Channel Use
Because the wider developer community utilizes Twitter as a platform to ask support questions, make feature suggestions, and share industry ideas, GitLab prioritizes this channel. We post to this platform first, and also more frequently. `@gitlab` broadcasts a variety of content including our blog, releases, important announcements, major integrated marketing campaigns, live event coverage, and All Remote content.

In addition to sharing curated content, the `@gitlab` Twitter handle participates in community management activities (likes, comments, and shares). Through this practice of engagement, we attempt to create a digital space for dev, friends and thought leadership. 

Tweet threads should and are consistently used on the `@gitlab` Twitter handle. Because of GitLab’s value of transparency, utilizing Twitter threads gives our brand the opportunity to provide all important details without the character limit. 

We ultimately use Twitter as an opportunity to provide our community with the most up to date information about gitlab.com, our brand, and also DevOps. We do that with a friendly voice because all devs are friends; no matter the tool they use. And we use emojis whenever we can. The overuse of emojis allows us to participate in a universally used language. GitLab believes that everyone can contribute. That’s why being better communicators is crucial for our audience- no message should go misunderstood. 

##### 🎯 Audience targets
From the United States to Japan, our Twitter audience is a global one. 66% of GitLab’s Twitter audience is between the ages of 18-24, and 16% are between the ages of 25-34. There is an equal breakdown of mobile device use between iOS and Android (both 50%). The nature of a developer’s job however still has 68% accessing Twitter from the web, not a mobile device. Twitter does not have the capability of breaking demographics down by title, however, our particular audience participates in the following conversations: DevOps, code, git, GitHub, remote and remote work, CI, and CD.

##### 📅 Calendaring, Cadence, and Volume
How often GitLab should post depends on the platform and kind of content we are sharing. That said, Twitter does not operate like our other GitLab Social branded Channels. There are no strict frequency limitations on Twitter and we aim to post at least 3 times a day if not more. Content shared should prioritize the blog, releases, the hackathon, culture, the handbook, and All Remote content. These specific pages make up the majority of traffic driven from LinkedIn posts shared between October 2019-March 2020.
*  If there are 4+ posts they should have at least 2 hours in between. This rule does NOT apply if there are time-sensitive posts including releases, security releases, patch releases,  live event coverage, etc.
*  Post every day, and utilize Sprout’s optimal time feature as we have many followers located across all time zones. 
*  In the Twitter ads section, create Twitter cards when the meta description is irrelevant, not unique, or creating repetition. Images used must be delivered by the Design or Social Media Teams. 
*  Retweet daily mentions that are authored by Team Members, our community, or are press opportunities.

### Facebook
##### ✍️Channel Use

GitLab has a smaller audience on Facebook so its priority does fall below Twitter and LinkedIn. It is still important to maintain our presence. That's why we aim to post to this platform no more than 2 posts per day (if that).  GitLab should prioritize posts around All Remote, third party content, and our culture (or Life at GitLab). We do have an obligation to always post important announcements, releases, and specific campaigns or events. 

##### 🎯 Audience targets
From the United States to India, our Facebook audience is a global one. Although our Facebook audience does see 15% of its fan base from the United States. And 51% of fans between the ages of 25-34 are the leading force of followers. Through the power of organic reach, our content has a high potential to also reach users between the ages of 45-54.

Facebook does not have the capability of breaking demographics down by title, however our particular audience participates in the following conversations: security, code, remote, CI, cloud, and CD. 

##### 📅 Calendaring, Cadence, and Volume
How often GitLab should post depends on the platform and kind of content we are sharing. That said, Facebook does not operate like our other GitLab Social branded Channels. It is utilized less frequently and should prioritize culture culture, handbook, jobs, homepage, and All Remote content. These specific pages make up the majority of traffic driven from Facebook posts shared between October 2019-March 2020. 
* There is no immediate need to post every single day on Facebook.
* There should not be more than 2 posts per day. 
* If there are 2 posts per day, there should be several hours in between each post. 
* Geotargeting posts through Sprout should always be used for events if applicable. For example, GitLab Connect in San Antonio, should be limited to Texas. Using organic targeting like this allows us to provide followers relevant information ONLY.

### LinkedIn <a name="linkedin"></a>
##### ✍️ Channel Use

Our company page on LinkedIn shares a variety of content including third party mentions, releases, our blog, company culture, major integrated marketing campaigns, events (must include geotargeting if applicable), and All Remote content. The repost feature of LinkedIn is then used to broadcast company mentions (Example: partner tags GitLab in their post-we broadcast that to our page). Ultimately we use LinkedIn as an opportunity to expand in more detail on the content that we care about. How do we do that? By using a variety of formatted posts with more text, bullets, emojis, questions, quotes, and/or even stats. 

##### 🎯 Audience targets

From San Francisco to India and in between, our LinkedIn audience views our company page from across the world and from many job functions including engineering, information technology, business development, education, and project and program management. Our audience is primarily focused in the entry-level, senior, manager, and director seniority levels in the following industries: IT + Services, Computer Software, Telecommunications, Financial Services, Higher Education, and Program Development. 

##### 📅 Calendaring, Cadence, and Volume
How often GitLab should post depends on the platform and kind of content we are sharing. That said, LinkedIn does not operate like Twitter and follows a 2-3 posts per day limit with the exception of timely announcements that must take place. Content shared should prioritize the home page, culture, the blog, hackathons and Heroes, Remote Work content, and the handbook. These specific pages make up the majority of traffic driven from LinkedIn posts shared between October 2019-March 2020. 
*  Post everyday, no more than a couple times a day- posting any more will result in a drop in engagement and cause GitLab posts to be hidden in followers’ news feeds. 
*  Posts should have a few hours in between to avoid eating up our own impressions and engagements. 
*  Geotargeting through Sprout should **always** be used for events if applicable. For example, GitLab Connect in San Antonio, was limited by location factor=Texas. Using organic targeting like this allows us to provide followers relevant content. 

## Other Social Channels, Audiences, and Calendaring <a name="other social channels audiences and calendaring"></a>
### Instagram
##### ✍️Channel Use
In no way shape or form would our Instagram account mirror our other GitLab Branded Social Channels. In fact, it will take more of an editorial look, will be a little less “marketing-y” and be more about building community. We're rallying around a number of general topics. It does include remote work (All Remote), but more deeply about "Life at GitLab" (talent brand) and stories around living our values.  
For remote specific content we will try and steer clear of remote working stereotypes as a dominant theme. The data and Remote Work Report says that reasons and lifestyles for remote are different for everyone - so really, that's the remote story we want to tell.

Instagram will help the Social Media team tell big stories with small moments. For example, corporate produced events like Commit can use Instagram to provide live experiences and video content. 

##### 🎯 Audience targets
This is essentially being treated as a net new social channel since it had never been posted to. There is no current data available on our audience. However,  we intend for our Instagram content to be consumed by gitlabbers, prospective team members, gitlab.com users, and those interested in remote work. The GitLab Instagram is targeting the #LifeAtGitLab, #AllRemote, #RemoteLife, #OpenSource, #DevOps, and #FutureOfWork conversations. 

##### 📅 Calendaring, Cadence, and Volume

How often GitLab should post depends on the platform and kind of content we are sharing. That said, the Instagram Story feature will not have a frequency limitation and should be utilized as often as content is available. However, we will not over post Instagram wall posts. Instagram is a visual platform where GitLab can showcase the human side of our company and our brand. Each wall post on the GitLab Instagram account should tell its own story- and be a stand alone post. At initial launch, we will aim to post once a month as we gear up content to post 1X per week or biweekly

### Medium <a name="medium"></a>
GitLab has a [Medium publication](https://medium.com/gitlab-magazine), and all GitLab team-members may be added as writers! To be added as a writer to the publication, [import](https://help.medium.com/hc/en-us/articles/214550207-Import-post) a blog post that you authored on about.gitlab.com/blog to your personal Medium account, and submit it to the GitLab publication (by hitting `edit` -> `submit to publication` -> `GitLab Magazine`). the Social Marketing Manager will approve you as a writer and help finalize the post before publishing. 

If you submit original content (i.e., not originally published somewhere else) to the publication for review, she may edit and publish your post. We want to highlight writers wherever possible, so we highly encourage you to import posts to your personal Medium.

**Content/Execution**
* Brand and thought leadership-focused
* Favorite articles about GitLab
* Aim to post one article to Medium every two weeks
* Cross-post our original thought-leadership posts to Medium, linking back to blog
  - Use the [import tool](https://help.medium.com/hc/en-us/articles/214550207-Import-post); do not copy and paste.

### YouTube <a name="youtube"></a>

**Content/Execution**
- Developer/community-focused
- Live events are more tech focused
  - Group Conversations, pick your brain meetings, demos, brainstorms, kickoffs

## Defining Social Media Sharing Information for web pages

**Per [Twitter's functionality](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started), we're moving to using custom OG tags across all owned pages, as well as, eliminating the redundant tags for Twitter cards.\
In our iterim process, please add opengraph images to `'/images/opengraph/page-name.png'`**

Social Media Sharing info is set by the post or page frontmatter, by adding two variables:

```yaml
description: "short description of the post or page"
twitter_image: '/images/tweets/image.png'

## Ensuring your Post Will Have a Functional Card and Image

When you post a link on Facebook or Twitter, either you can see only a link, or a full interactive card, which displays information about that link: title, **description**, **image** and URL.

For Facebook these cards are configured via [OpenGraph Meta Tags][OG]. Twitter Cards were recently set up for our website as well.

Please compare the following images illustrating post's tweets.

A complete card will look like this:

![Twitter Card example - complete][twitter-card-comp]

An incomplete card will look like this:

![Twitter Card example - incomplete][twitter-card-incomp]

Note that the [first post] has a **specific description** and the image is a **screenshot** of the post's cover image, taken from the [Blog landing page][blog]. This screenshot can be taken locally when previewing the site at `localhost:4567/blog/`.

```

This information is valid for the entire website, including all the webpages for about.GitLab.com, handbook, and blog posts.

### Images

All the images or screenshots for `twitter_image` should be pushed to the [www-gitlab-com] project at `/source/images/tweets/` and must be named after the page's file name.

For the second post above, note that the tweet image is the blog post cover image itself, not the screenshot. Also, there's no `description` provided in the frontmatter, so our Twitter Cards and Facebook's post will present the _fall back description_, which is the same for all about.GitLab.com.

For the handbook, make sure to name it so that it's obvious to which handbook it refers. For example, for the Marketing Handbook, the image file name is `handbook-marketing.png`. For the Team Handbook, the image is called `handbook-gitlab.png`. For Support, it would be named `handbook-support.png`, and so on.

### UTMs for tracking URLs

UTMs are used to track traffic sources & reach of posts/links. All external posts should contain a UTM parameter, please see [details in the Digital Marketing handbook](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#url-tagging).  

If you have questions or are unsure how to tag a URL please reach out to the Digital Marketing team &/or the Social Media Manager responsible for the campaign. 

### Description

The `description` meta tag [is important][description-tag]
for SEO, also is part of [Facebook Sharing][og] and [Twitter Cards]. We set it up in the
[post or page frontmatter](/handbook/marketing/blog/#frontmatter), as a small summary of what the post is about.

The description is not meant to repeat the post or page title, use your creativity to describe the content of the post or page.
Try to use about 70 to 100 chars in one sentence.

As soon as you add both description and social sharing image to a page or post, you must check and preview them with the [Twitter Card Validator]. You can also verify how it looks on the FB feed with the [Facebook Debugger].

### Examples

To see it working, you can either share the page on Twitter or Facebook, or just test it with the [Twitter Card Validator].

- Complete post, with `description` and `twitter_image` defined:
[GitLab Master Plan](/blog/2016/09/13/gitlab-master-plan/)
- Incomplete post, with only the `description` defined:
[Y Combinator Post](/blog/2016/09/30/gitlabs-application-for-y-combinator-winter-2015/)
- Incomplete post, with none defined: [8.9 Release](/blog/2016/06/22/gitlab-8-9-released/)
- Page with both defined: [Marketing Handbook](/handbook/marketing/)
- Page with only `twitter_image` defined: [Team Handbook](/handbook/)
- Page with none defined: [Blog landing page](/blog/)

## Various Other Details
### Sharing 3rd-party events (onsite or virtual) on organic social where a team member is speaking
The purpose of GitLab team members participating in 3rd-party events is to bring GitLab's message to the 3rd-party's audience and to gain more community. Therefore, it's contradictory for GitLab brand social channels to promote a team member speaker for a 3rd-party event. Periodically, if the speaking engagement is part of a critical campaign or would resonate with the zietgiest of the moment, we may tweet once for folks to register for the event. However, if you're the speaker, we encourage you to post to your own social channels so that your network can join the event. Share your post with the #social_media Slack channel and we'll like, comment, and maybe share your post!

<!-- Social Marketing Handbook - ref https://gitlab.com/gitlab-com/marketing/issues/524 -->
<!-- identifiers, in alphabetical order -->

[blog]: /blog/
[description-tag]: http://www.wordstream.com/meta-tags
[facebook debugger]: https://developers.facebook.com/tools/debug/
[first post]: /2016/07/19/markdown-kramdown-tips-and-tricks/
[OG]: https://developers.facebook.com/docs/sharing/webmasters#markup
[second post]: /2016/07/20/gitlab-is-open-core-github-is-closed-source/
[twitter card validator]: https://cards-dev.twitter.com/validator
[twitter cards]: https://dev.twitter.com/cards/overview
[twitter-card-comp]: /images/handbook/marketing/twitter-card-complete.jpg
[twitter-card-incomp]: /images/handbook/marketing/twitter-card-incomplete.jpg
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
