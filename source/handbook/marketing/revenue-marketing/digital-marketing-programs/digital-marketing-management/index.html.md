---
layout: handbook-page-toc
title: "Digital Marketing Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Digital Marketing Managers
Digital Marketers are responsible for all inbound marketing.

## DMP labels in GitLab

*Note*: Some of the following labels only exist on the Digital Marketing Programs project level.

* **Digital Marketing Programs**: General label to track all issues related to Digital Marketing Programs
* **SEO**: Used for any issues that require SEO support, including keyword research, reporting, etc.
* **SEM**: Used for issues that require organic and paid search initiatives
* **Paid Ads**: Used for any paid advertising campaign such as Google Ads
* **Paid Social**: Used for paid social campaigns such as LinkedIn InMail and Facebook Ads 
* **Site Optimization**: Used for any issues to optimize the website for conversions
* **Conversion Rate Optimization (CRO)**: Generic label used by Digital Marketing Programs Manager to track all issues related to conversion rate optimization (CRO) (*scoped labels below*)
    *  **CRO::A/B Test**: experiment using two or more variants of the same web page or email (A and B)
    *  **CRO::Analysis/Reporting**: conduct an analysis to determine patterns in data and identify potential experiments or tests to improve conversion rate
    *  **CRO::Copywriting/Editing**: used for copywriting/editing from CRO
    *  **CRO::Multivariate Test**: tests variants of two or more elements simultaneously to see which combination creates the best outcome
    *  **CRO::Redirect Test**: test separate web pages against each other
* **Hotjar**: used for requesting a heatmap for a page on the website from the Digital Marketing Programs team

## DMP Slack channels

*  `#digital-marketing`: General digital marketing conversation and questions
*  `#dmpteam`: Discussion for DMP team members

## Paid Digital Marketing 

### Goals and objectives accomplished with digital campaigns:
* Lead Generation
* Brand Awareness
* Event Registrations
* Webcast Views
* Content Downloads
* Website Traffic

### How does paid digital contribute to GitLab’s funnel?
* Top of Funnel: Introduce GitLab brand to potential customers with awareness and reach objectives.
* Middle of Funnel: Nurturing engaged prospects with educational messaging & content.
* Bottom of Funnel: Leading prospects to conversion by retargeting with relevant, personalized ad experiences.

### Digital Campaign Types
DMPs can recommend specific types based on your campaign goals. The most common type is Paid Social based on robust targeting criteria and successful performance in reach and lead volume.

#### Channels
* Paid Social
   - LinkedIn (InMail Messaging Ads and Sponsored Content Ads)
   - Facebook (Single Image & Video Ads, Lead Generation Ads, Carousel Ads)
   - Twitter (Promoted Tweets, Image or Video)
* Paid Search (Google and/or Bing)
* Paid Display
* Content Syndication
* Sponsorships: 
   - Virtual Event (Virtual Conference, Panel, Talking Head, All-Day Event/Summit)
   - Custom Webcasts (single & multi-vendor)
   - Microsites
   - Newsletter ads
   - Custom email blasts
   - Sponsored Custom & 3rd Party Content Creation (trend reports, ebooks, articles, etc.)


### Program definitions

#### **Paid search**: 
Paid search are [text ads](https://support.google.com/google-ads/answer/1704389?hl=en) on Google and/or Bing search engines to drive people to specific GitLab landing pages as they are looking for information on search engines. We do this by [bidding](https://support.google.com/google-ads/answer/2459326?hl=en) on targeted keywords and phrases based on the assumed intent of the person and matching that intent with a related landing page. 
* **Best used for**: Mid and bottom funnel content where we want someone to take action (ex: filling our a form to a gated asset, signing up for a demo, etc).
     * **Best type of content to use**: Use case type gated assets that directly applies to the intent for the search query. How to guides and ebooks, do well here. 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports. Caveat here is if the content is more of a how to report or guide. 

#### **Display ads**: 
Display ads are banner ads that we mostly run are through the [Google Display Network](https://support.google.com/google-ads/answer/2404190?hl=en). Banner ads will show on websites that have [Google Adsense](https://support.google.com/adsense/answer/6242051?hl=en) set-up on their website. There are no specific websites we show banner ads on - we earn the ad space by bidding on placements based on specific targeting criteria such as demographics, topics, and interests. On occassion, we run banner ads on websites through direct buys. This is handled more in the publisher program. 
* **Best used for**: Top and mid funnel content. More used for awareness and some action based response.
     * **Best type of content to use**: Use case type gated assets. How-to guides and ebooks do well here. 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports. Caveat here is if the content is more of a how to report or guide. 

* **Types of targeting we do**:
     * **Contextual targeting**: Show banners ads on websites that are related to the content of our landing page and website. This is done based on keyword and topic targeting.
     * **Prospecting targeting**: Show banner ads to related audiences that are similar to those who have converted on our website. A profile is developed based on people who convert. We would then show banner ads to people that closely match that profile. This method helps drive new traffic to GitLab that may not know about the brand or product. 
     * **Remarketing**: Show banner ads in order to re-engage people who have already visited pages on the GitLab website. This tactic can show ads on what seems to be irrelevant websites. However, targeting is based on the engagement of the person, not the context of a website. 
#### **Paid social**:
Paid social ads are ads that we show on social platforms. The three social media platforms that we primarily advertise on are Facebook/Instagram, LinkedIn (this includes LinkedIn InMail), and Twitter. 
* **Best used for**: Top and mid funnel content. Does well for both awareness and direct response (depending on the asset used).
     * **Best type of content to use**: Live webcasts, recorded webcasts, events, and ebooks/guides 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports.
* **Types of targeting we do**:
     * **Contextual targeting**: Show banners ads on websites that are related to the content of our landing page and website. This is done based on keyword and topic targeting.
     * **Prospecting targeting**: Show banner ads to related audiences that are similar to those who have converted on our website. A profile is developed based on people who convert. We would then show banner ads to people that closely match that profile. This method helps drive new traffic to GitLab that may not know about the brand or product. 
     * **Remarketing**: Show banner ads in order to re-engage people who have already visited pages on the GitLab website. This tactic can show ads on what seems to be irrelevant websites. However, targeting is based on the engagement of the person, not the context of a website.

#### **Publisher sponsorships**:
Publisher sponsorships are when we engage a specific publisher in order to purchase placement on their web properties. Generally, we make sure the publisher's website(s) and audience closely match the profile of who we want to advertise to before engaging with the publisher. Additionally, we want to make sure the programs that are offered by the publisher align with our goals. 
* **Best used for**: Primarily used for demand generation, so we focus on mid to bottom funnel content.
     * **Best type of content to use**: Live webcasts and recorded webcasts work the best. Ebooks and guides sometimes work, depending on the placement. 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports.
     
### Digital Campaign Design Specs
Each paid channel has its own unique design specifications and recommendations for their ad types to ensure ads can run at their optimal performance. If you do not yet have creative assets secured for your campaign, the design team can use this section as their guide when producing your creative.

#### Paid Social
* Facebook Image:
   - Recommended Image Size: 1200x628 pixels
   - Recommended Image Ratio: 1.91:1 to 4:5
   - Recommended Image File Type: JPG or PNG
   - No buttons allowed
   - Do no include test in the creative
* Facebook Video:
   - Recommended Video Ratio: 9:16 (full vertical) to 16:9 (feed/landscape)
   - Recommended Video File Type: MP4 or MOV
   - Recommended Video File Size: 4GB Max
   - Recommended Video Length: 5-15 seconds
   - Video Captions: Optional but recommended
   - Video Sound: Optional but recommended
* LinkedIn Image:
   - Recommended Image Size: 1200x627 pixels
   - Recommended Image Ratio: 1.91:1
   - Recommended Image File Type: JPG or PNG
   - Recommended Image File Size: 5MB Max
   - Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message
* LinkedIn Video: 
   - Recommended Video Length: Less than 15 seconds
   - Recommended Video File Size: 75 KB to 200 MB
   - Recommended Video File Format: MP4
* Twitter Image: 
   - Recommended Image Size: 1200x675 pixels
   - Recommended Image Ratio: 1:1
   - Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message
* Twitter Video:
   - Recommended Video Size: 1200x1200
   - Recommended Video Ratio: 1:1
   - Recommended Video Length: less than 15 seconds
   - Recommended Branding: Consistent in upper left-hand corner
   - Recommended Video File Type: MP4 or MOV
   - Recommended Video File Size: 1GB Max

#### Paid Display (Google Display Network)
* Image Sizes
   - 160x600
   - 250x250
   - 300x1050
   - 300x250
   - 300x600
   - 320x50
   - 336x280
   - 728x90
   - 970x250
* Recommended Image File Type: JPG or PNG
* Recommended Image File Size: 150KB Max
* In-Banner Design High Performers
   - Benefit/Value Prop and educational messaging
   - CICD emblem background
   - “Learn More” CTA
   - 4-7 word volume

## Requesting Digital Marketing Promotions

If you would like to request a paid digital marketing promotion in paid search, paid social, paid sponsorships or other paid marketing to support your event, content marketing, or webcast, asset, etc. create an issue in the [Digital Marketing Programs Repo](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues) and the follow [Paid Digital Request template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=paid-digital-request)

If you request a digital marketing promotion you probably also need a marketing campaign and should consult with the [Marketing Program Managers](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/).

## Requesting LinkedIn InMail as part of campaign

LinkedIn InMail campaigns are a good way to reach people at specific companies, industries, and more. They work best for inviting people to events and webinars if you do not have specific people in mind (unlike a Marketo campaign). If you would like to request an InMail campaign as part of your marketing campaign, create an issue in the [Digital Marketing Programs Repo](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues) and use the [`linked-inmail`](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/linkedin-inmail.md) template. InMail campaigns must send messages on behalf of a GitLab team member, usually an SDR. Although Digital Marketing can generate copy, we highly recommend that the sender or another member of the sales team generate the copy to be used in messages since they have more insight & context around targeted accounts.

InMail Best Practices:
* Customize the greeting with the member’s name 
* Refer to their job title  
* Try using the word “you"
* The copy is fewer than 1,000 characters
* The CTA is clear
* The landing page is optimized for mobile

Best performing subject lines often use some of the following keywords:
* Thanks 
* Exclusive invitation  
* Connect 
* Job opportunities  
* Join us  

Additional ways that we can test with message tone of voice:
* Genuine
* The Helpful Advisor
* VIP Invitation - sending personalized invites to 'exclusive' events
* The Cliffhanger

## Create a Culture of Testing and Optimization

### Conversion Rate Optimization (CRO)

A process of collecting and analyzing data, formulating hypotheses, and conducting experiments to increase the percentage of website visitors that convert into leads or potential customers. Testing and experiments generally occur on website pages that include desired actions such as watching a product demo, signing up for a newsletter, or signing up for a free product trial. This also extends to email recipients in email marketing wherein the recipient responds to the main call-to-action (CTA) of an email, driving additional traffic to the website, and converting into a new customer or upsell a current customer.

**Goals**

1. Understand what’s working and what’s not
1. Gain business insight
1. Improve customer experience

### Statistical significance

In order to draw accurate conclusions from a test, a large data set (sample size, time) is required. Customer behavior is not always consistent and responses fluctuate between times, offerings, and segments (e.g. device type, location, new vs. returning visitors). Other considerations include seasonality, competitor landscape, and industry evolution.

When analyzing results, it’s important to take different segments into account. Testing one element may reveal statistical significance in one area and affect another poorly. For example, an increase in conversions on the website via desktop could cause a decrease in conversions for mobile and vice versa. 

### Always be testing
In order to iteratively improve the site, we should test all major site changes before implementing. Testing requires a control and a variant within the same time period, while holding all other variables constant. Using a/b testing tools will allow us to create tests that follow testing best practices and gather data about what works or does not work to encourage people to spend time on our site or have a [conversion event](/handbook/business-ops/resources/#mql-scoring-model). With testing, we can make informed decisions about what works for our audience and helps them reach their goals, and what works for us to help use meet our business goals.

### Types of experiments

#### Website tests

* **A/B Test:** tests two variants of a page
* **A/B/n Test:** tests two or more variants of a page
* **Multivariate Test:** tests variants with two or more different sections
* **Redirect Test:** tests separate web pages identified by different URLs or paths
* **Personalization:** personalize your page for targeted visitors

#### Email A/B tests

**Body**
* Copy
* Images
* Layout

**Send Time**
* Time of day
* Day of week
* Personalized for each user

**Subject Line**
* Long
* Short
* Happy
* Serious
* Emojis

**Call-to-Action (CTA)**
* Button styling
* Copy

### CRO Process and Framework

Rather than simply testing ideas that we feel are relevant, we focus on building a solid hypothesis using data and information to ensure we are testing a hypothesis with the best chances of conversion. Theoretically, there are two approaches to building a hypothesis:

1. **Inductive approach:** Brainstorm a set of ideas and then look at the data to validate those ideas and form a hypothesis.
1. **Deductive approach:** Look at patterns in observations first, and then deducing a hypothesis for testing.

### Requesting a Website or Email Test

1. Build a test: If you would like to request an a/b or multivariate test, create an issue for [Digital Marketing Programs using this template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=ab-test).
1. CRO process and MVCs: Not all changes to pages need to be tested, only pages that we want to optimize for [conversion activities](/handbook/business-ops/resources/#mql-scoring-model). These pages/elements are often being tested:
    * [Homepage](/)
    * Top navigation
    * [Pricing page](/pricing/)
    * [Free trial](/free-trial/)
    * [Contact sales](/sales/)
1. Please check the [A/B and CRO issue board](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/boards/1086590?&label_name[]=Conversion%20Optimization&label_name[]=a%2Fb%20test) to see if a page you plan to update is being tested and also to see if a page you want to test is being tested or in plan to test. The URL will be the first word or `HP` for the homepage. If you have an MVC update to any of the pages in the `Doing` column, let us know in the `#digitalmarketing` Slack channel and we can scheduled tests around MVC updates or determine if updates can wait for tests to finish. Tests take 1 day to 4 weeks depending on traffic and conversion volume.

### Setting up a CRO test

We use [Google Optimize](https://optimize.google.com/optimize/home/#/accounts/4049624381/containers/9871021) to move and replace on-page elements for testing. If you need access to Optimize, please reach out to the Digital Marketing Programs team.

Google Optimize recommends using an anti-flicker snippet. Without this script installed, visitors may experience the page flickering and briefly see the original page before the test loads. This can skew the results of the test and confuse visitors. Tests can and will run without the anti-flicker snippet installed, but there is a chance it running without can impact visitor experience.

To install the anti-flicker script, add `google_optimize: true` to pages with active tests.

There's an `Active tests` section in CODEOWNERS for about.gitlab.com to notifiy the DMP team when changes are made to pages with active tests.

### Active Experiments

### Requesting a Hotjar heatmap test or report

[Use this link](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=hotjar-heatmap-request) to request a heatmap test or results for a page on `about.gitlab.com`.

New heatmaps will record data until the page has reached 2,000 pageviews. A digital marketing program manager will ping you when the results are complete (Duration depends on average traffic to the page. Some tests can take up to a month or more.)

**Note:** Every page viewed by a visitor is counted as a single pageview.

## UTMs for URL tagging and tracking
All URLs that are promoted on external sites and through email must use UTM URL tagging to increase the data cleanliness in Google Analytics and ensure marketing campaigns are correctly attributed. 

We don't use UTMs for internal links. UTM data sets attribution for visitors, so if we use UTMs on internal links it resets everything when the clicked URL loads. This breaks reporting for paid advertising and organic visitors.

You can access our internal [URL tagging tool in Google Sheets](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0). You will also find details in this spreadsheet on what "Campaign Medium" to use for each URL. If you need a new campaign medium, please check with the Digital Marketing Programs team as new mediums will not automatically be attributed correctly.

If you are not sure if a link needs a UTM, please speak with the marketer who is managing your campaign to ensure you are not interrupting the reporting structure they have in place.

UTM construction best practices:
- lowercase only, not camelcase
- alphanumeric characters only
- no spaces
- **Campaign Medium** covers general buckets like `paidsearch`, `social`, or `sponsorship` 
- **Campaign Source** names where the link lives. Examples include `ebook`, `twitter`, or `qrcode`
- **Campaign Name** describes a specific campaign. Try to add additional context like `reinvent`, `forrester`, and `bugbounty`
- **Campaign Content** differentiates ad types.
- **Campaign Term** identifies keywords used in a campaign