---
layout: handbook-page-toc
title: "Virtual Events"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

At GitLab, we believe everyone can contribute. Because nearly anyone with an internet connection and a device can participate in virtual events, as a speaker or an attendee, virtual events are an important part of fulfilling that mission. While our expertise in this area is still developing, this page documents the best practices we have studied and established for virtual events at GitLab. 

## GitLab virtual events decision tree

We have developed a decision tree to help you determine what type of GitLab-hosted virtual event is the best fit for you: 

<div style="width: 600px;" class="embed-thumb"> <h1 style="position: relative;vertical-align: middle;display: inline-block; font-size: 24px; line-height:22px; color: #393939;margin-bottom: 10px; font-weight: 300;font-family: Proxima Nova, sans-serif;"> <img src="https://app.mural.co/avatar/zbadgley6632" style="position: absolute; border-radius: 50%; width: 36px;height: 36px;margin-right: 14px; display: inline-block; margin-top: -6px;margin-right: 10px; vertical-align: middle;"> <div style="padding-left:50px"> <span style="max-width:555px;display: inline-block;overflow: hidden; white-space: nowrap;text-overflow: ellipsis;line-height: 1; height: 25px; margin-top: -3px;">VE Decision Tree - Final</span> <span style="position:relative;top:-3px;font-size: 16px; margin-top: -6px; line-height: 24px;color: #393939; font-weight: 300;"> by Zac Badgley</span> </div> </h1> <div style="position: relative; height: 0;overflow: hidden; height: 400px; max-width: 800px; min-width: 320px; border-width: 1px; border-style: solid; border-color: #d8d8d8;"> <div style="position: absolute;top: 0;left: 0;z-index: 10; width: 600px; height: 100%;background: url(https://murally.blob.core.windows.net/thumbnails/gitlab5736/murals/gitlab5736.1588010782243-5ea71f1ef275c73ba07ac175.png?v=666a7cc9-8361-4d90-b1f1-534b392a4bff) no-repeat center center; background-size: cover;"> <div style="position: absolute;top: 0;left: 0;z-index: 20;width: 100%; height: 100%;background-color: white;-webkit-filter: opacity(.4);"> </div> <a href="https://app.mural.co/t/gitlab5736/m/gitlab5736/1588010782243/b0870b7180e578f51b1dcf6d190032cb6dc73b25" target="_blank" rel="noopener noreferrer" style="transform: translate(-50%, -50%);top: 50%;left: 50%; position: absolute; z-index: 30; border: none; display: block; height: 50px; background: transparent;"> <img src="https://app.mural.co/static/images/btn-enter-mural.svg" alt="ENTER THE MURAL" width="233" height="50"> </a> </div> </div> <p style="margin-top: 10px;margin-bottom: 60px;line-height: 24px; font-size: 16px;font-family: Proxima Nova, sans-serif;font-weight: 400; color: #888888;"> You will be able to edit this mural. </p></div>

## Virtual event types

### Webcast

This is a Marketing Programs hosted virtual event with `Webcast` type configuration in zoom. This is the preferred setup for larger GitLab hosted virtual event (up to 500 attendees) that **requires registration** due to the integration with Marketo for automated lead flow and event tracking. GitLab hosted webcast type is a single room virtual event that allows multiple hosts. Attendees cannot share audio/video unless manually grated access. For this virtual event type, MPM supports program creation, driving registration, infrastructure, live moderation, and followup.


### Self-service

This is a light weight virtual event that can be hosted on GitLabber's personal zoom. This is recommended for smaller virtual events (200 attendees max) and **allows you to break the audience into smaller groups during the event. Attendees are able to be interactive in this event type, having the option to share both audio/video if allowed by the host. We can track registration, but there is NO Marketo integration, which requires manual list upload to Marketo post event. For this virtual event type, no MPM program support pre/post event will be required.

### Self-service w/ promotion

This is the same format as a self-service event, except **MPM will support Marketo/SFDC program creation, 1-2 invitations, assist with list handoff, and a send follow-up email**.

### Corporate Marketing event 

Please work with the Corporate Marketing events team on planning these large scale events. 

### Sponsored virtual events

Note: these events are not included in the above decision tree as they are not GitLab-hosted events.

* **Sponsored demand gen webcast:** This is webcast hosted on an external partner/vendor platform (e.g: DevOps.com). The partner/vendor is responsible for driving registration, moderating and hosting the webcast on their platform. Mktg-OPs will be responsible for uploading the list to our database and MPMs will be responsible for sending post-event follow-up emails. *[Link to Marketo program template.](https://app-ab13.marketo.com/#ME4634A1)*
* **Virtual Conference:** This is not a webcast but rather a virtual conference where we pay a sponsorship fee to get a virtual booth and sometimes a speaking session slot. Marketing programs will primarily be responsible for sending the post-event follow-up emails. *[Link to Marketo program template.](https://app-ab13.marketo.com/#ME4739A1)*


### GitLab-hosted Virtual events details

|                           | Webcast | Self-service |  Self-service w/ promotion   | Virtual Conference  |
|---------------------------|---------|---------------------|-----------------------|---|
| SLA                       | 45 BD   | 5 BD                | 21 BD                 | 45 BD+  |
| Type                      | Webcast | Meeting             | Meeting               | Conference  |
| Tracking                  | Yes     | No                  | No                    | Yes  |
| LP/Registration           | Yes     | Optional<br>(Zoom)  | Optional<br>(Zoom)    |   |
| Mktg<br>Promotion         | Yes     | No                  | Yes                   | Yes  |
| Mktg<br>Moderate          | Yes     | No                  | No                    |  Yes |
| Mktg <br>Followup         | Yes     | No                  | Yes                   | Yes  |
| Breakout<br>Rooms         | No      | Yes                 | Yes                   | Yes  |
| Polls/Q&A                 | Yes/Yes | Yes*/No             | Yes*/No               |  Yes |
| Confirmation/<br>Reminder | Yes/Yes | Yes/No*             | Yes/No*               |  Yes  |

**FAQ & Notes:**
* Breakout rooms CANNOT be selected by attendees. Only the host can add attendees to rooms. Within each breakout room, Co-hosts can record when given permission by the host.
* For self-service type events, the requestor is responsible for the zoom setup, live moderation, recording and upload (GitLab Unfiltered - Unlisted). Self-service w/ promotion events follow the same requestor responsibilities, with the addition of providing MPM with email and follow-up copy. 

## GitLab virtual events calendar

We have 3 separate calendars to manage different types of virtual events in GitLab. All 3 calendars will be consolidated into a single GitLab virtual events calendar view (below). **The purpose of the consolidated view is to help event organizer(s) minimize topic overlap with other GitLab virtual events happening around the same time and to provide executive visibility into ALL virtual events that GitLab is running/participating in.**

#### Glossary of calendar event naming convention:

* `[Hold WC Hosted] Webcast title` - GitLab hosted webcast still in planning
* `[WC Hosted] Webcast title` - Confirmed GitLab hosted webcast
* `[DR WC Hosted] Webcast title` - Dry run for GitLab hosted webcast
* `[Hold self-service] Event title` - Self-service virtual event still in planning
* `[Self-service] Event title` - Confirmed self-service virtual event 
* `[Hold WC sponsored] Webcast title` - Sponsored webcast still in planning
* `[WC sponsored] Webcast title` - Confirmed sponsored webcast
* `[Hold VC sponsored] Conference title` - Sponsored virtual conference still in planning
* `[VC sponsored] Conference title` - Confirmed sponsored virtual conference

<figure>
<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23B39DDB&amp;ctz=America%2FLos_Angeles&amp;src=Z2l0bGFiLmNvbV9uMnNibXZmMjlqczBzM3BiM2ozaHRwa3FmZ0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Z2l0bGFiLmNvbV8xOGVqOHN0NmlxajZpYXB1NTNrajUzNHBsa0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23039BE5&amp;color=%239E69AF&amp;color=%23009688&amp;title=All%20GitLab%20Virtual%20Events&amp;showCalendars=1" style="border:solid 1px #777" width="1000" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

### [Webcasts calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t)

This calendar will be used to log all planned and scheduled Marketing Programs hosted webcasts and their subsequent dry runs. **The purpose of the webcast calendar is to ensure MPMs don't schedule overlapping webcasts when using the shared webcast license and to provide executive visibility into all webcasts that MPMs are hosting.**

**DRI for adding to webcast calendar: MPM executing the webcast.**

#### Process for logging webcasts into the calendar

##### Planned webcasts:

1. As soon as an issue is created for a webcast request, add the planned webcast to the webcast calendar by creating an event on the day you plan to host the webcast. For webcasts that are still in planning, use the following naming convention `[Hold WC Hosted] Webcast title` (e.g: `[Hold WC Hosted] Mastering CI`) and create it as an all-day event (no time slot selected). Make sure to also include the link to the issue in the calendar description.
2. Please also add the planned webcast to the FY21 webcast planning issue. When adding to the issue, please use :asterisk: emoji prior to the webcast name to indicate this is still in planning.

##### Confirmed webcasts:

1. Once the date/time of the webcast has been confirmed, go to your calendar event and remove `Hold` from the event title `[WC Hosted] Webcast title` (e.g: `[WC Hosted] Mastering CI`). Specify the time on the calendar event and swap the issue link in the calendar description with the Epic link. *Note: For security reasons, do not add zoom links/invite presenters using this webcast calendar event, rather send the invite with zoom links to presenters from your personal google calendar.*
2. On the FY21 webcast planning issue, please also update the webcast as confirmed by switching out the :asterisk: emoji with :white_check_mark: emoji.
3. Make sure to also add dry runs to the webcast calendar. When creating the webcast dry run event(s), please use the following naming convention `[DR WC Hosted] Webcast title` (e.g: `[DR WC Hosted] Mastering CI`) and specify the date/time on the calendar event.

##### Rescheduled webcasts:

If your webcast needs to be rescheduled, please update the date/time of the webcast on the webcast calendar and the  FY21 webcast planning issue.

##### Canceled webcasts:

If a webcast is canceled, please remove the webcast from the webcast calendar and the  FY21 webcast planning issue.

### [Self-service virtual event calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9uMnNibXZmMjlqczBzM3BiM2ozaHRwa3FmZ0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t)

This calendar will be used to log all planned and scheduled self-service virtual events hosted by GitLab employees using their personal zoom accounts. **The purpose of this calendar is to provide visibility and help individuals minimize topic overlap with other GitLab virtual events happening around the same time and to provide executive visibility into all self-service events GitLabbers are hosting.**

**DRI adding to self-service virtual event calendar: Individual hosting the virtual event.**

#### Process for logging self-service virtual event into the calendar

##### Planned self-service virtual events:

As soon as you create the epic for your self-service virtual event, add your event to the self-service virtual event calendar by creating an event on the day you plan to host it. Make sure to also include the link to the epic in the calendar description.

For self-service events that still in planning, use the following naming convention `[Hold Self-service] Event title` (e.g: `[Hold Self-service] How to use GitLab for project planning`) and create it as an all-day event (no time slot selected).

##### Confirmed self-service virtual events 

Once the date/time of the event has been confirmed, go to your calendar event and remove `Hold` from the event title to `[Self-service] Event title` (e.g: `[Self-service] How to use GitLab for project planning`). *Note: For security reasons, do not add zoom links/invite presenters using this webcast calendar event, rather send the invite with zoom links to presenters from your personal google calendar.*

##### Rescheduled self-service virtual events 

If the event needs to be rescheduled, please update the date/time of your event on the self-service virtual events calendar.

##### Canceled self-service virtual events 

If the event is canceled, please remove your event from the self-service virtual events calendar.

### [Sponsored virtual events calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xOGVqOHN0NmlxajZpYXB1NTNrajUzNHBsa0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t)

This calendar will be used to log all planned and scheduled sponsored webcasts and virtual conferences. **The purpose of this calendar is to provide visibility and help the sponsoring team minimize topic overlap with other GitLab virtual events happening around the same time and to provide executive visibility into all GitLab sponsored virtual events.**

**DRI adding to sponsored virtual events calendar: Sponsor owner**

#### Process for logging sponsored virtual events into the calendar:

##### Planned sponsored virtual events

As soon as you create the epic for the sponsored virtual event, add the event to the sponsored virtual events calendar by creating an event on the day the sponsored virtual event will be live.Make sure to also include the link to the epic in the calendar description.
* For sponsored webcast please use the following naming convention `[Hold WC sponsored] Event title` (e.g: `[Hold WC sponsored] Securing your pipeline with GitLab and WhiteSource`).
* For sponsored virtual conferences, please use the following naming convention  `[Hold VC sponsored] Event title` (e.g: `[Hold VC sponsored] Predict 2021`).

##### Confirmed sponsored virtual events 

Once the sponsorship has been confirmed, go to your calendar event and remove `Hold` from the event title.
* For sponsored webcasts, change the event title to `[Hold WC sponsored] Event title` (e.g: `[WC sponsored] Securing your pipeline with GitLab and WhiteSource`).
* For sponsored virtual conferences, change the event title to  `[Hold VC sponsored] Event title` (e.g: `[VC sponsored] Predict 2021`).

##### Rescheduled sponsored virtual events

If the sponsorship needs to be rescheduled, please update the date/time of the event on the sponsored virtual events calendar.

##### Canceled sponsored virtual events

If the sponsorship is canceled, please remove the event from the sponsored virtual events calendar.

## How to go live?

### Self-service virtual events 

In the spirit of effiecency, we encourage team members to host self-service events when they have interesting content to share with the GitLab community. See best practices for producing self-service events in the [self-service virtual events execution page](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/index.html). 

### Webcasts 

Currently, only the Marketing Programs team can run GitLab hosted virtual event with `Webcast` type configuration in zoom since this configuration requires a special Zoom license and integration with Marketo.

#### How to request Marketing Programs support to run a GitLab hosted webcast.

**⚠ Note: MPM led webcast requests should be submitted no less than 45 business days before the event date so the new request can be added into the responsible Marketing Program Manager's (MPM) workflow and to ensure we have enough leeway for promotion time.**

**Step 1: Create a [virtual event request issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM_VirtualEvent_Request.md) in the digital marketing programs project.**
*  Please use the `MPM_VirtualEvent_Request.md` issue template linked above
*  Please put the date of the virtual event as a due date
*  Please specify the type of virtual event
*  @ mention MPM in the issue comment to confirm the requested date is feasible
*  MPM will check the requested date against [the virtual events calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) to make sure there is no overlapping virtual event that has been pre-scheduled
*  If the requested date is feasible and the speakers have been secured the Marketing Programs team will change the status label from `status:plan` to `status:wip`, add the `Webcast` label and applicable `FY..` label (to make sure this appears on the [webcast issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/922606?&label_name[]=Webcast))

**Step 2: MPM will create the Virtual Event EPIC.**
*  When "status:wip" is on the issue and necessary elements are documented, MPM creates epic for the virtual event.
*  Naming convention: [Virtual Event Name] - [3-letter Month] [Date], [Year]
*  MPM copy/pastes code below into the epic

```
## [Main Issue >>]()

## [GANTT >>]()

## [Copy for landing page and emails >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## Event Details
  * `place details from the event issue here`
  * [ ] [main salesforce campaign]()
  * [ ] [main marketo program]()

## Issue creation

* [ ] Secure presenters and dry runs issue created - MPM
* [ ] Only for Virtual sponsorship and 3rd Party Hosted webcast: List clean and upload issue created - MOps
* [ ] Landing page issue created - MPM
* [ ] Optional: New design assets issue created for the design team - MPM
* [ ] Invitation and reminder issue created - MPM
* [ ] Organic social issue created for social media manager - MPM
* [ ] Paid Ads issue created for DMP - MPM
* [ ] PathFactory request issue created - MPM
* [ ] Follow up email issue created - MPM
* [ ] Add to nurture stream issue created - MPM
* [ ] On-demand switch issue created - MPM

cc @jburton to create list upload issue (Only for Virtual sponsorship and 3rd Party Hosted webcast)

```
**Step 3: MPM will create the necessary MPM support issues (linked in table below) and add to epic.**

The table below shows execution tasks related to various types of Virtual Events along with the DRI for each task.

MPMs will follow this [step by step intructions when executing all webcasts.](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/)*

<div class="tg-wrap">
<table class="tg">
  <tr>
    <th class="tg-k6pi">Tasks</th>
    <th class="tg-k6pi">Due dates</th>
    <th class="tg-yw4l">Hosted demand gen webcast</th>
    <th class="tg-yw4l">Sponsored demand gen webcast</th>
    <th class="tg-yw4l">Sponsored Virtual Conference</th>
    <th class="tg-yw4l">Account specific webcast</th>
  </tr>
  <tr>
    <td><strong> <a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/%20MPM-00-secure-presenters_dryruns.md">Secure presenters</a></strong></td>
    <td>-45 business days</td>
    <td>MPM or Requestor</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
  </tr>
  <tr>
    <td><strong> Create MPM Epic, issues, and fill out GANTT</strong></td>
    <td>-43 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
  <tr>
    <td><strong> Host prep call with internal team</strong></td>
    <td>-40 business days</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>Only for large segment accounts: MPM</td>
  </tr>
   <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-02-landing-page.md"> Landing page copy due </a></strong></td>
    <td>-35 business days</td>
    <td>Requestor or Presenter</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?nav_source=navbar">New design assets delivered</a></strong></td>
    <td>-35 business days</td>
    <td>Design</td>
    <td>Design</td>
    <td>Design</td>
    <td>Design</td>
  </tr>
  <tr>
    <td><strong> <a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-02-landing-page.md"> Create landing page MR </a> </strong></td>
    <td>-33 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>MPM</td>
  </tr>
  <tr>
    <td><strong> Set up tracking in SFDC, Marketo, and Zoom </strong></td>
    <td>-33 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
   <tr>
    <td><strong> Drive registration - create issue for blog post promotion </strong></td>
    <td>-30 business days</td>
    <td>Optional: Requestor</td>
    <td>Only if we have paid ads budget: Requestor</td>
    <td>Only if we have paid ads budget: Requestor</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td><strong> Paid Ads LIVE </strong></td>
    <td>-30 business days</td>
    <td>DMP; MPM creates the Paid Ads request issue</td>
    <td>Only if we gave paid ads budget: DMP; Requestor creates the Paid Ads request issue</td>
    <td>Only if we gave paid ads budget: DMP; Requestor creates the Paid Ads request issue</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03B-virtualevent-promotion.md"> Promote in bi-weekly newsletter </a> </strong></td>
    <td>-21 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/corporate-marketing/blob/master/.gitlab/issue_templates/social-event-request.md"> Organic social promotion event support </a> </strong></td>
    <td>-21 business days</td>
    <td>MPM creates organic social request issue</td>
    <td>Requestor creates organic social request issue</td>
    <td>Requestor creates organic social request issue</td>
    <td>Optional(to be discussed in prep call): MPM creates organic social request issue</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03-invitations-reminder.md"> Marketing email invite copy written </a> </strong></td>
    <td>-21 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03-invitations-reminder.md"> Marketing email invite scheduled </a> </strong></td>
    <td>-16 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong> Presentation slides due </strong></td>
    <td>-7 business days</td>
    <td>Requestor or Presenter</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>Optional (to be discusseed in prep call): SALs</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-04-follow-up-email.md"> Post event follow up email copy written </a></strong></td>
    <td>-5 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/%20MPM-00-secure-presenters_dryruns.md">  Host dry run </a> </strong></td>
    <td>-5 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong> Host virtual event</strong></td>
    <td>0 business days</td>
    <td>MPM</td>
    <td>3rd party vendor</td>
    <td>3rd party vendor</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong> Moderate webcast</strong></td>
    <td>0 business days</td>
    <td>MPM or Presenter</td>
    <td>3rd party vendor</td>
    <td>N/A</td>
    <td>SALs</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-06-on-demand-switch.md"> Upload live webcast recording to youtube</a></strong></td>
    <td>+1 business days</td>
    <td>MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (to be discussed in prep call): MPM</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/pathfactory_request.md"> Upload youtube recording to PathFactory</a></strong></td>
    <td>+1 business days</td>
    <td>DMP</td>
    <td>Optional (if available): DMP</td>
    <td>Optional (if available): DMP</td>
    <td>Optional (to be discussed in prep call): MPM</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-06-on-demand-switch.md"> Convert registration page to on-demand </a> </strong></td>
    <td>+2 business days</td>
    <td>MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (to be discussed in prep call): MPM</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/blob/master/.gitlab/issue_templates/event-clean-upload-list.md"> Forward cleaned lead list to MOPs for upload </a></strong></td>
    <td>+2 business days</td>
    <td>N/A</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/blob/master/.gitlab/issue_templates/event-clean-upload-list.md"> MOPs finished uploading list to SFDC (CRM) </a></strong></td>
    <td>+4 business days</td>
    <td>N/A</td>
    <td>MOPs</td>
    <td>MOPs</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-04-follow-up-email.md"> Send marketo follow up emails </a></strong></td>
    <td>+2 to +5 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong> Facilitate retrospective </strong></td>
    <td>+7 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>N/A</td>
  </tr>
  </table>
</div>


## How to promote your virtual event?

### Self-service virtual event promotion guide

**If you have not yet identified if a self-service virtual event is the correct event type of event for you, please scroll to the top of this page to find out.**

Self-service events are organized for quick turnarounds and additional support or guidance from the team is not necessary. Using personal channels to self-promote your event comes with many benefits: it’s an authentic way for you to build your personal audience, reach people beyond GitLab’s corporate channels, and boosts your credibility online. This page outlines everything that you'll need to promote your own virtual event. `This checklist should take less than 2 hours`. 

Please consider reviewing and applying our [GitLab Team Member Social Media Guidelines](https://about.gitlab.com/handbook/marketing/social-media-guidelines/) prior to promoting your own event. Specifically, review our [GitLab Voice information](https://about.gitlab.com/handbook/marketing/social-media-guidelines/#gitlab-voice).

#### Checklist of deliverables to get your self-service virtual event started (details below)
* [ ]  Copy to promote your event
* [ ]  Designs for creative/images
* [ ]  Getting the word out

#### How to write copy for your self-service virtual event
1. Gather a list of handles and hashtags to include in your social copy 
   * Your event topic is already a hashtag: #DevOps, #security, #RemoteWork, etc. Integrate these into your sentences naturally. 
   * Do you have a guest speaker? Talking about a company, person, or brand that would have a Twitter or LinkedIn page? Include the handle (@name) in the Twitter post - and tag the appropriate page on LinkedIn. 

* Do’s
   * DO use 1-3 hashtags in your posts, across social channels
      * Example: I’m so excited to announce that I’ll be speaking about #remote #DevOps at my webcast later this week! Join me and learn how @GitLab can help.
   * DO write in your own voice, be creative, and use emojis
      * Example: 🗣Everyone is talking about #remote #DevOps and at @GitLab it’s all I work on. 💻 Join me for a webinar to learn how we make it all work.
* Don’ts
    * DON’T hashtags the predicate, object, or complements in a sentence unless it’s a direct topic of your event.
      * Example: So #excited to #announce our new #event on #remote #DevOps… #GitLab #Tech #Webcast 

2. Top-Level messaging to aid in copy 
   * Write 3 sentences answering these questions: 
      * What topic is your virtual event about?
      * What is something that attendees will walk away with that provides them value? / Why should someone attend your virtual event? 
      * Why are you the person to listen to about this topic? / What makes you the authority?
   * Use the answer to these three questions to write all copy for your promotions. 
      * A "tweetable" (280 characters or less) event description - add this to your event landing page and use this as your copy for all social promo for your first post on Twitter and LinkedIn
      * Write actual social copy with your hashtags, handles, and the answers to the questions above. (If there are no handles that's okay. But there is always a hashtaggable topic. Lost? Ask #social_media on Slack)

#### How to make images for your self-service virtual event
Because this is a self-service virtual event, you'll also be creating your own image for promotion. Don't worry! We have everything you need.

Here's what you'll need to do:

* Use [GitLab's Virtual Events Promotional Templates on Canva](https://www.canva.com/design/DAD44foMGks/HGNo4ixNIq0D1FMTuXqVqQ/view?utm_content=DAD44foMGks&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton&mode=preview)
* Follow the directions on the first slide of the template.
* Once your image is made, click the "share" button, followed by the "download" button, and you've made your promotional image!

The GitLab Canva Virtual Events Promotional Templates are a small series of "blanks" to use to promote your event! You may choose any or all of these for your virtual events. You will need to learn basic photo editing tactics if you don't know them already, but Canva is user-friendly software. Lost? Ask #social_media on Slack for assistance.

#### Getting the word out - Launching your self-service virtual event promotion
Now that you’ve put together the copy and creative needed to promote your event, it’s time to get the word out. Depending on how much time there is until your webcast, you may be able to promote your event more than what we’ve outlined in this section. There are more details below on a more sustained promotion for events with more than 3 days of time between promotion launch and webcast.

We recommend doing all of the following: 

##### 1. Share your event on your social media channels
Promote your event on Twitter and LinkedIn. You may also do this on Facebook or other channels, but know that Twitter and LinkedIn are key. Take the copy + creative you made and send them out to your social followers. Be sure to “convert” your @handles from Twitter to tagging people and companies on LinkedIn. While you start with “@” like all social channels, you’ll want to make sure that you select the right person or company on LinkedIn. Here is how to try this out (https://www.linkedin.com/help/linkedin/answer/34936/mention-people-in-your-posts?lang=en). 

##### 2. Share your social media posts on GitLab Slack Channels related to your topic, Ask for team members to amplify your posts
Building personal credibility and extending reach for your event promotions is a critical part of success. Team members who work in the same space as your topic would be more likely to share on their channels than others. Here’s how to do it while living up to our values around informal communication: 
Example: Your webcast is the same from our examples above, about remote DevOps. One of the topics to focus on is `remote work`. In addition to being a hashtag in your social posts, `remote work` is also the topic for a GitLab Slack channel, `remote`. Team members in this channel share stories and opinions on your webcast topic. 

After sending out your first social media posts on Twitter and LinkedIn, copy the links to both posts. Here’s how to copy a link to your post on LinkedIn (https://www.linkedin.com/help/linkedin/answer/108024/finding-the-url-for-shared-content?lang=en). Here’s how to copy a link to your tweet on Twitter (https://help.twitter.com/en/using-twitter/tweet-and-moment-url). 

Take the links to your social media posts and add them to a Slack channel message. Send a variation of this message to the channel: 

> Hi Everyone,
> 
> I’m hosting a webcast on remote DevOps later this week. I would appreciate your support by retweeting or sharing my social posts.
> 
> Retweet My Tweet (make this a clickable link to your tweet)
> 
> Share My LinkedIn Post (make this a clickable link to your LinkedIn post)
> 
> Please let me know if you have any questions or would like to contribute to the webcast.

While sharing on your social media channels and other digital spaces should occur more than once if time allows, please do not message GitLab Slack channels more than once about your webcast promotion. Please feel free to provide an update to the slack channel about what worked well or how many people attended following the end of your webcast. 

##### 3. Consider your other affiliations for promotion
Are you a part of a tech-focused nonprofit? Do you have former coworkers who would be interested in your webcast? Are you a part of group chats, Facebook or LinkedIn groups, or some other channel where members might be interested in your topic? Share it with them as well. 

It’s important to note that you should be aware of sharing practices in your groups - some groups have a strict “no advertising” policy. For all Facebook and LinkedIn groups, consider reviewing any group rules and don’t get in trouble with the moderators. 

##### Extra: Specific Twitter options
While we’re not suggesting that you spam your audiences, Twitter is not used in the same way LinkedIn is. On the day of your webcast, consider tweeting a photo of you prepping the webcast, tweeting a “thank you” to everyone that attended, and even consider linking to a recording (if available) for those who could not join live. It’s completely acceptable for a list of tweets on the day of your webcast to include 3-5 tweets. Ultimately, if you have something else to say, say it on Twitter.

##### Extra: Getting the word out - More than one-day of promotion
If you’re planning a webcast and have more than 3 days between your first day of promotion and the actual webcast, you have time to add additional posts to your promotion. 

Do not promote your event more than once a day, at most. Remember, this is your network, don’t spam them.

Consider posting about non-webcast or work-related topics between webcast promotional posts.

Take the same copy you wrote in step 1 and remix it for future posts.

### Self-service with promotion

Below are Marketing promotion options for self-service virtual event, including requirements and DRI to contact to get the promotions started.

<div class="tg-wrap">
<table class="tg">
  <tr>
    <th class="tg-k6pi">Promotion type</th>
    <th class="tg-k6pi">Number of posts/sends</th>
    <th class="tg-yw4l">Turnaround time</th>
    <th class="tg-yw4l">DRI</th>
    <th class="tg-yw4l">Budget required?</th>
    <th class="tg-yw4l">Other requirements</th>
  </tr>
  <tr>
    <td> Organic soial post <br> (Twitter) </td>
    <td> 1x </td>
    <td> 5 Business days</td>
    <td> Social team</td>
    <td> No</td>
    <td> * Requestor would need to open a general social request issue.<br>
         * Need an image associated with the link to share (could be embedded in the landing page but can't be shared without an asset).<br>
         * Can only support English language events as of today.</td>
  </tr>
   <tr>
    <td><strong> Targeted email blast </strong></td>
    <td> 1-2x</td>
    <td>5 Business days</td>
    <td>MPM</td>
    <td>No</td>
    <td> * Requestor would need to open an invitation email request issue.<br>
         * Email copy need to be provided by requestor.<br>
         * Target list have to exist in Marketo/SFDC.<br> 
         * Minimum target audience size: 500 <br>
         * Pre-Req: Mkto/SFDC created by MPM</td>
  </tr>
  </table>
</div>

### Webcasts

Below is a summary of the promotion plan for webcasts. MPMs will be responsible for creating all promotion request issues as part of the webcast project management process.  

<div class="tg-wrap">
<table class="tg">
  <tr>
    <th class="tg-k6pi">Promotion type</th>
    <th class="tg-k6pi">Number of posts/sends</th>
    <th class="tg-yw4l">Promo LIVE date <br> (from webcast date) </th>
    <th class="tg-yw4l">DRI</th>
    <th class="tg-yw4l">Budget required?</th>
    <th class="tg-yw4l">Other requirements</th>
  </tr>
  <tr>
    <td> Blog merchandising </td>
    <td> 1x </td>
    <td> -30 Business days</td>
    <td> Content team</td>
    <td> No</td>
    <td> Need to be aligned to blog's content.</td>
  </tr>
   <tr>
    <td> Organic social <br> (mainly twitter, other channels at social teams discretion)</td>
    <td> 1-2x</td>
    <td> -7 to -2 Business days</td>
    <td> Social team</td>
    <td> No </td>
    <td> * Need an image associated with the link to share (could be embedded in the landing page but can't be shared without an asset)<br>
         * Can only support English language webcasts as of today</td>
  </tr>
   <tr>
    <td> Paid ads</td>
    <td> N/A</td>
    <td> -21 Business days</td>
    <td> DMP</td>
    <td> Yes </td>
    <td> Need gitlab owned pages (about.gitlab.com/marketo pages) </td>
  </tr>
   <tr>
    <td> Targeted emails</td>
    <td> 2-3x</td>
    <td> -14 /-7 business days /-2 hours (optional)</td>
    <td> MPM</td>
    <td> No </td>
    <td> * Email copy need to be provided by requestor<br>
         * Target list have to exist in Marketo/SFDC<br> 
         * Minimum target audience size: 500 <br>
         * Pre-Req: Mkto/SFDC created by MPM </td>
  </tr>
    <tr>
    <td> CTA in bi-weekly newsletter</td>
    <td> 1x</td>
    <td> -15 to -1 business day</td>
    <td> MPM</td>
    <td> No </td>
    <td> Can only support English language webcasts as of today. </td>
  </tr>
  </table>
</div>

## Virtual event operations 

#### Zoom capabilities

When scheduling a self-service event, this table can help guide you towards the right event type to select based on the features you would like to use during the event. GitLab hosted virtual events will need to fit into either: Zoom Webcast type or Zoom Meeting type.
 
|                                       | Zoom Webcast                | Zoom Meeting                                  |
|---------------------------------------|-----------------------------|-----------------------------------------------|
| Screenshare                           | Yes                         | Yes                                           |
| Video/Audio sharing                   | Host/Panelist only          | All participants                              |
| Participant<br>List                   | Visible to<br>host/panelist | All participants                              |
| Multiple Hosts                        | Yes                         | Yes                                           |
| Attendee Limit                        | 500                         | 200                                           |
| Registration<br>Zoom                  | Yes<br>(Optional)           | Yes<br>(Optional)                             |
| Registration<br>Marketo               | Yes                         | No                                            |
| Marketo<br>Integration                | Yes                         | No                                            |
| Automated Tracking<br>and Lead Flow   | Yes                         | No                                            |
| Confirmation Email                    | Yes <br>(from Zoom)         | Yes<br>(from Zoom)                            |
| Auto Reminder<br>Email Send           | Yes<br>(from Zoom)          | No*                                           |
| Practice Session                      | Yes                         | Yes*<br>(requires activating<br>waiting room) |
| Breakout Rooms /<br>Max participants  | 0 / 0                       | 50 / 200                                      |
| Polls                                 | Yes                         | Yes                                           |
| Q&A                                   | Yes                         | No                                            |
| Chat                                  | Yes                         | Yes                                           |
| Raise Hand                            | Yes                         | No                                            |
| Livestream                            | Yes                         | Yes                                           |

### Participant engagement

#### Resources

*  [Managing participants in webcast](https://support.zoom.us/hc/en-us/articles/115004834466-Managing-Participants-in-Webinar)
*  [Managing participants in a meeting](https://support.zoom.us/hc/en-us/articles/115005759423-Managing-participants-in-a-meeting)

#### Chat

*  [In-meeting chat](https://support.zoom.us/hc/en-us/articles/203650445-In-Meeting-Chat) / [Save in-meeting chat](https://support.zoom.us/hc/en-us/articles/115004792763-Saving-In-Meeting-Chat)
*  [Webcast chat](https://support.zoom.us/hc/en-us/articles/205761999-Webinar-Chat)

#### Q&A

The question & answer (Q&A) feature for webcasts allows attendees to ask questions during the webcast and for the panelists, co-hosts and host to answer their questions. With the public Q&A feature, attendees can answer each other's questions and if enabled, they may also upvote each others questions.

*  [Getting started with question & answer](https://support.zoom.us/hc/en-us/articles/203686015-Getting-Started-with-Question-Answer)

#### Polling

You can enable polling in your meeting or virtual event to survey your attendees. See the [prerequisites](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#prerequisites) for how to enable polling.

You can also download the results of your poll - see [reporting](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#reporting).

*  [Polling for webcasts](https://support.zoom.us/hc/en-us/articles/203749865-Polling-for-Webinars)
*  [Polling for meetings](https://support.zoom.us/hc/en-us/articles/213756303-Polling-for-Meetings)

### Security

Please follow the [prerequisites](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#prerequisites) when setting up your virtual event. The [prerequisites](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#prerequisites) include steps for ensuring your virtual event and account are secure.

### Reporting

#### Resources

*  [Getting started with reporting](https://support.zoom.us/hc/en-us/articles/201363213-Getting-started-with-reports)

There are two types of reports you can export for virtual events from Zoom:

1. **Usage:** view meetings, participants, and meeting minutes within a specified time range
1. **Meeting:** view registration reports and poll reports for meetings

For registration lists:

1. Navigate to `Reports` > `Meeting`
1. Select your report type. For a registration report, select `Registration Report`. If you had polling enabled for your virtual event or meeting, you can select `Poll Report` to download the results of your poll.
1. Select the date of your event. Maximum report duration is one month.
1. Select the checkbox next to your event and click `Generate`.
1. A pop-up box will appear for you to choose the type of registrants (all, approved, or denied).  Choose `all` and click `Continue`.
1. After processing, you are redirected to the `Report Queue` where you can download your results as a .csv file.

For attendee lists:

1. Navigate to `Reports` > `Usage`
1. Select the date of your event and click `Search`. Maximum report duration is one month. The report displays information for meetings that ended at least 30 minutes ago.
1. You can toggle the visible columns you want shown in the results from the `Toggle columns` drop down on the right-side of the screen (optional).
1. Once your event is shown in results, scroll right to the `Participants` column to view the number of participants hyperlinked in blue. Click the linked number of participants.
1. A pop-up box will appear called `Meeting Participants`. From here, you can select the checkbox whether you want to export this list with meeting data or not. Meeting data includes the meeting ID, duration (minutes), # of participants, topic, start time, end time, and user email. 
1. Click `Export`. It exports the list as a .csv file.
