---
layout: markdown_page
title: "Peer Review Management"
---
## Peer Review Management

GitLab actively manages its Peer Review presence as part of its Customer Reference Program to maintain its values of Transparency, Collaboration, and Iteration.  By maintaining Peer Reviews, GitLab has a clear grasp of customer expectations.  Consistent maintenance of Peer Review presence allows an updated understanding of the Voice of the Customer and helps GitLab’s Customer Success, Development, Marketing, and Sales teams to take maximum advantage of these valuable assets offered by their most valuable asset: the customers.

| [Want access to a Peer Review Asset?](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/peer-reviews/#current-available-quote-assets) | [Need to Request Peer Review Help?](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/peer-reviews/#current-available-quote-assets) |
| ------ | ------ |
| ex. I'd like to find a ready-made quote to drop into a Sales deck | ex.  I need help researching and identifying new quotes for specific context |


### Maintenance Methodology

GitLab's approach to Peer Review maintenance begins with identifying priority Peer Review sites and establishing vendor relationships with these companies.  We work within the policies and Rules of Engagement for each Vendor, and take the GitLab profile for each site and category under active management - allowing us to customize the presentation of each profile with our own content.  

To curate each review appropriately, we create an issue within GitLab in the Peer Review Curation Project, which was created to act as a Content Management System for this purpose.  Each Review Issue is tagged with custom labels that function for retrieval to identify lifespan, itemize, expire, track, measure metrics, use cases, and GitLab product features.  This makes each review easily searchable via issue boards based on ad-hoc requests and recurring needs, and for Peer Review maintenance.

### GitLab User Peer Review Request Options

>* How do I interact with Peer Reviews to request assets?
>>* Option 1) Place Assets exist: 
>>>* Pitch deck
>>>* Use Case Pages
>>>* Event Signage
>>* Option 2) Request Form to pull assets
>>* Option 3) Issue Request for Specific Quote Research

>* Ex. A) I have a Sales deal with a prospect that needs validation, how do I interact with Peer Reviews?
>>* In this example, Sales would likely choose from Option 1 or 2, as available assets will support the needs in many cases.  However, in a unique need, feel free to submit a request via an Issue in Product Marketing (See below for specific directions on accessing assets or submitting requests)

>* Ex. B) Competitive Intelligence Department has specific request for Peer Review quote research?
>>* In this example, the department needs specific research, and would want to submit a request via an issue. (See below for specific instructions on requesting Quote Asset Research)

### Current Available Quote Assets

Some GitLab assets expire; please follow the process for *Asset Check-Out* for tracking purposes.

1) Complete and submit the [Peer Review Submission Request Form](https://forms.gle/Mwt6KrpYZ1kuVifh9) indicating which Peer Review(s) you will be downloading

2) Access the [Peer Review Quote Assets Shared Document](https://docs.google.com/presentation/d/13tHJ1BgABMTwt0gZeP_hZK0B96Ui2FUrMWFVmFtT9Nw/edit?usp=sharing) to download your Quote Asset(s)

>* please do not copy/save the entire deck, only the relevant slide(s) required for your deck.  This will facilitate later retrieving/expiring the assets.
>* if additional slides are saved that were not on the original submission form, please retroactively submit an additional form with this information.
>* the quotes most be used exactly as provided, with the URL intact
>* if you have a specific request, see the next section
>* for questions, please @jparker in slack

### Requesting Peer Review Help in an Issue

When available assets do not meet requirements, please submit a specific research request using the following process.

1) Submit an issue using the template "peer-review-request" in the [Product Marketing Project](https://gitlab.com/gitlab-com/marketing/product-marketing)
>* Make sure you use leave the labels contained in the template "Customer Reference Program," "Peer Review Request," which will direct it to the appropriate [Customer Reference Program Issue Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?label_name[]=Customer%20Reference%20Program) 
>* assign it to @jlparker 
>* please give at least a 5-7 days notice, to give proper lead time
>* if your request is urgent or you have questions, please @jparker in slack  


### Roadmap for Peer Reviews

*Goal: to set the standard in using Peer Reviews as a full customer listening tool*

|  **Stage 1: Current**  | **Stage 2: Evolve**  | **Stage 3: Firehose** | 
|---|---|---|
| Bring in new reviews to CMS under Product Marketing project, listed Epics under Marketing; Public Response for each curated review *(Values: Collaboration, Transparency)*; Get Feedback from broader PMM team re:labels and systematic CMS approach, Ruby scripting; PathFactory insights; Develop Quote Assets by Request | Move to New Project; Rework labels for Brevity and Color coding; Add aditional Epics for additional Peer Review Sites; Develop Relationships with additional Peer Review Sites; Create Vision Page, Ruby Scripting, and updated Handbook page based on Feedback | Use key contacts in other departments to funnel functionally derived information from Peer reviews (Customer Success, Product Development); Send information that achieves critical mass through appropraite channels (meetings, issues, presentations) to improve GitLab's service *(Values: Iteration, Collaboration)* |  
