---
layout: handbook-page-toc
title: "Learn@GitLab"
---

Welcome to Learn@GitLab, here to provide you a deeper look into GitLab and how it works, organized by the main problems which people come to GitLab looking to solve for (what we call our [customer use cases](https://about.gitlab.com/handbook/use-cases/)).


## What would you like to learn about?

<!-- ------------------- Version Control & Collaboration ------------------- -->
<details><summary>Version Control & Collaboration</summary><p>

<h3> Control Changes to Product Development Assets (video) </h3>
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/l6K3Xn2MPJw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Manage, Track and Maintain Access (video) </h3>
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/nRxCz4vMv5Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Foster Collaboration (video) </h3>
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/OFNUjvgm2_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Merge Request Approval by API (simulation demo) </h3>
<!-- July 2019, 12.0 -->
<a href="/handbook/marketing/product-marketing/demo/ct-files/mr-approval.html"><img src="/handbook/marketing/product-marketing/demo/ct-files/mr_approval.png"></a>
 <!-- [<img src="./ct-files/mr_approval.png">](./ct-files/mr-approval.html)
 <!--[Downloadable macOS](https://drive.google.com/file/d/1Uvl4DorNh_eM7Xn_BJhMDv0yFwuHbdtt/view?usp=sharing)<br/>
 [Downloadable Windows](https://drive.google.com/file/d/1EAEuVsRAlLoCTwi5fSOvR959eKFCAsoY/view?usp=sharing)-->

</p></details>

<!-- ------------------- Continuous Integration ------------------- -->
<details><summary>Continuous Integration</summary><p>

<h3> Getting started with GitLab (video) </h3>
<!-- April 2020, 12.9 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/0GFEV0r_AC0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Ease of CI configuration (video) </h3>
<!-- April 2020, 12.9 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/DGWqJotKQyM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Build and test automation (video) </h3>
<!-- April 2020, 12.9 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/6207TKNGgJs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Visibility and Collaboration (video) </h3>
<!-- April 2020, 12.9 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/z8r3rFQT8xg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Cross-project Pipeline Triggering and Visualization (simulation demo) </h3>
<!-- May 2019, 11.10 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRZaaHmlJcBQS56SSS1QKyvLJQayJ31ZXqlAJMzQOMckbHt0dSj0KBN2bzg6lwny-lqfvhfhOl7tK8H/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1AVYo0HqTrwG59G2a55YzQhbXX2EKIsxQ-9P7-g_2bQc/edit?usp=sharing">G-slides file</a>

<h3> GitLab CI with GitHub Repositories (video) </h3>
<!-- June 2018, 11.0 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/qgl3F2j-1cI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

</p></details>

<!-- ------------------- Continuous Delivery ------------------- -->
<details><summary>Continuous Delivery</summary><p>

Sorry, nothing here yet.

</p></details>

<!-- ------------------- DevSecOps ------------------- -->
<details><summary>DevSecOps</summary><p>

<h3> DevSecOps Use Case Overview (video) </h3>
<!-- Feb 2020, 12.7 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/UgCHtr-6uG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Adding Security to the CICD pipeline (video) </h3>
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Fd5DhebtScg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Managing Security Vulnerabilities with the Security Dashboard (video) </h3>
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/t-3TSlChHy4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Dependency Scanning (video) </h3>
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/39RvTMLDszc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Container Scanning (video) </h3>
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/wIcaSerMfFQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> License Compliance (video) </h3>
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/42f9LiP5J_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Secure Capabilities (simulation demo) </h3>
<!-- March 2019, 11.8 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTmtpY92Uun3YsixCJHZu7yt69M9rFB5SuFRSglOBRXoFNTKBdgPZpE4JBTl3LtAAX1zS4zQdBdD6Ga/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>  
<a href="https://docs.google.com/presentation/d/1fdTmdepdaq03OSfcA3pYduDxDnQEyvY4ARPqXEX8KrY/edit#slide=id.g2823c3f9ca_0_9">G-slides file</a>

<h3> Secure Capabilities (short, guided) (simulation demo) </h3>
<!-- March 2019, 11.8 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQF2Neh1h0vFwMapLvhppr_bZZVaxbtnVvTP69xd6YNGreW5dZ43w4w5qQTmYNewmI-3pViilsvbIcX/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1cfzdLFWk3hYLw_aocgunVmJCD-TSiOgypr66A_nR8VQ/edit?usp=sharing">G-slides file</a>

<h3> DevSecOps Application Security Capabilities (short, guided) (click-through demo) </h3>
<!-- Nov 2019, 12.4 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSg77UguL0fBT75sB6aPe-DbUSiUvfcU_TIDo3MZS96dOGRkqelQBYgOz2X4rE1GHkTY8e0eVRNNrPI/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1wRL9tGPIJP8qwpLAWS05KqjR3GoqSctEJdPa0uh-Nfg/edit?usp=sharing">G-slides file</a>

</p></details>

<!-- ------------------- Agile Management ------------------- -->
<details><summary>Agile Management</summary><p>

<h3> How to set up GitLab groups and projects to run multiple Agile teams with microservices (video) </h3>
<!-- Jan 2019, 11.7 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/VR2r1TJCDew" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

<h3> SAFe and Agile Planning with GitLab (video) </h3>
<!-- Jan 2019, 11.7 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/PmFFlTH2DQk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

<h3> GitLab VSM - Issue Boards for Mapping (video) </h3>
<!-- June 2018, 11.0 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/9ASHiQ2juYY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

<h3> GitLab VSM - Business Value Monitoring (video) </h3>
<!-- June 2018, 11.0 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/oG0VESUOFAI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

<h3> Agile Project Management (full, guided) (simulation demo) </h3>
<!-- June 2019, 12.0 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR5EWjzgr-Qe-cYdMj4WJnnVDxKyoyirMv0OfOJWbgCzevJoLGTXnbKDYm2TUhpdPAkh-nTcucIgZKx/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/13Zj83pjpwyq3s4T2fPSTuKO8NwqdCdn827GB7S-3hW8/edit?usp=sharing">G-slides file</a>

<h3> Agile Project Management (short, guided) (simulation demo) </h3>
<!-- Nov 2019, 11.3 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSmsSstAwEsbh4t_vVtSIRI2-hX67H-YvQJqToE5cMH14WyENhH8xXxezU0Va9Jg79BD-SHe-kE73G-/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1gSVO07I049o0ZHDzllLX6ADgCUiuyj-icIZi_o60v80/edit?usp=sharing">G-slides file</a>

<h3> GitLab integration with Jira (video) </h3>
<!-- May 2018, 10.8 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/Jn-_fyra7xQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

</p></details>

<!-- ------------------- Simplify DevOps ------------------- -->
<details><summary>Simplify DevOps</summary><p>

<h3> GitLab Overview - Planning to Monitoring in 12 mins (video) </h3>
<!-- March 2020, 11.3 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/7q9Y1Cv-ib0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

<h3> Benefits of a Single App (video) </h3>
<!-- Aug 2019, 12.1 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/MNxkyLrA5Aw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> GitLab Planning to Monitoring (simulation demo) </h3>
<!-- Aug 2019, 12.0 -->
<a href="/handbook/marketing/product-marketing/demo/ct-files/AutoDevOps-v13.html"><img src="/handbook/marketing/product-marketing/demo/ct-files/ptm.png"></a>
<!--> [<img src="./ct-files/ptm.png">](./ct-files/AutoDevOps-v13.html)
<!--T [Downloadable macOS](https://drive.google.com/file/d/1H9-l9K9cPhLl7kjSG8np7fvkkCnCRLyH/view?usp=sharing)<br/>
 [Downloadable Windows](https://drive.google.com/file/d/1Sg7jMODZYUvdrkuaS2FI88feWoYv7CxO/view?usp=sharing)-->

<h3> GitLab Overview - Planning to Monitoring in 18 mins (video) </h3>
<!-- Oct 2018, 11.3 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/nMAgP4WIcno" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

<h3> Auto DevOps (full, guided) (simulation demo) </h3>
<!-- Oct 2018, 11.3 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTNeBj0TJWca1PVwccxUTXWDWYqaIyB6Q1fRYCfjtMzeK8DtpmAcG1o6ipFBi-lhYKVTAA9kYBWyKKu/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1oKHU3MsbJmxVQyO-7c6JLMoCOS80uS-0NlcI-mRxAAY/edit?usp=sharing">G-slides file</a>

<h3> Auto DevOps (short, guided) (click-through demo) </h3>
<!-- Oct 2018, 11.3 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRQ4xltHhYRO_zgbo7exF6BwR09jvPmyFzR4XvjdlpYMRqT4dctx61XCkLjfR-8sq6QyOsoEFBBJjJh/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1UkQI_9V-CJZcbZJBDTB7tyOg14XHCKIwNoUHW1K6tC8/edit?usp=sharing">G-slides file</a>


</p></details>

<!-- ------------------- Cloud Native ------------------- -->
<details><summary>Cloud Native</summary><p>

<h3> Create Kubernetes cluster (simulation demo) </h3>
<!-- Aug 2019, 12.0 -->
<a href="/handbook/marketing/product-marketing/demo/ct-files/gke-setup.html"><img src="/handbook/marketing/product-marketing/demo/ct-files/gke_setup.png"></a>
<!--[<img src="./ct-files/gke_setup.png">](./ct-files/gke-setup.html)
<!--[Downloadable macOS](https://drive.google.com/file/d/1tGpBmyJkMn_ljmFBCAtGkU9DmrPjuGDI/view?usp=sharing)<br/>
[Downloadable Windows](https://drive.google.com/file/d/1fTkVWsH1okcm_r3MtfRhS-XeeVSExotU/view?usp=sharing)-->

<h3> App Modernization with Kubernetes and Serverless (short, guided) (click-through demo) </h3>
<!-- Feb 2020, 12.7 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR9QjFBFQYAsVPOyW_afnLsRRsyysRcWqD1OKMp1GBS1JBxAomsIMsEWJuS1rff5rLCX0R4hE3XUfBx/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1Cj6qEpUUI7rOIXS150Pp34SBiZfCwWy8vFRccijnSRA/edit?usp=sharing">G-slides file</a>

<h3> Continuous Verification with GitLab and VMware (short, guided) (click-through demo) </h3>
<!-- Nov 2019, 12.4 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRq44Wfc19tZfTkVEfJDgSDlCNEBA0HAhYfGftgsJmYj8nHhr1KEpKiv4oE0L1Y3rM2nXnU8_tIox93/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1mYWajYWMLMBAcEcPiyh7FOMaL_0SJDo2tzeImxVFkko/edit?usp=sharing">G-slides file</a>

<h3> Kubernetes Cluster Management an Monitoring (short, guided) (click-through demo) </h3>
<!-- Feb 2020, 12.7 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSJSEZ8zKxeAPU0O4oYy52W48LPiTOBH1A5Hku8AtvhxbL8TrvoqJ3ehZd8i13FRDgsEKeoHkypQkts/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1zAAMsn5ZoMv6ibTLkKdZK5r9iEkFbLZzjgTtVAOHmW4/edit?usp=sharing">G-slides file</a>

<h3> Create and use an EKS cluster using GitLab (short, guided) (click-through demo) </h3>
<!-- Dec 2019, 12.5 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQlDO-z2RHvElzBYnfFonJsxwh0M1YeU1bPl7yXduWtuj55L-DRSf9P6-h6zk4v7yBgA1nVNLxdJPth/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1iXnB6lvTx2_-_0ASElLUDZwyFPWILCRx54XjJkMFuw0/edit?usp=sharing">G-slides file</a>

</p></details>


<!-- ------------------- GitOps ------------------- -->
<details><summary>GitOps</summary><p>

<h3> GitOps with GitLab (short, guided) (click-through demo) </h3>
<!-- Dec 2019, 12.5 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQTBQAHx7zyNd4o3YIyKmFEsRJl8-BCdd2g6MdCKuJuFab_HNea_HYK7HDSzd3macx6LnVtYwIlCxV7/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1UT32lLvXtwAslkK7o8asbko3a231WKrjmlcM0z9coPw/edit?usp=sharing">G-slides file</a>

<h3> Process with GitOps (video) </h3>
<!-- Oct 2019, 12.3 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/wk7YAXijIZI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Infrastructure with GitOps (video) </h3>
<!-- Nov 2019, 12.4 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/5rqoLj8N5PA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

<h3> Applications with GitOps (video) </h3>
<!-- Nov 2019, 12.4 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/heQ1WY_08Tc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

</p></details>


<!-- ------------------- Remote Development ------------------- -->
<details><summary>Remote Development</summary><p>

<h3> GitLab for Remote Teams (video) </h3>
<!-- April 2020, 12.10 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/qCDAioq3eis" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

</p></details>


<!-- ------------------- OvOther Use Cases and Overviews ------------------- -->
<details><summary>Other Use Cases and Overviews</summary><p>

<h3> GitLab in 3 minutes </h3>
<!-- Aug 2019, 12.1 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Jve98tlZ394" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>


<!-- ------------------- Archive ------------------- -->
<details><summary## Archive</summary><p>

<h3> Auto DevOps - Setup (GKE) </h3>
<!-- Oct 2018, 11.3 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQIPrinsvTG1s6ppnUWqSY-fpHnxe6oAwM7g91uBl8Mx3EYQhaejlKUF9_c3GtagIhzwg-8dJlIrNgw/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1AGABPlNzMm5-rrYfwGIzueXIbPleVkGpnc2Qk6JtnWk/edit?usp=sharing">G-slides file</a>

<h3> Auto DevOps - Setup (EKS) </h3>
<!-- Oct 2018, 11.3 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRjhO2VvKf_gWbObwi5APm18gxQrzQk5vvAARD-4vLQeT0NbkrSuP3t4sTVylRYZOD6kINLr37HmtHA/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1Ejnho9pqXPj-OHNU2q51cC0xCG5c8pVLmvg-maIA7BQ/edit?usp=sharing">G-slides file</a>

<h3> Auto DevOps in GitLab 11.0 </h3>
<!-- June 2018, 11.0 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/0Tc0YYBxqi4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

<h3> 2018 Vision Prototype </h3>
<!-- Feb 2018, 10.5 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/RmSTLGnEmpQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

<h3> GitLab for the Enterprise </h3>
<!-- Feb 2018, 10.5 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/gcWfUw_Cau4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

<h3> GitLab CI/CD Deep Dive Live Demo </h3>
<!-- July 2017, 9.4 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/pBe4t1CD8Fc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

</p></details>

