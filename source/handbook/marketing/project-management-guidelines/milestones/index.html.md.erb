---
layout: handbook-page-toc
title: "Milestones"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<%= partial("handbook/marketing/project-management-guidelines/includes/_wip-notice.html.md.erb") %>

### Milestones - background

[Milestones](https://docs.gitlab.com/ee/user/project/milestones/) are a great way to track the progress of multiple related issues across a specific time period.  With milestones, you can see how fast issues are being completed in that time period ([burndown chart](https://docs.gitlab.com/ee/user/project/milestones/burndown_charts.html)), and you can view the issues grouped by labels, and grouped by status (unassigned, assigned, and completed)

Milestones are **very useful** when tracking the progress of multiple issues and when planning and managing epics.

![Milestone4](/handbook/marketing/project-management-guidelines/images/milestone.png)

Here are two examples of milestones:

1. [Strategic Marketing Quarterly Milestone](https://gitlab.com/gitlab-com/marketing/product-marketing/-/milestones/6) - showing the bulk of strategic marketing work for a given quarter.
1. [UseCase Monthly Sprint Milestone](https://gitlab.com/gitlab-com/marketing/product-marketing/-/milestones/12)

#### Key things to know - using milestones

*   Milestones can be created at **both** `Project` and `Group` levels.
*   Milestones can be used to establish dates for epics (where the epic inherits the dates from the issues and associated milestones)
*   Milestones set a date range (start date and end date)
*   Issues are added to milestones
*   Milestone lists can be sorted **only** by **name** and **date** (either `due date` or `start date`)

#### UI / Usage Limitations

*   Milestone lists **cannot be filtered** and have limited sorting
*   Milestones **do not have labels** and cannot be filtered by labels
*   Milestones **do not track history of changes** or **who created/changed** the milestone
*   On issues - when adding an issue to a milestone, the list of milestones is **ALL** of the milestones in that project and the parent groups (potentially LOTS AND LOTS of milestones)
*   On groups - the list of milestones is **ALL** of the milestones defined in that group and below that group (potentially LOTS AND LOTS of milestones) - [This feature request addresses this limitation.](https://gitlab.com/gitlab-org/gitlab/-/issues/214652)
*   On Projects - the list of milestones is **ONLY** the milestones defined in that project (deceptively very few milestones.) see [feature request](https://gitlab.com/gitlab-org/gitlab/-/issues/214901)

### Guidelines:

#### Guideline: Milestone Naming Convention - prefix in name

Because of the filtering limitations and the massive volume of potential milestones, a consistent Naming Convention is very, very, very helpful in finding the right milestones.

*   All Milestone names should start with either the group or project abbreviation. The recommendation is to prepend the name with a two-letter abbreviation followed by underscore: `gg_`.

For example:
    *   A **project** milestone in the “**Field Marketing**” project should be named:
        *   `**fm_**Milestone Name`
    *   A **group** milestone in the “**Strategic Marketing**” group should be named:
        *   `**sm_**Milestone Name`

This will help in two ways.  First, when looking at lists of milestones, they can be sorted and put the related milestones together.   Second, in issues, when adding a milestone, you can search by name, which will make it easy to find the right subset of milestones (either for the project or group)

#### Guideline: Group or Project Milestone? - Define milestone at Lowest level

Define milestones at the LOWEST level of the organization as possible.

- If the milestone ONLY applies to a specific project, then create the milestone there
- If the milestone applies to a group of projects, then create the milestone at the lowest possible group.

This will mean that the lists of milestones available to a given Issue (when adding an issue to a milestone) will be limited to a smaller list of relevant milestones. (though the list might still be very long)

#### Guideline: Milestone Dates & Duration - It depends, though shorter is often better

Because the milestone has dates for start and finish, the implication is that issues and MRs in the milestone are completed in this time window.  If the milestone window is 3 weeks, then the expectation is that the work in the milestone is completed in that time frame.

Every team and every project is unique and there is no ONE answer for milestone duration.  Some teams have had milestones as long as a quarter, others 4 weeks and others 1 week.  Choose the duration that works for your team, learn, and evolve.

#### Guideline: Milestone Description - Include milestone history and who owns

Because the milestone does not yet include change history or details about who created it or why, we should use the description field to fill in the blanks.

- In the description field describe the purpose of the milestone and who is milestone owner/manager
