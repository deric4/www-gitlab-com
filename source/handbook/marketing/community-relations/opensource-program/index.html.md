---
layout: handbook-page-toc
title: "Open Source Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab for Open Source Program Overview
*At GitLab, our mission is to change all creative work from read-only to read-write so that everyone can contribute.* 

The [GitLab for Open Source](/solutions/open-source/) program supports GitLab's mission by empowering and enabling open source projects with our most advanced features so that they can create greater impact and amplify the contribution mindset within their spheres of influence.

This program's vision is to make GitLab the best product for Open Source projects to thrive at scale. 

Apart from actively maintaining and constantly adding value to the open source [Community Edition](/install/ce-or-ee/), GitLab makes the Enterprise Edition's top [Ultimate](/pricing/#self-managed) and [Gold](/pricing/#gitlab-com) tiers available free-of-cost to qualifying open source projects. 

### What's included?
- GitLab's top tiers ([self-hosted Ultimate and cloud-hosted Gold](/pricing/)) are [free for open source projects](/solutions/open-source/)
- 50,000 CI minutes are included for free for GitLab Gold users. Additional CI minutes can be purchased ($8 one-time fee for 1,000 extra CI minutes)
- Support can be purchased at a discount of 95% off, at $4.95 per user per month

Note: If you want to host your personal project on GitLab.com and you decide to make it publicly visible instead of private, you will automatically have access to GitLab's Gold features when you create a Free account. To access Gold features at the *group* level (to enable things like epics, roadmaps, merge requests, and other Gold features for groups), you'll need to upgrade to our paid Gold plan or you may consider applying for this GitLab for Open Source program.

### Who qualifies for the GitLab for Open Source program?

- Any project that uses an [OSI-approved open source license](https://opensource.org/licenses/category) and which does not seek to make a profit from the resulting project software may apply.
- See our [full terms](/terms/#edu-oss) regarding this program
- For more questions, please see our [GitLab for Open Source FAQ](/solutions/open-source/#FAQ)
- 
### How to get started
 * Qualifying open source projects can start benefiting from self-hosted Ultimate and cloud-hosted Gold by completing a simple [application process](https://about.gitlab.com/solutions/open-source/program/).
 * Read more about the [Community Edition vs Enterprise Edition](https://about.gitlab.com/install/ce-or-ee/) to see what the best option is for your project. This page includes a way to download the Community Edition from there. 

## Deep Dive into the GitLab for Open Source program
Here is more information on how the GitLab for Open Source program operates. 

### OKRs (goals for current quarter)
GitLab creates [Objectives and Key Results (OKRs)](https://en.wikipedia.org/wiki/OKR) per quarter. Here are the current OKRs for the GitLab for Open Source program:
 * [Revamp OSS Program to support growth](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/10)
 * [Build up OSS program relationships](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/11)

### Where to find what we're working on
Epics, issue boards, and labels are used to track our work. For more information on the specific ones we use, please visit the [Community Relations project management page](/handbook/marketing/community-relations/project-management/). 

### Other resources

- Community Advocacy [workflow](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/)
- GitLab for Open Source [repository](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/gitlab-oss/)

### Metrics

SFDC Opportunities by Stage for OSS campaign - SFDC [report](https://na34.salesforce.com/00O61000004hfom)
