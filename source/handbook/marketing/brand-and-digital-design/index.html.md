---
layout: handbook-page-toc
title: "Brand and Digital Design Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand & Digital Department
{:.no_toc}

---

[Slack](https://gitlab.slack.com/app_redirect?channel=marketing-brand-and-digital){:.btn .btn-purple-inv} 
<a href="mailto:brand-and-digital@gitlab.com" class="btn btn-purple">Email</a>

# Mission

<details markdown="1">

<summary>show/hide this section</summary>

We build deliverables and experiences in both digital and physical realms such as the about.gitlab.com website, brand collateral, and more.

We assist with conversion design, growth marketing, user experience (UX), search-engine optimization (SEO), and performance metrics to build the logged out experience. We're involved with related marketing campaigns, events, lead-generation, and more. We guide the brand design and experience to assist with creating a cohesive identity.

## Lean design
{:.no_toc}

### Metrics & measures
{:.no_toc}

[ TODO : Document ]

## Value-focused
{:.no_toc}

### Objectives
{:.no_toc}

[ TODO : Document ]

### Design is good business
{:.no_toc}

[ TODO : Document ]

## Co-design
{:.no_toc}

### Inclusive
{:.no_toc}

[ TODO : Document ]

### Value-focused
{:.no_toc}

[ TODO : Document ]

### Metrics
{:.no_toc}

[ TODO : Document ]

#### Periscope dashboards
{:.no_toc}

[ TODO : Document ]

## MVC
{:.no_toc}

### Product performance indicators
{:.no_toc}

[ TODO : Document ]

## Iterative co-design
{:.no_toc}

### Design systems
{:.no_toc}

We've broken out the GitLab interface into a set of atomic pieces to form our design system, [Pajamas](https://design.gitlab.com/). Pajamas includes information such as our principles, components, usage guidelines, research methodologies, and more.

#### [GitLab Product UX Guide](https://docs.gitlab.com/ee/development/ux_guide/)
{:.no_toc}

The goal of this guide is to provide written standards, principles and in-depth information to design beautiful and effective GitLab features. This is a living document and will be updated and expanded as we iterate.

## Feedback & validation
{:.no_toc}

#### AB testing
{:.no_toc}

[ TODO : Document ]

</details>


















# How to request work

<details markdown="1">

<summary>show/hide this section</summary>

Please submit your request with sufficient lead time to allow us to plan accordingly. **The sooner the better.** We field new requests from hundreds of GitLabbers daily and there are fewer than 10 of us.

<details markdown="1">

<summary>About status labels</summary>

* `mktg-status::triage` this work is in the pre-planning stage. We're still discussing what to do.
* `mktg-status::plan` this work is in the planning stage. We know what we want to do but don't know how we want to do it yet.
* `mktg-status::design` this issue needs some prototyping or other UX designs before we know where we're going.
* `mktg-status::ready-to-build` this issue has been planned and detailed. We know what needs to be done. We can start building it.
* `mktg-status::wip` this issue is actively being worked on.
* `mktg-status::blocked` something is blocking progress on this issue.
* `mktg-status::review` enough work has been completed that this is ready for review and approval.
* `mktg-status::scheduled` this issue cannot be merged until a scheduled date but the work is complete and approved.

</details>

## Brand work

<details markdown="1">

<summary>show/hide this section</summary>

[General request](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=design-request-general){:.btn .btn-purple-inv}
[Content resource](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=design-request-content-resource){:.btn .btn-purple}
[Integrated campaign](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=design-requirements-integrated-campaign){:.btn .btn-purple}
[Team backlog refinement](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=brand-and-digital-design-grooming){:.btn .btn-purple}

**Brand labels**

The `Design` label helps us find and track issues relevant to the Design team. If you create an issue where Design is the primary focus, please use this label.

**Cross-link to design system**

[ TODO : Document ]

</details>

## Website work

<details markdown="1">

<summary>show/hide this section</summary>

Because the website exists as a project at the top level, labels and boards for the digital team should usually be created in the root gitlab.com group. For more information on this works, please see [How it all fits together](https://about.gitlab.com/handbook/marketing/#how-it-all-fits-together).

[Website work request](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-work-request){:.btn .btn-purple-inv}
[Website bug report](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-bug-report){:.btn .btn-purple}
[Team backlog refinement](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=brand-and-digital-design-grooming){:.btn .btn-purple}

**Website labels**

At a minimum, website related issues should have the label `mktg-website` applied in order to populate appropriate boards.

They should also have a label for your team and/or subject matter (ex: `blog`, `Digital Marketing`, `SEO`). These labels need to exist in either the root `GitLab.com` group or the `www-gitlab-com` repository.

Issues should follow the standard marketing status label] flow labels, listed in the [About status labels](#how-to-request-work) collapsible section above, with a few additions.

</details>

</details>














# Issue boards

<details markdown="1">

<summary>show/hide this section</summary>

[ TODO : Document how to request a new board ]

[ TODO : Document how we use the design-P1 through design-P4 labels. ]

| Team or subject | Brand | Digital | Prioritization lead |
| - | - | - | - |
|All of Marketing|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1511332)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483370)|Todd Barr|
|Account Based Marketing|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1690658)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1691347)|Emily Luehrs|
|All Remote|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571555)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485066)|Jessica Reeder|
|Analyst Relations|-|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485071)|-|
|Blog|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571570)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483337)|Erica Lindberg|
|Blocked|-|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485169)|-|
|Brand & Digital Team|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1485124)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485124)|Shane Bouchard|
|CMO|-|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1486533)|Todd Barr|
|Content Marketing|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571561)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483354)|Erica Lindberg|
|Corporate Marketing|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1541149)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485085)|Danielle Morrill|
|Design Handbook|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1498563)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1498563)|Shane Bouchard|
|Diversity and Inclusion|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1621520)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1632756)|Candace Byrdsong Williams|
|Events|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1541174)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485090)|Emily Kyle|
|Field Marketing|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1541162)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1533790)|Leslie Blanchard|
|Marketing Ops|-|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485111)|Dara Wade|
|Marketing Programs|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571580)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1580521)|Jackie Gragnola|
|OKR|-|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483333)|Shane Bouchard|
|Outsource|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1511334)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1502288)|Shane Bouchard|
|Strategic Marketing|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571417)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1489415)|Ashish Kuthiala|
|Social|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571585)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485179)|Natasha Woods|
|Talent Brand|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571573)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1580510)|Betsy Church|
|Technical Evangelism|[Brand](https://gitlab.com/groups/gitlab-com/-/boards/1596489)|[Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485182)|-|

</details>














# Our workflow

<details markdown="1">

<summary>show/hide this section</summary>

[ TODO : Add a flowchart ]

Sometimes it can be difficult to estimate timeframes. Because of this, we ask that you gather your project priorites as a team. We hope to have regular refinement sessions scheduled once we figure out a good schedule.

At each stage of project refinement, impact is assessed as well as the ability to achieve goals given specific timeframes, MVC, and resource constraints. For more info please read below [how we prioritize projects](#project-prioritization).

Each team we work with aligns on their internal top 5 projects/initiatives.

Once each team's needs have been gathered, then there are meetings for planning overall project priorities based on available bandwidth (ie DMP needs X but Events needs Y but Remote needs Z and we only have 2 people to work on it, 1 of which is already busy during that timeframe).

Based on the above, there may be 18 items in the overall Design queue and 23 in the overall Digital queue. Work in those two queues is scheduled and started as available. Different people are working from different queues to achieve different deliverables while aligned on initiatives and timeframes.

We recommend creating a separate issue for website deliverables and an issue for design deliverables. Break things down into small MVC chunks.

## Project prioritization

<details markdown="1">

<summary>Brand prioritization</summary>

Per the Design team's discretion, the prioritization of design projects will be based on the direct impact on Marketing.

To get a better sense of [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) prioritization, you can view the [Design Issue Board](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/913023?&label_name[]=Design).

Design projects within the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues) can be tracked using the [Website](https://gitlab.com/gitlab-com/www-gitlab-com/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Website) label. The prioritization of projects for [about.gitlab.com](/) can be viewed on the [Website Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/boards/349137?milestone_title=No+Milestone&).

Any design requests that do not fall in line with the goals and objectives of Marketing will be given a lower priority and factored in as time allows.

</details>

<details markdown="1">

<summary>Digital Prioritization</summary>

We use the following criteria to assess issue priority:

**Common**

* Frequency of use - How often is the single application diagram used? Web traffic to this page?
* Number of people impacted - How many GitLabbers would benefit from this asset? How many unique users would benefit from this page?

**Critical**

* High customer risk - If we don't do this, what are the risks to customers? How severe are those risks?
* High business risk - If we don't do this, how might it create risk for our business? Could it create a large volume of support calls? Make us non-GDPR compliant?
* Business criticality - Part of high ROI opportunity or other business critical initiative?
* Impact to important stakeholders - CEO or CMO request? Impacts bottom of funnel (BOFU) prospects very close to buying? Impacts key partners or customers?

**Differentiator**

* Brand and or product differentiator - Creates value by positioning our brand and or product against competition.

**Reusable**

* Can we reuse - If we build this, can we reuse it elsewhere to get more ROI. Perhaps it's low value score for this project, but high "lifetime" value via reuse.

**Time & Cost**

* Time and cost required to complete the work.

**Deadlines**

* Are there any hard deadlines due to contract or event obligations?

</details>

## Working in milestones

<details markdown="1">

<summary>show/hide this section</summary>

In alignment with other marketing teams, we may use milestones for: "1: We're working on this and expect it to be released during X milestone" and Due Dates for "2: We would like to have this done in timeframe Y". We have also occasionally set up our own related issue (with dependency if appropriate) for things that are team only on a larger issue where others are involved. That way we can pull into our own milestone and close when complete, even though the larger project is not yet complete.

Note that milestones don't need to specify version numbers and can be used similarly to a backlog. For example, the milestone "Next 3-4 releases".

</details>

</details>











# Brand guidelines

<details markdown="1">

<summary>show/hide this section</summary>

## Guidelines home

### Guidelines how we work
[ TODO : Document ]

#### Partnership with third parties

In certain cases, the help of a third party agency or design partner may be brought in for a project. The following serves as criteria for when to outsource design:

- Smaller-scale projects, such as stickers or requests that do not meet our current business prioritization, where the brand guidelines provide sufficient creative direction and parameters for the third party to work with. 
- Larger-scale projects where the Brand and Digital team need additional support given the timeline and/or scale of the request.

Whenever a third party is brought in to support design, the work must be shared with the Brand and Digital team to ensure brand integrity and that we are [working transparently](https://about.gitlab.com/handbook/values/#transparency) with one another.

For guidance on which third-party agency or designer(s) to work with, please reach out to [brand-and-digital@gitlab.com](mailto:brand-and-digital@gitlab.com).

#### Requesting design help

1. Create an [issue](https://about.gitlab.com/handbook/marketing/brand-and-digital-design/#issue-templates) in the corresponding project repository.
    1. For tasks pertaining to [about.gitlab.com](/) create an issue in the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues).
    1. For all other marketing related tasks create an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues).
1. Add all relevant details, goal(s), purpose, resources, and links in the issue description. Also `@` mention team members who will be involved.
1. Set due date (if possible) — please leave at least 2 week lead time in order to generate custom design assets. If you need them sooner, ping @luke in the #marketing-design Slack channel and we will make our best effort to accommodate, but can't promise delivery.
1. Add the `Design` and `Website Redesign` (if applicable) label(s) to your issue.

#### Team logo request guidelines

As the company continues to grow, incoming requests for internal team logos are increasing at a rate that is not scalable for the Brand Design team. We understand the desire for teams within GitLab to have their own identity, so we've created these guidelines to help direct your request:

* Teams can create their own logos that are for internal (non-public) use only.
* If you believe a public-facing team logo would be valuable to our business, please submit a [design request issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) outlining how it will be used, who will see it, and it's perceived value. Logos will be created and approved on a case-by-case basis to capitalize on brand opportunities and ensure brand integrity.

### Privacy

[ TODO : Document ]

### License

[ TODO : Document ]

### What's new

[ TODO : Document ]

## Design principles

### Generate value

[ TODO : Document ]

### Omni-channel experiences

[ TODO : Document ]

### Everyone can contribute

[ TODO : Document ]

### Grow a community

[ TODO : Document ]

## Foundations

### Personality

GitLab's brand has a personality that is reflected in everything we do. It doesn't matter if we are hosting a fancy dinner with fortune 500 CIOs, at a hackathon, or telling our story on about.gitlab.com…across all our communication methods, and all our audiences, GitLab has a personality that shows up in how we communicate.

Our personality is built around four main characteristics.

- Human: We write like we talk. We avoid buzzwords and jargon, and instead communicate simply, clearly, and sincerely. We treat people with kindness.
- Competent: We are highly accomplished, and we communicate with conviction. We are efficient at everything we do.
- Quirky: We embrace diversity of opinion. We embrace new ideas based on their merit, even if they defy commonly held norms.
- 
Humble: We care about helping those around us achieve great things more than we care about our personal accomplishments.

These four characteristics work together to form a personality that is authentic to GitLab team-members, community, and relatable to our audience. If we were quirky without being human we could come across as eccentric. If we were competent without being humble we could come across as arrogant.

GitLab has a [higher purpose](https://about.gitlab.com/company/strategy/#mission). We want to inspire a sense of adventure in those around us so that they join us in contributing to making that mission a reality.

### Writing style

The following guide outlines the set of standards used for all written company communications to ensure consistency in voice, style, and personality, across all of GitLab's public communications.

See the [Blog Editorial Style Guide](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/editorial-team/#blog-style-guide) for more.

The [tone of voice](https://about.gitlab.com/handbook/marketing/corporate-marketing/#tone-of-voice-1) we use when speaking as GitLab should always be informed by our [Content Strategy](https://gitlab.com/gitlab-com/marketing/blob/master/content/content-strategy.md#strategy). 

### Guidelines website

#### Using other logos

Logos used on the about.gitlab.com site should always be in full color and be used to the specifications provided by the owner of that logo, which can usually be found on the owners website. The trust marks component found throughout the site is the only exception and should use a neutral tone:

<img src="/images/handbook/marketing/corporate-marketing/design/trust-marks.png" class="full-width">

#### Text

Our website uses the [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro?selection.family=Source+Sans+Pro) font family. Headers (h1, h2, etc.) always have a weight of 600 (unless used in special situations like large, custom quotes) and the body text always has a weight of 400. Headers should not be given custom classes, they should be used as tags and tags alone (h1, h2, etc.) and their sizes or weights should not be changed, unless rare circumstances occur. Here are typography tags.

`H1: Header Level 1`

`H2: Header Level 2`

`H3: Header Level 3`

`H4: Header Level 4`

`p: Body text`

#### Buttons

Buttons are an important facet to any design system. Buttons define a call to action that lead people somewhere else, related to adjacent content. Here are buttons and their classes that should be used throughout the marketing website:

**Note**: Text within buttons should be concise, containing no more than 4 words, and should not contain bold text. This is to keep things simple, straightforward, and limits confusion as to where the button takes you.

**Primary buttons**

Primary buttons are solid and should be the default buttons used. Depending on the color scheme of the content, purple or orange solid buttons can be used depending on the background color of the content. These primary buttons should be used on white or lighter gray backgrounds or any background that has a high contrast with the button color. They should also be a `%a` tag so it can be linked elsewhere and for accessibility. Buttons should also be given the class `margin-top20` if the button lacks space between itself and the content above.

<div class="flex-container flex-column flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn orange margin-top20">Primary Button 1</a>
  <pre class="highlight shell">.btn.cta-btn.orange</pre>
  <p>OR</p>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn purple">Primary Button 2</a>
  <pre class="highlight shell">.btn.cta-btn.purple</pre>
</div>

**Secondary buttons**

There will be times when two buttons are needed. This will be in places such as [our jobs page](/jobs/), where we have a button to view opportunities and one to view our culture video. In this example, both buttons are solid, but one is considered the primary button (orange), and the other is the secondary button (white). The CSS class for the solid white button is <br> `.btn.cta-btn.btn-white`.

<img src="/images/handbook/marketing/corporate-marketing/design/jobs-buttons-example.png" class="full-width">

This is the proper use of two buttons, both being solid, but different colors based on hierarchy. If the background is white or a lighter color that doesn't contrast well with a white-backgound button, a ghost button should be used as a secondary button, and should match in color to the primary button beside it as shown below:

<div class="buttons-container flex-start">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn orange margin-top20">Primary Button</a>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-orange margin-top20">Secondary Button</a>
</div>

<div class="buttons-container flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn purple margin-top20">Primary Button</a>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-purple margin-top20">Secondary Button</a>
</div>

DO NOT: Do not use these ghost buttons styles as standalone buttons. They have been proven to be less effective than solid buttons [in a number of studies](https://conversionxl.com/blog/ghost-buttons/). They should only be used as a secondary button, next to a solid primary button that already exists. Here are the classes for the secondary buttons:

<div class="flex-container flex-column flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-orange margin-top20">Secondary Button 1</a>
  <pre class="highlight shell">.btn.cta-btn.ghost-orange</pre>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-purple">Secondary Button 2</a>
  <pre class="highlight shell">.btn.cta-btn.ghost-purple</pre>
</div>

### Iconography

Icons are a valuable visual component to the GitLab brand; contributing to the overall visual language and user experience of a webpage, advertisement, or slide deck. The GitLab iconography currently consists of "label icons" and "content icons", each are explained in further detail below:

**Label icons**

Label icons are intended to support usability and interaction. These are found in interactive elements of the website such as navigation and [toggles](/pricing/).

![Label icons example](/images/handbook/marketing/corporate-marketing/design/label-icons-example.png){: .medium.center}

**Content icons**

Content icons are intended to provide visual context and support to content on a webpage; these icons also have a direct correlation to our illustration style with the use of bold outlines and fill colors.

A few examples include our [event landing pages](/events/aws-reinvent/) and [Resources page](/resources/).

<img src="/images/handbook/marketing/corporate-marketing/design/content-icons-example.png" class="full-width">

### Logos

To download the GitLab logo (in various formats and file types) check out our [Press page](/press/).

The GitLab logo consists of two components, the icon (the tanuki) and the wordmark:

![GitLab logo](/images/handbook/marketing/corporate-marketing/design/gitlab-lockup.png){: .small.left}

GitLab is most commonly represented by the logo, and in some cases, the icon alone. GitLab is rarely represented by the wordmark alone as we'd like to build brand recognition of the icon alone (e.g. the Nike swoosh), and doing so by pairing it with the GitLab wordmark.

#### Logo safe space

Safe space acts as a buffer between the logo or icon and other visual components, including text. this space is the minimum distance needed and is equal to the x-height of the GitLab wordmark:

![Logo x-height](/images/handbook/marketing/corporate-marketing/design/x-height.png){: .medium.left}

![Logo safe space](/images/handbook/marketing/corporate-marketing/design/logo-safe-space.png){: .small.left}

![Icon safe space](/images/handbook/marketing/corporate-marketing/design/icon-safe-space.png){: .small.left}

The x-height also determines the proper spacing between icon and wordmark, as well as, the correct scale of the icon relative to the wordmark:

![Stacked logo safe space](/images/handbook/marketing/corporate-marketing/design/stacked-logo-safe-space.png){: .small.left}

#### Minimum logo size

Here are the recommended minimum sizes at which the logo may be reproduced. For legibility reasons, we ask that you stick to these dimensions:

**Logo**

![Horizontal logo minimum size](/images/handbook/marketing/corporate-marketing/design/logo-min-size.png){: .small.left}

- Digital: 100px wide
- Print: 1.25in (31.75mm) wide

**Stacked logo**

![Stacked logo minimum size](/images/handbook/marketing/corporate-marketing/design/logo-stacked-min-size.png){: .small.left}

- Digital: 60px wide
- Print: 0.75in (19mm) wide

**Icon**

![Icon minimum size](/images/handbook/marketing/corporate-marketing/design/icon-min-size.png){: .small.left}

- Digital: 30px wide
- Print: 0.50in (13mm) wide


#### The Tanuki

The [tanuki](https://en.wikipedia.org/wiki/Japanese_raccoon_dog) is a very smart animal that works together in a group to achieve a common goal. We feel this symbolism embodies GitLab's [mission](/company/strategy/#mission) that everyone can contribute, our [values](/handbook/values/), and our [open source stewardship](/company/stewardship/).

The tanuki logo should also not have facial features (eyes, ears, nose...); it is meant to be kept neutral, but it can be accessorized.

#### Brand oversight

Occasionally the [old GitLab logo](* https://gitlab.com/gitlab-com/gitlab-artwork/blob/master/_archive/logo/fox.png) is still in use on partner websites, diagrams or images, and within our own documentation. If you come across our old logo in use, please bring it to our attention by creating an issue in the [Marketing](https://gitlab.com/gitlab-com/marketing/general/issues) issue tracker. Please include a link and screenshot (if possible) in the description of the issue and we will follow-up to get it updated. Thanks for contributing to our brand integrity!

### Trademark

GitLab is a registered trademark of GitLab, Inc. You are welcome to use the GitLab trademark and logo, subject to the terms of the [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). The most current version of the GitLab logo can be found on our [Press page](/press/).

Under the Creative Commons license, you may use the GitLab trademark and logo so long as you give attribution to GitLab and provide a link to the license. If you make any changes to the logo, you must state so, along with the attribution, and distribute under the same license.

Your use of the GitLab trademark and logo:

- May not be for commercial purposes;
- May not suggest or imply that you or your use of the GitLab trademark or logo is endorsed by GitLab, or create confusion as to whether or not you or your use of the GitLab trademark or logo is endorsed by GitLab; and
- May not suggest or imply or that you are affiliated with GitLab in any way, or create confusion as to whether or not you are affiliated with GitLab in any way.

Examples of improper use of the GitLab trademark and logo:

- The GitLab name may not be used in any root URL, including subdomains such as `gitlab.company.com` or `gitlab.citool.io`.
- The GitLab trademark and/or logo may not be used as the primary or prominent feature on any non-GitLab materials.

### Typography  

The GitLab brand uses the [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro?selection.family=Source+Sans+Pro) font family.

### Color

While the brand is ever-evolving, the GitLab brand currently consists of six primary colors that are used in a wide array of marketing materials. RGB and CMYK swatch libraries can be found [here](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/tree/master/design/gitlab-brand-files/color-palettes). 

##### Hex/RGB

![GitLab Hex/RGB Colors](/images/handbook/marketing/corporate-marketing/design/gitlab-hex-rgb-colors.png)


### Illustration  

### Iconography

Icons are a valuable visual component to the GitLab brand; contributing to the overall visual language and user experience of a webpage, advertisement, or slide deck. 

**Badges**
[TODO: Document]

**Patterns**
[TODO: Document]

#### Illustration library
[ TODO : Document ]


#### Icon library

- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/line-icons) (.png)
- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/_resources/icons/svg) (.svg)
- [Icon illustrations](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/illustrated-icons) (.png)
- [Software Development Lifecycle](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/software-development-lifecycle/complete-lifecycle-icons/png)

#### Icon pattern

- [GitLab icon pattern](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/illustrations/icon-pattern)

#### Social media

- [Profile assets](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/social-media/profile-assets/png/assets)

### Photography

#### Photo library
[ TODO : Document ]

### Brand resources

- [GitLab icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-logo) (web RGB & print CMYK)
- [GitLab logos](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-wordmark) (web RGB & print CMYK)
- Print-ready [event one-pagers](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/print/event-one-pagers)
- Color palette and Typography can be found in the [Brand Guidelines](#brand-guidelines)
- [Authorized Reseller GitLab Virtuoso Badge](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/authorized-resellers/gitlab-virtuoso-badge)

### Templates

#### Issue templates

[ TODO : Merge with the "how to request work" section ]

To work more efficiently as an [asynchronous](/company/culture/all-remote/asynchronous/) team, everything should [begin](/handbook/communication/#everything-starts-with-a-merge-request) in an issue or merge request as opposed to Slack or email.

Creating an issue may seem like an added burden, but the long-term benefit of having [context](/company/culture/all-remote/effective-communication/#understanding-low-context-communication) around a given piece of work prevents [knowledge gaps](https://about.gitlab.com/company/culture/all-remote/asynchronous/#plugging-the-knowledge-leak) from occurring. Issue templates exist to make the process of creating issues easier. If you find yourself starting similar issues over and over, look through existing issue templates. If a suitable one does not exist, consider creating one in the appropriate repository.

For more on how to make beautiful templates, check out GitLab's [Markdown Guide](https://about.gitlab.com/handbook/markdown-guide/). To add an emoji in an issue, begin by typing `:` and the title of the emoji. A list of emoji markup is [here](https://gist.github.com/rxaviers/7360908).

Remember to always add `Related Issues` and `Epics` after you've created your issue so others have context on what issues connect to this work.

- Visit the [Corporate Marketing Issue Template Repository](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/tree/master/.gitlab/issue_templates) to view all available issue templates. You'll find templates covering [Corporate Events](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/Corporate-Event-Request.md), [Bulk Swag Requests](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/BulkSwagRequest.md), Social Requests for [Events](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-event-request.md) and [General](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-general-request.md), Video Requests, [Webpage Updates](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/webpage-update.md), and more.
- If you're for a [general or universal issue template](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/general-project-template.md) to begin tracking discussion and progress on any given piece of work, big or small, search for `general-project-template` in a newly-created [Corporate Marketing Issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues). This template is pre-populated and beautified with emojis and descriptors for common sub-sections such as `Background`, `Details and reach`, `Goals and key messages`, and `Due dates, DRI(s) and next steps/to-dos`.


#### Presentation kits

- [General GitLab deck template](https://docs.google.com/a/gitlab.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing)
- [Group Conversation template](https://docs.google.com/a/gitlab.com/presentation/d/1JYHRhLaO9fMy1Sfr1WDnCPGv6DrlohlpOzs48VvmlQw/edit?usp=sharing)
- [GitLab pitch deck template](https://docs.google.com/a/gitlab.com/presentation/d/1LC1lT-gxpl1oUZ2InX4Oni9T4MfR0DFF0RLi4uNxBBQ/edit?usp=sharing)

#### Event kits
[ TODO : Document ]

#### Swag kit
[ TODO : Document ]

#### Print templates
[ TODO : Document ]

</details>

















# Teams

<details markdown="1">

<summary>show/hide this section</summary>

## Brand team

<details markdown="1">

<summary>show/hide this section</summary>

### Overview
{:.no_toc}

The Design team has a rather wide reach and plays a big part in almost all marketing efforts. Design touchpoints range from the [GitLab website](/) to print collateral, swag, and business cards. This includes, but certainly not limited to:

#### Field design & branding
{:.no_toc}
- Marketing Design System
- Conference booth design
- Banners & signage
- Swag
- Event-specific slide decks
- Event-specific branding (e.g. GitLab World Tour, Team Summits, etc.)
- Business cards
- One-pagers, handouts, and other print collateral
- GitLab [Brand Guidelines](#brand-guidelines)

#### Content design
{:.no_toc}
- Promotional videos & animations
- Social media campaign assets
- Webcast collateral & assets
- eBooks
- Whitepapers
- Infographics & diagrams

*In the spirit of 'everyone can contribute' (as well as version control and SEO) we prefer webpages over PDFs. We will implement a `print.css` component to these webpages so that print PDFs can still be utilized for events and in-person meetings without the headache of version control*

### Brand team members
{:.no_toc}

[**Shane Bouchard**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#sbouchard1)

* Title: Director of Brand and Digital Design
* Email: sbouchard@gitlab.com
* GitLab handle: sbouchard1
* Slack handle: sbouchard

[**Luke Babb**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#luke)

* Title: Manager, Creative
* Email: luke@gitlab.com
* GitLab handle: @luke
* Slack handle: @luke

[**Monica Galletto**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#monica_galletto) 

* Title: Associate Production Designer
* Email: mgalletto@gitlab.com
* GitLab handle: @monica_galletto
* Slack handle: @monicagalletto

[**Vic Bell**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#vicbell)	

* Title: Senior Illustrator
* Email: vbell@gitlab.com
* GitLab handle: @vicbell
* Slack handle: @vic

</details>

## Digital team

<details markdown="1">

<summary>show/hide this section</summary>

### Overview
{:.no_toc}

The Digital team is responsible for the development and maintence of about.gitlab.com and technical support for marketing campaigns. This includes, but certainly not limited to:

{:.no_toc}
- Redesign, updates, and maintenance of the [GitLab website](/)
- Landing pages (campaigns, webcasts, events, and feature marketing)
- Email template design

### Digital team members
{:.no_toc}

[**Shane Bouchard**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#sbouchard1)

* Title: Director of Brand and Digital Design
* Email: sbouchard@gitlab.com
* GitLab handle: sbouchard1
* Slack handle: sbouchard

[**Brandon Lyon**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#brandon_lyon)

* Title: Website Developer and Designer
* Email: skarpeles
* GitLab handle: @brandon_lyon
* Slack handle: @Brandon Lyon 

[**Lauren Barker**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#laurenbarker) 

* Title: Fullstack Engineer
* Email: lbarker@gitlab.com
* GitLab handle: laurenbarker
* Slack handle: lbarker

[**Stephen Karpeles**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#skarpeles)	

* Title: Website Developer and Designer
* Email: skarpeles@gitlab.com
* GitLab handle: @skarpeles
* Slack handle: @Stephen Karpeles

</details>

## Growth team

<details markdown="1">

<summary>show/hide this section</summary>

### Overview
{:.no_toc}

[ TODO : Document ]

### Team  

[**Shane Rice**](https://about.gitlab.com/company/team/#shanerice)

* Title: Manager, Growth Marketing
* GitLab handle: shanerice
* Slack handle: shanerice

[**Niall Cregan**](https://about.gitlab.com/company/team/#niallcregan)

* Title: Growth Marketing Associate
* GitLab handle: ncregan
* Slack handle: Niall Cregan

### Website analytics dashboards

We use Google dataStudio to create an easy to share and repeatable process for sharing analytics data for about.gitlab.com. dataStudio allows us to combine data from Google Analytics, Google Ads, and a variety of other sources to create reports with key data for any GitLab team.

When we connect data from Google Sheets everyone on the DMP team needs to able to access and edit data as needed. To accomplish this we add all Google Sheets used in dataStudio to the Shared Drive in the [dataStudio sub-folder](https://drive.google.com/drive/folders/1pO0fVLM-K0KrLNu8MWvzI-i96QXFdgeR). Be sure you only share your Google Sheets in the shared drive because dataStudio does not currently support sharing data sources from shared drives.

### Self-serve Website analytics dashboards
* Google Analytics traffic: If you would like to check the traffic, referrals, or clicks off of a page hosted on the marketing site (about, docs, etc) just enter your page URL or URLs into the `Page` field in this dataStudio report [Page-level Google Analytics data](https://datastudio.google.com/u/0/reporting/11pNZyzJ1JEudO4jRWaX989etLLcR6wUn/page/KNqS).
* Google Search results keywords: If you would like to check the keywords from a specific about.gitlab.com page in Google search, just enter your page URL into the `Landing Page` field in this dataStudio report [Google Search Console keyword lookup](https://datastudio.google.com/open/1No1sSsCH2EHkqPyLl-AEEQ1be3_p_kpj).
* Google Search results pages: If you would like a list of URLs surfacing for a specific keyword surfacing for about.gitlab.com in Google search (about, docs, etc) just enter your keyword into the `Query` field in this dataStudio report [Google Search Console landing page lookup](https://datastudio.google.com/open/10Qa9AgGt11xJWaycUDfvFq1Nn714sJQq).

#### Active dataStudio dashboards

- [Marketing metrics dashboard](https://datastudio.google.com/reporting/1mvDffnzlIWsr-2S_cvkpRx0X25hiM_TI/page/1M) — Used to generate data for our monthly marketing metrics deck. A few elements update manually, ping the Digital Marketing team in #digitalmarketing if you need updated numbers.
- [Content marketing dashboard](https://datastudio.google.com/reporting/1NIxCW309H19eLqc4rz0S8WqcmqPMK4Qb/page/FCsh) — Blog content reports, primarily used by the content team to track OKR progress.
- [Just Commit dashboard](https://datastudio.google.com/reporting/1dbt-3WI6KzySYrnolIUfCufvvtba20f9/page/kWdQ) — Tracks progress of Just Commit integrated campaign.
- [Job pages dashboard](https://datastudio.google.com/reporting/1w6TwUeGjkQpPZz4jvp9Hye8vdGP6MYel/page/JcPY) — Provides context around job page interactions.
- [Security releases dashboard](https://datastudio.google.com/reporting/1bP748BOhYmgWRcfeoRiSCOHz7q4NUMkV/page/l7vj) — Website analytics data for security release blog posts.
- [about.gitlab.com CrUX dashboard](https://datastudio.google.com/reporting/1f-akzELoGzJRdBFPgMTzgHPrSOshUgki/page/cJUR) — *Public* See [Chrome User Experience Report](https://developers.google.com/web/tools/chrome-user-experience-report/) for definition of report dimensions. This dashboard shows CrUX data for about.gitlab.com, assembled with PageSpeed Insights and Public Google BigQuery.  

### Links on about.gitlab.com

We should link to resources that will help our readers. Be sure to include links to blog posts, guides, and other reference material. You can also include links to company or product websites if they are relevant to your topic. These links do not need to be "nofollowed" if they are informational.

However, we should use [Google's guidelines on nofollowing links](https://webmasters.googleblog.com/2016/03/best-practices-for-bloggers-reviewing.html) when we exchange a link for a product or service. It's also a best practice to ask for a nofollow link when we sponsor and disclose our links to sponsored content.

### Website Health Check

Regular website health checks should be performed. These checks are meant to ensure that there are no issues with the website that could cause issues with traffic to the site. These are the following things to check to ensure the health of the site:

- [Google Search Console](https://www.google.com/webmasters/tools/)
- [Google Analytics](https://analytics.google.com/analytics/web/)

Issues to look for in each of these tools:

- **Google Search Console**: Check the dashboard and messages for any important notifications regarding the website. Also check under `Search Traffic` > `Manual Actions` for any URLs that have been identified as spam or harmful content. Forward security warnings to the Abuse team and follow the [DMCA complaint process](/handbook/support/workflows/dmca.html) with the support team.
- **Google Analytics**: Compare organic site traffic from the most previous week compared to the previous week and look for any large fluctuations.

### Using robots metadata to manage search index

There are times we need to keep pages out of search indexes. For example, we might duplicate most of a page to improve conversion for an ad campaign. It's relatively rare to use this, but it's an important tool that helps us increase organic reach and paid advertising efficiency.

All pages are set to `meta name="robots" content="index, follow"` by default. To exclude a page from the index add `noindex: true` to the frontmatter, and this will set the robots metadata to `meta name="robots" content="noindex, follow"`.

### about.gitlab.com redirects

Occasionally we need to change URL structures of the website, and we want to make sure that people can find pages they need. We use 301 redirects to send people to the right URL when it's appropriate. 

#### about.gitlab.com redirect policy

We will redirect URLs included in Google's search index. A simple test to see if a page is indexed is to search for the URL with a site modifier, `site:url`, using Google.

We want to reduce the number of internal redirects, which means we need to update links across about.gitlab.com when we change URLs. When you request a redirect please indicate whether or not you were able to search across about.gitlab.com and update the links for a page you're moving.

#### Request an about.gitlab.com redirect

The Digital Marketing Programs team can set up and manage all redirects for about.gitlab.com.

To redirect an outdated page, open an issue with the [set up a new redirect template in the Digital Marketing Programs project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=set-up-a-new-redirect). You'll need to provide the following:

- Old URL that needs to be redirected
- New URL where users should now be sent
- Were you able to update existing links to the old URL across about.gitlab.com?

If you have any questions or concerns regarding your redirect request, ask for help in the `#digital-marketing` channel on Slack.

#### Redirect process documentation

The Digital Marketing Programs team uses these [technical details for the redirect process](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/redirects_on_about_gitlab_com.md) on about.gitlab.com.

### GitLab Google Tag Manager system

We use Google Tag Manager(GTM) to simplify activity tracking on about.gitlab.com with Google Analytics. This documents the system the Digital Marketing Programs team uses with our Google Tag Manager container.

#### Naming convention

We use semantic names for tags, triggers, and variables to make it simpler to determine what these components are supposed to do. Use an em dash (shift+option-_) to divide the components of each GTM component name.

Tags start with the platform, followed by the tag’s purpose, and are finally contextualized to about.gitlab.com. Any tags related to timed events need the timeframe indicated in a note attached to the tag in GTM to make it clear when to remove a tag.

###### Tag naming examples

- Google Analytics Event — Free Trial CTA click  
- Google Ads —  conversion tracker  
- Facebook — base pixel
- Drift — snippet v 0.3.1

Triggers start with a description of the action triggering a tag, followed by contextualization for about.gitlab.com.

###### Trigger naming examples

- Link click — Learn more 100M carousel  
- CTA click — cta-btn  
- Custom event — mktoformSubmit free trial  

Variables start with the tag or trigger they reference, followed by contextual data about their purpose.

###### Variable naming examples

- Google Analytics — GitLab Universal Analytics ID  
- dataLayer — postType custom dimension  

If you are making changes to the GTM container and have questions about what to name one of these components the Digital Marketing Programs team can help.

#### dataLayer values

Today we’re using the dataLayer sparingly on about.gitlab.com, with our primary use focused on [differentiating blog content with `postType`](/handbook/marketing/blog/#definitions). We’ll expand how we use the dataLayer to create content groupings for improved customer journey insights and document those updates here.

#### Event tags

We need consistent tags across Google Analytics events and have introduced the following structure to our event tags. Our goal is to cover important visitor events with the smallest number of tags in Google Tag Manager. Reducing the number of tags and the overall complexity of our Google Tag Manager container helps us spot and fix coverage issues faster.

![GitLab Google Tag Manager event structure](/images/handbook/marketing/GTM-event-structure.png)

These three components help us organize and identify specific event data. **Event Categories** help us group specific customer journey steps, **Event Actions** describe visitor interactions with about.gitlab.com, and **Event Labels** provide contextual details for reporting our performance.

**Example Event Label**  
{{Page URL}} | {{Referrer}} | {{Click text}} | {{Click URL}} | {{Click Class}}

Google Analytics limits event label fields to around 2000 characters, and we'll update the handbook if we start to see truncated event labels.

### Google Tag Manager inventory

We're using Simo Ahava's Googl Sheets Add-On to sync notes for our tags and create a [GitLab Google Tag Manager inventory](https://docs.google.com/spreadsheets/d/1oT5AQQ0nH4-7iS-QY-UJP4vbFxv2GCGy9XiKpj8ebuU/edit#gid=1443259273) This simplifies scanning and searching over the Google Tag Manager web app.

### Changes of note
Whenever we make major changes to tags through Google Tag Manager we document them in [changes of note](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/changes-of-note/). Examples of changes we document are adding or removing tags, changing tag sequencing, or changing when a tag is fired.

### Using marketing trend data

Search term volume is hard to estimate. Different tools use different methodolgies and models for reporting this data. AdWords provides data from Google, but the ranges are broad and terms can be combined into a single phrase. Google Trend data normalizes search trends from 1 to 100 based on the terms you're exploring, which doesn't give us any idea of how many people are using a particular phrase.

For our keyword research we rely on Moz Keyword Explorer data because it provides a narrower volume with full phrase integrity. When we report keyword volume, this is the tool we use for this data.

### Google Analytics Crash Course

#### Dimensions vs Metrics

Dimensions are the different attributes of your data. For example, the landing page is a dimension that is the first page a person views when they come to the site.

Metrics are the numbers that are being measured, such as number of page views or number of sessions.

Each dimension and metric has a scope, so it’s important to understand the three different scopes:

1. User-level
1. Session-level
1. Page-level

Due to the scoping, not every dimension can be combined with every metric. In most cases, the dimensions and the metrics should match the scope.

#### Understanding Reporting

###### Setting a date range

You can use the calendar in the top right to set the active date range. You can also select the `compare to` box to compare metrics from different time periods. This will allow you to see month over month or year over year growth for the desired metrics.

###### Annotations

Annotations are used to mark a point in time in Google Analytics. They can be used to mark an important event such as a change to the setup of Google Analytics, or an event that heavily impacted traffic positively or negatively.

To create an annotation, double-click on a date. The double-click will bring up the annotation field where you can enter details, and select `private` or `public`. Public annotations can be seen by anyone that has access to that view within Google Analytics, and private annotations can only be seen by you.

###### Data Tables

Most reports have a data table below the graph. The data tables contain a dimension and associated metrics.

The `Primary Dimension` of the data tables can be changed by selecting a different primary dimension from above the table.

A secondary dimension can also be added by clicking on the `Secondary Dimension` button above the table and selecting the secondary dimension you’d like to add to the table.

#### Audience Reports

The audience reports are used to understand characteristics of your users such as location, and browser used, and user behavior over multiple visits such as average session time.

#### Acquisition Reports

The acquisition reports help you know how people find the website. These reports will help you to analyze the benefits of the different digital marketing efforts that you are involved in.

###### Channels Report

The `All Traffic > Channels` report breaks down all the different channels that are sending traffic to the site. You can click on any of the channels to drill down  and get more granular data about that specific channel. For example, if you click on the `Referral` channel, you will see which sites are referring traffic to your site.

#### Behaviors Reports

The behaviors section is about how users use the website. This includes what pages of the site people are looking at as well as how they flow through the site.

###### All Pages Report

The `All Pages` report shows the number of times a given page was viewed within the selected period of time. You can change the primary dimension to `Page Title` if it is easier to tell what the page is by looking at the title rather than the URL.

`Avg. Time on Page` and `Bounce Rate` can be used to find underperforming content or content that is very engaging to users and can be used in future marketing efforts.

###### Landing Pages and Exit Pages

The `Landing Pages` and `Exit Pages` reports are scoped at the session and tell us how many people are beginning a session at a certain page (landing page) and how many people are ending a session at a certain page (exit page).

These reports can be valuable to see what content is bringing people to the site and what content is causing people to leave the site.

###### Events

Events can be set up to track actions that people take on the website, such as clicking links or selecting drop downs. These events can be set up in Google Tag Manager and for the most part won’t require any additional code to be placed on the website.

</details>

</details>



















# Other

<details markdown="1">

<summary>show/hide this section</summary>

## Calendars
{:.no_toc}

<details markdown="1">

<summary>show/hide this section</summary>

[ TODO : Document ]

* [TODO: Homepage merchandising schedule](#)

</details>

## Tools

<details markdown="1">

<summary>show/hide this section</summary>

- [Adobe Creative Cloud / Suite](https://www.adobe.com/): Adobe Creative Cloud is a set of applications and services from Adobe Inc. that gives subscribers access to a collection of software used for graphic design, video editing, web development, photography, along with a set of mobile applications and also some optional cloud services.
- [Sketch](https://www.sketch.com/): Create, prototype, collaborate and turn your ideas into incredible products with the definitive platform for digital design.
- [Mural](https://mural.co/): MURAL is an Online Virtual Collaboration Space, Easy to Use Specially Designed for Teams. You can Post Stickies, Share Ideas, Brainstorm and Run Product Sprints.
- [Google Analytics](https://analytics.google.com/analytics/web/): Google Analytics lets you measure your advertising ROI as well as track your Flash, video, and social networking sites and applications.
- [Sisense ( previously Periscope )](https://www.sisense.com/product/data-teams/): Sisense for Cloud Data Teams (previously Periscope Data) empowers data teams to quickly connect to cloud data sources, then explore and analyze data in a matter of minutes. Extend cloud investments with the Sisense analytics platform to build, embed, and deploy analytics at scale.
- [Hotjar](https://www.hotjar.com/): Hotjar is a powerful tool that reveals the online behavior and voice of your users. By combining both Analysis and Feedback tools, Hotjar gives you the ‘big picture’ of how to improve your site's user experience and performance/conversion rates.
- [Launch Darkly](https://launchdarkly.com/): LaunchDarkly is a Feature Management Platform that serves over 100 billion feature flags daily to help software teams build better software, faster.
- [Google Optimize](https://optimize.google.com/optimize/home/): Google Website Optimizer was a free website optimization tool that helped online marketers and webmasters increase visitor conversion rates and overall visitor satisfaction by continually testing different combinations of website content.

</details>

## Experiments

<details markdown="1">

<summary>show/hide this section</summary>

This section is related to A/B and multivariate testing on the marketing website, about.gitlab.com. It is a work in progress while we assess new testing tools for integration into our toolkit.

Until the toolkit assessment is finalized, please reference digital marketing's [testing documentation](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#create-a-culture-of-testing-and-optimization).

Going forward, we hope newly created issues align with the [growth team's testing template](https://gitlab.com/gitlab-org/growth/product/-/blob/master/.gitlab/issue_templates/Growth%20experiment.md).

**Before you experiment**

Always gather relevant heatmaps, analytics, metrics, KPI (key performance indicators), etc.

**Testing Tools**

We are in the process of establishing a new toolset for running experiments. Our hybrid suite of tools will include:

*Testing via feature flags*

This is where we plan to do the bulk of our testing. We can run several of these at the same time. For full-page, partial-page, component, and small changes.

*Feature flag best practices*

Feature flags should be implemented in code similarly to the includes system. Example:

* Login to our third party service and create a feature flag and related configuration variables.
* Assign ownership of that flag from within the interface.
* Edit a page.
* Put the existing contents of the page into an include file named `/source/experiments/1234-control.html.haml`, where experiments is the folder name instead of includes and 1234 is the id number of the associated issue. "Control" refers to the baseline measurement you are testing against.
* Duplicate that include file with the name `/source/experiments/1234-test.html.haml`
* Make your changes and validate the feature toggle works locally and/or on a review app before deployment.
* Ensure you'll be able to collect all the data you need. Setup appropriate tools like heatmaps and analytics.
* Note that one advantage of feature flags is that they can be released to production without being turned on.
* When ready, enable the test. Start gathering data.

**Testing via CDN**

This is an advanced tool meant to test large-scale changes at a systemic level. For now we plan to run only one of these at a time.

**Testing via WYSIWYG**

This is a rudimentary tool for small-scale changes with few safeguards and important caveats. We can use this for small items like colors and copy but not layout. This is mainly meant as a tool for non-developers.

</details>

</details>
