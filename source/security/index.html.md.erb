---
layout: security
title: Security - Trust Center
description: "GitLab CE is open source, which means we have hundreds of people checking the source code. GitLab EE's source code is open for inspection."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Security

GitLab protects your data with encryption in transit and at rest, providing organization-wide protection mechanism such as SAML SSO and enforced 2FA.

### Security of GitLab as an Open-Source Product
{: #open-source-security}

GitLab CE is open source and has over 2,000 contributors.
This means there have been over 2,000 pairs of eyes looking at the GitLab CE source code.
GitLab EE is open-core, which means the source code is also open for inspection to our customers.

### GitLab Security Releases
{: #gitlab-security-releases}

GitLab releases patches for vulnerabilities in dedicated security releases.
There are two types of security releases: a monthly, scheduled security 
release, released a week after the feature release (which deploys on the 22nd of each month),
and ad-hoc security releases for critical vulnerabilities. The fix for every
vulnerability is backported to the current release, and the 2 previous
`major.minor` versions.  

To help you understand the vulnerabilities that were fixed, a blog post accompanies
each security release and includes a brief description, the 
versions affected, and the assigned CVE ID for each vulnerability. 
Feature and security release blog posts are located in the [`releases` section of our blog](/releases/categories/releases/).
In addition, the issues detailing each vulnerability are made public 30 days 
after the release in which they were patched. It is highly recommended that 
all customers upgrade to at least the latest security release for their 
supported version.

To be notified when a security release is released, the following channels are
available:
- [Security Notices Mailing list](/company/contact/)
- [Security Release RSS feed](/security-releases.xml)
- [Gitlab Twitter feed](https://twitter.com/gitlab)


---

## Privacy

We are committed to protecting the [privacy of your data](/privacy/), preventing it from unauthorized access. 

### GitLab's Access to Your Private Repositories
{: #privacy-repositories}

GitLab Support will never access private repositories unless required for support reasons and only when requested by the owner of the repository via a support ticket.
When working a support issue we strive to respect your privacy as much as possible. Once we've [verified account ownership](/handbook/support/workflows/account_verification.html), we will only access the files and settings needed to resolve your issue.
Support may sign into your account to access configurations but we will limit the scope of our review to the minimal required to solve your issue.
In the event we need to pull a clone of your code, we will only do so with your consent.
All cloned repositories are deleted as soon as the support issue has been resolved.
There are two exceptions to this policy: we have determined that there has been a violation of our [terms of service](/terms/) or if we are compelled as part of a legal matter.

### Data Protection Officers 

Oversight of Data Security is handled by GitLab's Data Protection Officers. Should you wish to make modifications, deletions or additions to any personal data you believe to be captured by GitLab, or if you have any general security concerns, please contact a GitLab **Data Protection Officer (DPO)** by emailing [security@gitlab.com](mailto:security@gitlab.com).

In the event you feel that you have not received proper attention to your data concern, or if you have any other legal/law enforcement concern - please contact GitLab's Legal Department at [Legal@gitlab.com](mailto:legal@gitlab.com).

You also always have the right to lodge a complaint with a supervisory authority if you believe that your data is being misused.

### Other Contact Methods

- [Disclosure policy](/security/disclosure/)
- [Vulnerability acknowledgements](/security/vulnerability-acknowledgements/)
- [Team member immediate security panic](/handbook/security/#panic-email)
- [Report a DMCA claim](/handbook/dmca/)

---

## Compliance

A SOC 2 (Service and Organization Controls) examination with a third party assessor is on GitLab's 2020 security compliance roadmap. Whereas we currently do not maintain an independently certified security controls certification, we do operate within an [information security control framework](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)

Please visit our [Security Compliance](/handbook/engineering/security/security-assurance/security-compliance/compliance.html) page for more information on our Compliance Roadmap. 

## Common Security Related Questions for Enterprises
{: #common-security-related-questions}

### Governance
{: #governance}

1. Do you maintain a quality management system (QMS) approved by management?
In lieu of a formal and static QMS, GitLab has a dynamic and responsive approach to quality management.
Does your quality management system (QMS) include coverage for software application security principles?
   - YES

1. Is quality management system (QMS) content published and communicated to all relevant employees?
   - YES.

1. Is quality management system (QMS) content reviewed and updated (if appropriate) at least once per year?
   - It is considered a constant Work In Progress; and updated almost daily in small increments.

1. Is there defined management oversight who is responsible for application quality and security reporting & signoff?
   - YES. See our [team page](/company/team/)  (search for "maintainer"). Also see [code review guidelines](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/code_review.md) which mentions the endbosses, and stresses team-wide code review.

1. Is access to and maintenance of applications, systems, network components (including routers, databases, firewalls, voice communications servers, voice recording servers, etc), operating systems, virtualization components, hypervisors, or other information objects restricted to authorized personnel only?
   - YES

1. Is access to and maintenance of applications, systems, network components (including routers, firewalls, voice communications servers, voice recording servers, voice response units (VRU) etc), operating systems, virtualization components, hypervisors, or other information objects granted based upon need-to-know job function?
   - YES

1. For all IT systems including but not limited to servers, routers, switches, firewalls, databases, and external social spaces, is management approval required prior to creating all user and privileged accounts (e.g., system or security administrator)?
   - YES

1. For all IT systems including but not limited to servers, routers, switches, firewalls, databases are privileged accounts (e.g., system or security administrator) logged at all times and reviewed on at least a quarterly basis?
   - YES

1. Are all user accounts (including, but not limited to, standard user, system administrator, security administrator, internal social spaces, etc) assigned to an individual employee and not shared?
   - YES

1. Are all user accounts disabled after no more than ten unsuccessful logon attempts?
   - YES

1. For all IT systems (including but not limited to servers, routers, database, switches, firewalls, external social spaces), are inactive user and privileged accounts (e.g., system administrator or security administrator) disabled after 90 days or more?
   - YES

1. Is a user's identity verified before communicating an initial/temporary password or initiating a password reset by an automated or manual process?
   - YES

1. Do application, system, and device passwords (including routers, firewalls, databases, and external social spaces) require passwords to have the following characteristics: 1. minimum length of 8 characters, 2. chosen from any acceptable character sets available on the target system, 3. includes at least one alphabetic and one numeric character.)
   - YES

1. Are passwords prevented from being displayed in clear text during user authentication or in electronic/printed reports?
   - YES

1. Are passwords/PINs sent to users utilizing a secure method (e.g. secure e-mail) and sent separately from other authentication information such as the user account?
   - YES

1. For all IT systems (including but not limited to servers, routers, databases, switches, firewalls), are user and privileged account (e.g., system or security administrator) passwords changed at least every 90 days?
   - YES

1. Are users required to authenticate prior to changing their password?
   - YES

1. Are all system, application and device password files encrypted using an industry standard encryption algorithm where technically feasible?
   - YES

1. In instances where a software token is used to access an application or system, is a password or PIN required?
   - YES

1. In instances where a software token is used to access an application or system, are stored keys and software token files encrypted using an industry standard algorithm and smartcards compliant to FIPS level 2 or above?
   - YES

1. For externally hosted environment, is there separation of administrative access between the hosting infrastructure/platform and the hosted platform and data?
   - YES

1. If user accounts are assigned to non-permanent personnel (e.g., contractors, consultants)  for troubleshooting purposes, are the accounts disabled or removed after each use?
   - YES

1. Is the retirement or replacement of encryption keys included in key management procedures when the integrity of the key has been weakened (such as departure of an employee with key knowledge) or keys are suspected of being compromised?
   - YES

1. If you use cloud services, do you ensure that confidential data or an aggregation of proprietary information that can reveal confidential information is encrypted to ensure confidentiality at rest and in transit?
   - YES

1. If you use cloud services, do you have key management procedures to manage and maintain encryption keys?
   - YES

1. How does GitLab help companies ensure HIPAA compliance?
    - <%= partial "includes/hipaa_faq.md" %>

### Software Development Life Cycle (SDLC)
{: #sdlc}

1. Are there documented processes, procedures, standards and templates used in your SDLC process?
   - YES. See our [Contributing guidelines](/community/contribute/), and related documentation on [code review](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/code_review.md) and [development processes](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc/development).

1. Do the materials above  include references to application security best-practices and principles being followed?
   - YES.

1. Are design and code reviews performed as part of your SDLC processes?
   - YES. See our [testing guidelines](http://doc.gitlab.com/ee/development/testing.html).

1. Are security considerations (checklists, standards and policies) referenced in the design and code review?
   - YES.

1. Is app security threat modeling performed when deemed appropriate (i.e. new or changed architectures and approaches)?
   - NO.

1. Is application code managed in a secure configuration management system with access controls?
   - YES.

1. Is there a configuration management plan and are release artifacts maintained in a configuration management system?
   - YES.

1. Are test plans and records kept that reflects the tests performed and results observed for each release?
   - YES.

1. Is security testing defined and included in the test plan for each release?
   - YES.

1. Is a release criteria defined, measured and reported on to confirm targeted release quality is achieved?
   - YES. We do manual QA testing for each monthly release and deploy all new versions on GitLab.com to be our own test subjects.

1. Are specific application security characteristics and measures part of the defined release criteria?
   - NO.

1. Do you work with third parties that may have access to your IP and sensitive data?
   - N/A w.r.t. code, as our code is all open-source (community edition) or open-core (enterprise edition).

1. If so, is access to data controlled by terms of Non-Disclosure Agreements?
   - N/A

### Training
{: #training}

1. Is Internal company training available & performed commensurate with personnel roles and responsibilities?
   - YES; peer-to-peer training is commonplace.

1. Does training include security awareness?
   - YES; as applicable for the role.

1. Does training include education on policies, standards, procedures and updates when needed?
   - YES; as applicable.

1. Are personnel training plans and records kept for internal company compliance purposes?
   - Tasks and training completed during [onboarding](/handbook/general-onboarding/) are recorded.

### Enterprise Protection
{: #enterprise-protection}

1. Is antivirus protection enabled on endpoints?
   - NO. Antivirus solution will be dependent on management decision on device management strategy. We use Apple Macbook Pro's and Linux GitLab-issued laptops. Mac's provide application sandboxing which will require the malware to escape; requires by default all applications installed are signed. Linux laptops are operating with user trust. GitLab hosts are monitored utilizing Uptycs and OSquery.

### Validation
{: #validation}

1. Are results from the execution of test plans reported and used to track and justify release readiness?
   - YES. We require all automated tests to pass before any official release (monthly and patch versions), and perform manual QA testing for each monthly release.

1. Does the quality assurance organization have authority to delay shipment of releases due to non-conformance reasons?
   - YES.

1. Is some form of static code scanning performed as part of the release acceptance? What tools are used?
   - YES. For example, [brakeman](https://github.com/presidentbeef/brakeman) and [bundler-audit](https://github.com/rubysec/bundler-audit) are part of our test suite to be alerted to any security issues in our dependent Ruby libraries.

1. Is some form of dynamic code scanning performed as part of the release acceptance? What tools are used?
   - YES. We use GitLab CI for this purpose.

### Security Response
{: #security-response}

1. Do you have a documented company security incident response process?
   - YES. See [security documentation](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/security.md) as well as details on [service level response times and priorities](/handbook/support/).

1. Do your maintenance releases include fixes for both quality and security related issues?
   - YES.

1. Do you provide dedicated security patches for software versions that are released and supported in the field? How?
   - YES, for the latest release and the two prior monthly releases, when applicable.

1. Is there proactive notification provided to customers and software partners (PTC)?  How?
   - YES. Notifications in the "version check" image, blog posts, tweets, and a mailing list just for security fixes.

1. Do you have a formal risk severity classification assessment approach?
   - NO.

1. Is there a specified response policy that includes the timeframe issues are to be addressed?
   - YES. See [security documentation](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/security.md) as well as details on [service level response times and priorities](/handbook/support/).

---

## GitLab Development Guidelines
{: #gitlab-development-guidelines}

* [Guidelines for shell commands in the GitLab codebase](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/shell_commands.md)
* For community contributions, we enforce [style guides](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#style-guides)
* In addition, both for community contributions and for internal development, we use [brakeman](http://brakemanscanner.org/) and [RuboCop](https://github.com/bbatsov/rubocop) run by GitLab CI.
* We engage security experts to do white box testing such as fault injection (manual), penetration testing (manual) and vulnerabilities testing (both automatic, and manual)

GitLab also has a [responsible disclosure program](/security/disclosure/).

---

## External Testing
{: #external-testing}

GitLab's on-premises product as well as GitLab.com SaaS is subjected to security
tests regularly through a combination of:

- Bug reporting program through direct email, or HackerOne. For more details on
this, please see the page on [responsible disclosure](/security/disclosure/).
- A combination of third party white box, grey box, and pen testing.

[Grimm](https://www.grimm-co.com) carries out a black box
assessment of GitLab and GitLab.com annually and the last assessment was performed in Q3 of 2019. Their report is available for
(prospective) enterprise customers by emailing `security@gitlab.com`.

Given that penetration testing and other simulated events are frequently indistinguishable from real life attack activities and have the potential to impact performance and site reliability, we do not permit customers, prospective customers, or members of the larger GitLab community to conduct penetration tests and vulnerability scans to or originating from the GitLab.com environment.

---

## Business Continuity Plan
{: #business-continuity-plan}
GitLab, by its remote-only nature, is not easily affected by typical causes of business disruption, such as local failures of equipment, power supplies, telecommunications, social unrest, terrorist attacks, fire, or natural disasters.

The detailed business continuity plan is documented in the [BuinessOps section of this handbook](/handbook/business-ops/gitlab-business-continuity-plan.html).


## Data Breach Notification Policy
{: #data-breach-notification-policy}

This policy defines what qualifies as a breach of user data, what actions will be
taken in the event of user data exposure or compromise, and the timeline for
action.

This policy applies to user data stored on GitLab.com and GitLab Hosted (GitHost).
It does not apply to self-managed / on-premises GitLab instances or instances hosted with other
providers than GitLab.com and GitLab Hosted.

### Data Classification - What information is covered by this policy

This policy covers "private user data" stored by GitLab.com and GitLab Hosted,
 and includes:

* Private git Repositories
* Private Wikis, Issues, and Merge Requests
* Encrypted Passwords
* Private Email Addresses

Note: GitLab.com and GitLab Hosted do not store any "personally identifiable information"
(PII) such as (i) Private Addresses, (ii) Credit Card Numbers, (iii) Bank
Account Information, (iv) ID numbers (e.g. passport, driver's license, social
security, national identification, etc.). GitLab.com and GitLab Hosted also do not store
any "personal health information" (PHI). Therefore, laws and regulations
relating to PII and PHI do not apply.

### What qualifies as a breach

A breach of user data is the unintended or accidental exposure of private user
data. This can be caused by accidents, misconfigurations, or malicious actions
performed by an external attacker or team member.

An event is considered a data breach when there is evidence that private user data
has been exposed to the public or to an untrusted third party.

Trusted third parties may have authorized access to user data under a signed
Non-Disclosure Agreement (NDA). Such trusted third parties include but are not
limited to:

* Cloud service providers
* Database consultants
* Security auditors
* Financial auditors

Some examples of a user data breach would include:

* Compromise of a database server that contains private user data with evidence
that an attacker may have had access to or copied the data off-site.
* Compromise of an application server account that has access to private user data
and evidence that the attacker has downloaded or accessed private data.
* Theft of a device known to contain private user data.
* Web application attack used to download a list of all user emails and encrypted
passwords.

### What is not considered a breach

Examples of security incidents that would not be considered a breach of private
user data:

* Compromise of an application server that does not contain or have access to
private user data.
* Compromise of a team member application account that does not have access to
private user data.
* Malware infection on a server or team member computer system that does not
contain private user data.
* Compromise of non-sensitive user data such as login IP addresses, login history,
project permissions.
* Unintentional disclosure of project names, group names, issue titles, or
project or user metadata unless this data can cause damage to the user or their
business.
* Discovery of a vulnerability that could have been used to compromise
private data, but for which there is no evidence of exploitation.
* Theft of a team member's mobile device that does not contain private user
data.
* Theft of a team member's private keys, tokens, or other credentials provided
there is no evidence they were used to access private user data.

### Who will be notified in the event of a data breach

If GitLab has detected evidence of a breach of GitLab.com or GitLab Hosted private
user data, all affected users will be notified via the configured email address
for their accounts. Emails will contain information on what data was exposed or
compromised, when, and for how long (to the extent this information is available).

For a breach that exposes private data for a large number of users, the public will
also be informed via the configured email addresses for their accounts, and
additional means of communication will be considered (e.g. press release, the
blog, etc.) on a case by case basis.

### Notification timing

GitLab will endeavor to notify users within 24 hours of breach discovery. This
may be delayed when necessary to comply with requests by law enforcement.
