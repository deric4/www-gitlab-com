---
layout: markdown_page
title: "Embracing asynchronous communication"
twitter_image: "/images/opengraph/all-remote.jpg"
---

## On this page
{:.no_toc}

- TOC
{:toc}
## Introduction

In a world dictated by [calendars](/blog/2019/12/30/mastering-the-all-remote-environment/) and schedules, people are conditioned to operate in synchronicity — a manner in which two or more parties exert effort to be in the same place (either physically or virtually) at the same time. Asynchronous communication is the art of communicating and moving projects forward *without* the need for additional stakeholders to be available at the same time your communique is sent. 

In an all-remote setting, where team members are empowered to live and work where they're most fulfilled, mastering asynchronous workflows is vital to avoiding dysfunction and enjoying outsized efficiencies. Increasingly, operating asynchronously is necessary even in colocated companies which have team members on various floors or offices, especially when multiple time zones are involved. 

## How to implement asynchronous workflows

The easiest way to enter into an asynchronous mindset is to ask this question: "How would I deliver this message, present this work, or move this project forward right now if no one else on my team (or in my company) were awake?"

This removes the temptation to take shortcuts, or to call a [meeting](/company/culture/all-remote/meetings/) to simply gather input. (After all, every meeting should be [a review of a concrete proposal](/handbook/values/#make-a-proposal), and only called when it will lead to a more efficient outcome than would be possible asynchronously.)

### Documentation as a prerequisite

Mastering the art of communicating asynchronously has a prerequisite: [documentation](/company/culture/all-remote/handbook-first-documentation/). At its core, asynchronous communication *is* documentation. It is delivering a message or series of messages in a way that does not require the recipient(s) to be available — or even awake — at the same time. 

If your organization has no standardized method of documentation, establish that first. Otherwise, team members will be left to determine their own methods for communicating asynchronously, creating a cacophony of textual noise which is poorly organized and difficult to query against. 

### Utilize the right tools

Asynchronous communication works best when there is companywide alignment on how and where to input communication. Leaders should carefully select their tools, aiming to direct communications to as few channels as possible. 

A common frustration in large organizations — regardless of what [stage of remote](/company/culture/all-remote/stages/) they're in — is the chaotic splintering of communication. Projects frequently end up strewn across email, chat, text messages, unrecorded meetings, design tools, Google Docs, etc. While there are a litany of unified communication tools available which attempt to wrangle all of that, you're best served by choosing a single system for communicating project progress.

At GitLab ([the company](/company/)), that destination is GitLab ([the product](/product/)). Any side conversation that occurs in a meeting is [documented](/handbook/communication/#internal-communication) in an agenda, and the useful elements are [contextualized](/company/culture/all-remote/effective-communication/) and ported to relevant GitLab issues and/or merge requests. The same goes for side conversations that happen in Slack or email. Relevant portions are ported over into GitLab (the product), which is the [single source of truth](/handbook/values/#single-source-of-truth) for any ongoing work.

**If it's not in a GitLab issue or merge request, it doesn't exist**. This [mentality](/handbook/communication/#everything-starts-with-a-merge-request) is essential to reaping the benefits of asynchronous communication. 

### Using GitLab for remote collaboration

GitLab is a collaboration tool designed to help people work better together whether they are in the same location or spread across multiple time zones. Originally, GitLab let software developers collaborate on writing code and packaging it up into software applications. Today, GitLab has a wide range of capabilities used by people around the globe in all kinds of companies and roles.

You can learn more at GitLab's [remote team solutions page](/solutions/gitlab-for-remote/).

### Remove bias toward one time zone

Leaders should strive to remove bias toward one time zone, or one swath of time zones (e.g. time zones covering North America). For company all-hands meetings, look to rotate these to accommodate a more diverse array of time zones. Also consider recording them so that others can watch at a later time.

When hosting live learning sessions, for instance, host several instances so people around the globe are able to attend one that suits their schedule. 

If a company pulls too hard in the direction of one time zone (oftentimes the zone where most company executives live), it signals to the rest of the company that asyncronous workflows aren't taken seriously. 

## Asynchronous meeting participation

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/amiDcpIXIQ8?start=301" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the GitLab Unfiltered [video](https://youtu.be/amiDcpIXIQ8) above, two GitLab colleagues discuss lessons learned from managing a team asyncronously, across many time zones, while traveling.*

Have as few mandated meetings as possible. The notion of "[optional meetings](/company/culture/all-remote/meetings/#make-meetings-optional)" is absurd to those who only think in terms of syncronous communication — you're either at a meeting to contribute, or you aren't.

The beauty of asynchronous is that team members can [contribute](/blog/2019/12/23/six-key-practices-that-improve-communication/) to meetings that occur while they sleep.

Meetings are more easily made optional when each one has an [agenda](/company/culture/all-remote/meetings/#document-everything-live-yes-everything) and a Google Doc attached to each invite. This allows people anywhere in the world to contribute questions/input asynchronously in advance, and catch up on documented outcomes [at a later time](/blog/2019/12/10/how-to-build-a-more-productive-remote-team/). 

The person who called the meeting is responsible for contextualizing the outcomes and porting relevant snippets to relevant GitLab issues and/or merge requests. 

By placing this burden on the meeting organizer, it acts as a filter for whether or not a meeting is truly necessary. The organizer is responsible for informing the entire company, via post-meeting documentation, of the outcomes should team members [go searching](/company/culture/all-remote/self-service/). That's a big responsibility, which keeps the quantity of meetings in check. 

## Benefits of asynchronous workflows

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/cy6WGuzArgY?start=2019" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the GitLab Unfiltered [video](https://youtu.be/cy6WGuzArgY) above, Emna G., founder and CEO at [Veamly](https://veamly.com/), speaks with GitLab's [Darren M.](https://twitter.com/darrenmurph) about the impact of defaulting to asynchronous as it applies to stress, anxiety, mental health, and overall wellbeing.*

Working asynchronously is more efficient, less stressful, and more amenable to [scale](/company/culture/all-remote/scaling/). The benefits for both employee and employer are numerous, and we've highlighted a few below. 

### Mental health

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/IU2nTj6NSlQ?start=621" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the video above, GitLab's Head of Remote discusses the oft overlooked mental health benefits of asynchronous communication.*

A tremendous amount of stress comes with expectations to be online, available, and responsive during set working hours. Worse, our hyper-connected society has allowed this notion to seep into every hour of the day, destroying boundaries between work and self. 

An unsung benefit to working asynchronously is a [reduction of tension](/company/culture/all-remote/building-culture/#gratitude-and-transparency). When your entire company operates with an understanding that any team member could be offline at any time, for any reason, there is no expectation that one will reply instantly to an inquiry. 

This creates an environment where your [mental health](/company/culture/all-remote/mental-health/) is prioritized, freeing team members from a perpetual assault of notifications and judgment. 

When asked how asynchronous communication impacts a societal expectation for an immediate response at all hours of the day, [Darren M.](https://gitlab.com/dmurph), GitLab's Head of Remote, offered the following during an interview with [Stuart Miniman](https://twitter.com/stu/), host of [theCUBE](https://www.thecube.net/) and GM of Content at [SiliconANGLE Media](https://siliconangle.com/).

> Remote is much better for your mental health and sanity than other settings, and it's because it forces you to work asynchronously. At GitLab, we have people spread across 65 countries, so almost every time zone is covered. But, that also means that someone on your team is likely in a vastly different timezone. In fact, they may be asleep the entire time you're up working. 
>
> With an asynchronous mindset, it enables all of us to take a step back and assume that whatever we're doing is done with no one else online. It removes the burden of a nonstop string of messages where you're expected to respond to things immediately. 
>
> From a mental health standpoint, when you have an entire company that embraces that, we're all given a little more breathing room to do really deep work that requires long periods of uninterrupted time. 
>
> As a society, we're getting close to a tipping point where people are at their limit on how many more messages, or emails, or seemingly urgent pings they can manage while also doing their job well. We may be a bit ahead of the curve on that, but my hope is that the industry at large embraces asynchronous communication, and allows their people more time to actually do the work they were hired to do. 

### Everything is thoughtful 

A core problem with synchronous communication is the perception of deadlines. When there is an arbitrary start time and end time to a working day, there is an irrational pressure to communicate as much as possible between those times, oftentimes at the expense of processing time.

This is also entirely incongruent with today's business world. There is no actual start time and end time. Business occurs around the clock, in all time zones, in perpetuity. Attempting to shoehorn communications into a predefined set of hours without a documented need leads to [dysfunction](/handbook/values/#five-dysfunctions) and misinterpretation. 

[Sahil Lavingia](https://twitter.com/shl), founder and CEO at [Gumroad](https://gumroad.com/), shares a series of powerful benefits his company realized in going fully asynchronous.

<blockquote class="twitter-tweet tw-align-center"><p lang="en" dir="ltr">Going fully remote was nice, but the real benefit was in going fully asynchronous. Here are a list of the benefits we&#39;ve seen at <a href="https://twitter.com/gumroad?ref_src=twsrc%5Etfw">@Gumroad</a>:<br><br>A thread 👇🏽</p>&mdash; Sahil Lavingia (@shl) <a href="https://twitter.com/shl/status/1222545212477599751?ref_src=twsrc%5Etfw">January 29, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

> All communication is thoughtful. Because nothing is urgent (unless the site is down), comments are made after mindful processing and never in real-time. There's no drama
>
> Because everyone is always effectively "blocked," everyone plans ahead. It also means anyone can disappear for an hour, a day, or a week and not feel like they are holding the company back. Even me!
>
> People build their work around their life, not the other way around. This is especially great for new parents, but everyone benefits from being able to structure their days to maximize their happiness and productivity.
>
> This is possible because everything is documented. And because everyone talks through different text-based mediums, it's easy for people to peer into anything if they're curious (or take over if need be). There are also no meetings, and all numbers are public, so there's no FOMO.
>
> The software we ship is well-tested and incredibly stable. It has to be, because we're never online at the same time to "deploy" together. There are rarely fires to fight, and we lower the amount of technical debt we have at Gumroad every week too!
>
> Overall, it's a very low stress environment. Many of us don't even have Slack installed. Yet, we're shipping the best software we've ever shipped, and growing faster than ever. Funny how that works!

### Autonomy, empowerment, and agency

In an asynchronous company, team members are given [agency](/handbook/values/#give-agency) to move projects forward on a schedule that suits them. At GitLab, we [measure results, not hours](/handbook/values/#measure-results-not-hours). This means that people are free to achieve results when it best suits them. 

If they're [traveling](/company/culture/all-remote/people/#travelers) to a new time zone each month, or they've chosen to spend a beautiful afternoon [with family](/company/culture/all-remote/people/#worklife-harmony) in favor of working a time-shifted schedule upon their return, that's their [prerogative](/blog/2019/12/10/how-to-build-a-more-productive-remote-team/).

Unsurprisingly, providing those who are capable of being [managers of one](/handbook/values/#managers-of-one) with this type of autonomy leads to extraordinary [loyalty](/company/culture/all-remote/benefits/#for-your-organization), [retention](/handbook/people-group/people-group-metrics/#team-member-retention), and quality of work. 

To further optimize this approach, consider adding a "[no ask, must tell](/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off)" time off policy, which means team members do not need to ask permission to step away from work. 

### Plugging the knowledge leak

Asynchronous companies should implement a [low-context culture](/company/culture/all-remote/effective-communication/#understanding-low-context-communication). This means that communication is precise and direct. Team members forecast what questions may be asked about a communique and add in as much context as possible in its delivery. By assuming that the recipient is asleep, or perhaps doesn't even work at the company yet, this added context removes ambiguity and decreases the likelihood of misinterpretation. 

This may feel inefficient, as communiques may take longer to compose and edit. However, the long-term benefits are remarkable. At GitLab, we have years of documented decisions — such as [this example of availability over velocity](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/30046) — loaded with context. This enables new hires to sift through archives and understand the context of the moment, and what went into a given decision. 

Synchronous organizations often make decisions in a series of meetings, documenting little to nothing along the way, such that those who come into the process mid-stream are constantly wasting cycles on fact-finding missions. Plus, those who are hired after a significant decision is made has no way of understanding the context that went into something prior to their arrival, creating cavernous knowledge gaps that eat away at a company's efficiency. 

[Coda Hale](https://twitter.com/coda), principal engineer at MailChimp, articulates this well in a comprehensive article on organizational design entitled *[Work is Work](https://codahale.com/work-is-work/)*.

> A significant source of [failure demand](https://en.wikipedia.org/wiki/Lean_services#Value_Demand_and_Failure_Demand) for meetings and status updates is the desire of organizational leaders to keep abreast of who’s doing what. This situational awareness is indeed important, but trying to maintain it by calling meetings, messaging people on Slack, and catching people on the hallways is a significant systemic drag on organizational productivity.
>
> A better model for staying informed of developments as the organization scales is for groups to publish status updates as part of the regular cadence of their work. Leaders can asynchronously read these updates and, should the need arise, initiate additional, synchronous conversation to ask questions, provide feedback, etc.
>
> Synchronous meetings should be reserved for low-latency collaboration on complex issues; likewise, collaboration should be reserved for synchronous meetings. — [*Coda Hale*](https://twitter.com/coda)

As companies scale, people will come and go. By utilizing asynchronous communication, an organization is able to retain knowledge throughout these natural cycles. 

For example, the [Git blame history](https://gitlab.com/gitlab-com/www-gitlab-com/blame/master/source/handbook/values/index.html.md) of GitLab's [Values](/handbook/values/) page shows a complete list of who made what change, and what the context was for each of them. This insight is invaluable, as some contributors no longer work at GitLab. Too, those seeking information on this are able to [find it asynchronously](/company/culture/all-remote/self-service/) — they do not have to bother anyone else, nor do they have to wait for anyone else to wake up or come online. 

## Limitations and challenges

Asynchronous communication has its limits. Although projects are moved forward asynchronously at GitLab, with decisions documented along the way in issues and/or merge requests, there are times when portions of the project are best handled synchronously. 

### Evaluating efficiency 

As a rule, when team members at GitLab go back and forth three times, we look to jump on a synchronous video call (and document outcomes).

### Client-facing roles

Certain roles are more tolerable of asynchronous than others. Client-facing roles, for instance, may have certain requirements for coverage during certain hours. It's possible to layer asynchronous atop these demands by ensuring that there is no [single point of failure](/blog/2015/12/17/gitlab-release-process/), such that a team within an asynchronous organization can self-organize and decide who covers given time slots. 

### Time zones

While communicating asynchronously is an excellent way to reduce the pain of having team members spread across an array of time zones, managing this as a *small* team is particularly challenging. For example, a small team which is primarily based in North America may struggle to communicate well with the first team member who joins from Singapore given the time zone difference. 

However, as a team scales and more coverage is added in time zones in between, it's easier to hand work off as the world turns. In many ways, managing time zones becomes *easier* with scale, as the delta between teams is reduced. 

### Interviewing external candidates

All of GitLab's [interview processes](/handbook/hiring/interviewing/) involve some form of synchronous communication. Some of our teams  utilize [asynchronous practices during the interview process](https://www.youtube.com/watch?v=jSbCt8b_4ug), however, this is not a standard approach across every interview process. 

## Is this advice any good?

![GitLab all-remote team illustration](/images/all-remote/gitlab-com-all-remote-1280x270.png){: .shadow.medium.center}

GitLab is the world's largest all-remote company. We are 100% remote, with no company-owned offices *anywhere* on the planet. We have over 1,200 team members in more than 65 countries. The primary contributor to this article ([Darren Murph](/handbook/marketing/readmes/dmurph/), GitLab's Head of Remote) has over 14 years of experience working in and reporting on colocated companies, [hybrid-remote](/company/culture/all-remote/hybrid-remote/) companies, and all-remote companies of various scale. 

Just as it is valid to [ask if GitLab's product is any good](/is-it-any-good/), we want to be transparent about our expertise in the field of remote work. 

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

---

Return to the main [all-remote page](/company/culture/all-remote/).

