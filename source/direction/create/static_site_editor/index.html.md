---
layout: markdown_page
title: "Category Direction - Static Site Editor"
---

- TOC
{:toc}

## Static Site Editor

|  Stage   |   Maturity  |   Content Last Reviewed   |
|  ---   |   ---   |   ---   |
| [Create](/direction/dev/index.html#create) | [Minimal](/direction/maturity/) | `2020-04-22` |

## Introduction and how you can help

This is the category direction page for the Static Site Editor in GitLab. This page belongs to the [Static Site Editor](/handbook/product/categories/#static-site-editor-group) group of the Create stage and is maintained by Eric Schurter ([E-Mail](mailto:eschurter@gitlab.com)).

## Overview

Creating and editing content on static websites should not require deep knowledge of any particular templating language, markdown syntax, or git branching workflows. The Static Site Editor category is focused on delivering a familiar and intuitive content editing experience that enables efficient collaboration on static sites across all users, on any platform, regardless of their technical experience.

Static websites are a flexible and performant option for many types of sites, but are frequently used to make blogs, documentation portals, and portfolios. These use cases necessitate the ability for content to be edited quickly, frequently, and often away from a desktop computer. For this reason, the goal is to deliver a delightful content editing experience on desktops, tablets, and mobile devices.

### Target Audience

**GitLab Team Members:** The GitLab handbook is a prime example of a static site and every member of the team should be able to actively contribute to it, whether or not they have technical experience. As this category reaches a lovable level of maturity, it will be possible to create and edit content on the GitLab handbook without writing a single line of code.

**UX Writers, Technical Writers, Copywriters:** Authors of website content should be focused on the content itself, not on remembering markdown syntax or specifics around templating markup. These contributors are at best slowed down and, at worst, entirely prevented from doing their job when the technology doesn't adapt to their workflow and technical ability.

**Product Managers, Leadership, Stakeholders:** As companies move to a more distributed way of working, processes and documentation becomes more important. Contributions to this type of content come from every type of user. Stakeholders and members of leadership need a fast, reliable, and accessible way to collaborate with their team.

## Where we are Headed

Contributors want to limit context switching between the site itself and the underlying repository structure, so the editing experience for content hosted on a static site should be accessible and available in the context of the webpage itself.

The editing experience will be intuitive and scalable for those who do not know, or do not wish to write, markdown. To compliment a more "what you see is what you get" (WYSIWYG) editing experience, we'll make it possible to preview the edits live and in real-time on the site using the page's custom styles and layout.

At the end of the journey, publishing content to the site will be improved by abstracting and streamlining a lot of the process related to creating branches, committing changes, and creating merge requests while maintaining the power of a versioned, distributed, git-based backend.


### What's Next & Why

Our current focus is on enabling a single Middleman page to be edited using a visual editor ([Epic !2784](https://gitlab.com/groups/gitlab-org/-/epics/2784)). A critical part of this initial user journey is the ability for a collaborator to initiate editing from the page in a production environment and commit changes in the form of a merge request without using the main GitLab user interface. Contributors will be able to write in a friendly, WYSIWYG Markdown editor ([Epic !2793](https://gitlab.com/groups/gitlab-org/-/epics/2793)). The end result of this effort will be a Middleman project template ([Issue #209317](https://gitlab.com/gitlab-org/gitlab/-/issues/209317)) available to all GitLab users that is pre-configured to use the Static Site Editor and contains documentation on its configuration and usage.

We have identified Middleman as the first static site generator to target so that we can move quickly to integrate it with the GitLab handbook ([Epic !2786](https://gitlab.com/groups/gitlab-org/-/epics/2786)), which is a static site powered by Middleman, and begin gathering feedback and optimizing the experience.

### What is Not Planned Right Now

Right now, we are not planning on investing any time in developing a new framework for generating static sites or forking an existing project to extend its functionality. We are also not choosing a single static site generator on which to build our product. Instead, our goal is to provide a solution that works across many, if not all, of the most popular platforms like [Middleman](https://middlemanapp.com/), [Jekyll](https://jekyllrb.com/), [Hugo](https://gohugo.io/), and [Gatsby](https://www.gatsbyjs.org/).

The Static Site Editor group is not working toward a solution that allows users to create static sites from scratch without using code. The initial setup of a static site will, for now, remain something that requires at least some technical knowledge and configuration.

We are also not planning to build a way to visually edit relational databases, API, or other dynamic content. As the group name suggests, our focus is on bringing a user-friendly interface for editing static content.

While we hope to provide visual formatting tools for a more familiar text editing experience, the underlying text markup language will remain Markdown. We will not support writing content in alternative markup languages like LaTeX, Org-mode, or Asciidoc.

### Maturity Plan

This category is currently at the Minimal maturity level (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

The [Category Strategy epic](https://gitlab.com/groups/gitlab-org/-/epics/2688) is where we're planning the work necessary to reach Viable maturity. This includes the introduction of a rich text WYSIWYG editor, a more flexible approach to making multiple edits across pages, and an initial approach to handling non-Markdown content on a page. We plan on reaching the Viable maturity level by July 22, 2020 when we can successfully integrate it into the GitLab handbook.

## Competitive Landscape

[Netlify CMS](https://www.netlifycms.org/) and [Forestry.io](https://forestry.io/) are both products that directionally inspire our vision for
static site editing.
