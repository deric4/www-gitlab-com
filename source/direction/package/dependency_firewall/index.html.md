---
layout: markdown_page
title: "Category Direction - Dependency Firewall"
---

- TOC
{:toc}

## Dependency Firewall

Many projects depend on packages that may come from unknown or unverified providers, introducing potential security vulnerabilities.  GitLab already provides [dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#supported-languages-and-package-managers) across a variety of languages to alert users of any known security vulnerabilities, but we currently do not allow organizations to prevent those vulnerabilities from being downloaded to begin with.

The goal of this category will be to leverage the [dependency proxy](https://about.gitlab.com/direction/package/dependency_proxy/), which proxies and caches dependencies, to give more control and visibility to security and compliance teams. We will do this by allowing users to create and maintain an approved/banned list of dependencies, providing more insight into the usage and impact of external dependencies and by ensuring the [GitLab Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/) is the single source of truth for all security related issues.

By preventing the introduction of security vulnerabilities further upstream, organizations can let their development teams work faster and more efficiently.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADependency%20Firewall)
- [Overall Vision](/direction/ops/#package)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## Use cases

- Verify package integrity from one single place. See what has been changed and test them for security vulnerabilities (part of black duck model). 
- Filter the available upstream packages to include only approved, whitelisted packages.
- Delay updates from packages that have been recently updated under suspicious circumstances. For example, you will be able to delay any packages in which the following circumstances have occurred:
  - Author change
  - Author information change
  - Programming language change 
  - Activity after a long period of inactivity
  - Large code changes
  
## What's next & why

[gitlab-#215393](https://gitlab.com/gitlab-org/gitlab/-/issues/215393) will flag any npm packages that are pulled through the Dependency Proxy that have recently had the author name or email updated to ensure that users are aware of any suspicious changes.introduce the concept of approved/banned lists for external dependencies. 

## Maturity Plan

This category is currently at the "Planned" maturity level, and
our next maturity target is Minimal (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Approved / Banned List for GitLab Package Registry](https://gitlab.com/gitlab-org/gitlab-ee/issues/13761)
- [Introduce NPM Audit to the GitLab NPM Registry](https://gitlab.com/gitlab-org/gitlab-ee/issues/13754)
- [Dependency graph for NPM packages](https://gitlab.com/gitlab-org/gitlab-ee/issues/13762) (In Progress)
- [Dependency graph for Maven packages](https://gitlab.com/gitlab-org/gitlab-ee/issues/13763)
- [Flag npm packages in which the author email or name have recently changed](https://gitlab.com/gitlab-org/gitlab/-/issues/215393)

## Competitive landscape

* [JFrog X-Ray](https://jfrog.com/xray/)
* [Nexus](https://www.sonatype.com/nexus-firewall)
* [GitHub Package Registry](https://github.com/features/package-registry) 

JFrog utilizes a combination of their Bintray and XRay products, to proxy, cache and screen dependencies. They also provide dependency graphs across multiple languages and centralized dashboards for the review and remediation of vulnerabilities. It is a mature product, that is generally well received by users. 

GitHub's new package registry does a really nice job of creating visibility into the dependency graph for a given package, but they do not give users the ability to control which packages are used in a given group/project. 
​​
## Top Customer Success/Sales issue(s)
This is a new category and does not yet have any customer success or sales issues.

## Top user issue(s)
The top user issue is [gitlab-ce#55374](https://gitlab.com/gitlab-org/gitlab-ce/issues/55374) which aims to capture and make use of the various meta data associated with images and packages. 

## Top internal customer issue(s)
This is a new category and has not yet received any internal customer issues. 

## Top Vision Item(s)

Our top vision item is [gitlab-ee#13761](https://gitlab.com/gitlab-org/gitlab-ee/issues/13761) which will introduce the concept of approved and banned dependencies to Gitlab. 
