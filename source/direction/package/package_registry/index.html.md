---
layout: markdown_page
title: "Category Direction - Package Registry"
---

- TOC
{:toc}

## Package Registry

As part of our overall vision for packaging at GitLab, we want to provide a single interface for managing dependencies, registries, and package repositories. Whether C/C++, Java, Ruby, or any other language, our vision is that you can store your binaries and dependencies in the GitLab Package Registry.

You can view the list of supported and planned formats in our documentation [here](https://docs.gitlab.com/ee/user/packages/).

Our plan is to provide a single, consistent interface to whichever package manager you're using. We want you to be able to take advantage of the great features each of these tools offer, but at the same time, we want there to be a consistent experience that's well integrated with the rest of your workflow at GitLab.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3APackage+Registry)
- [Overall Vision](https://about.gitlab.com/direction/ops/#package)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## What's Next & Why

We have a lot of exciting features currently in progress. Our current plan is to add support for several new package manager formats, redesign the user interface and improve our existing integrations.

First up is [gitlab-#803](https://gitlab.com/gitlab-org/gitlab/-/issues/803), the MVC for the GitLab RubyGems Repository, which will help Ruby developers by allowing them to publish and share dependencies to a private, GitLab-hosted repository.

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is "Complete" (see our [definitions of maturity levels](/direction/maturity/)).

For a list of key deliverables and expected outcomes, check out the epic, [gitlab-#2891](https://gitlab.com/groups/gitlab-org/-/epics/2891), which includes links and expected timing for each issue.

## Competitive Landscape

|         | GitLab | Artifactory | Nexus | GitHub | JetBrains
| ------- | ------ | ----------- | ----- | ------ | ------ |
| Conan    | ✔️ | ✔️ | ☑️ | - | - |
| Debian   | - | ✔️ | ✔️ | - | - |
| Maven    | ✔️ | ✔️ | ✔️ | ️✔️ ️| ✔️ |
| NPM      | ✔️ | ✔️ | ✔️ | ✔️ | - |
| NuGet    | ✔️ | ✔️ | ✔️ | ✔️ | - |
| PyPI     | ✔️ | ✔️ | ✔️ | - | - |
| RPM      | - | ✔️ | ✔️ | - | - |
| RubyGems | - | ✔️ | ✔️ | ✔️ | - |


☑️ _indicates support is through community plugin or beta feature_

## Top Customer Success/Sales Issue(s)

- Our top customer success and sales issues are all focused on adding support for new package manager formats:
  - [gitlab-#803](https://gitlab.com/gitlab-org/gitlab/issues/803): RubyGems
  - [gitlab-#5835](https://gitlab.com/gitlab-org/gitlab/issues/5835): Debian
  - [gitlab-#5932](https://gitlab.com/gitlab-org/gitlab/issues/5932): RPM

## Top Customer Issue(s)

- For Maven and NPM, our top customer issues are [gitlab-#32159](https://gitlab.com/gitlab-org/gitlab/issues/32159) and [gitlab-#33685](https://gitlab.com/gitlab-org/gitlab/issues/33685) which will simplify how we enforce package scopes and naming conventions.


## Top Internal Customer Issue(s)

- The Distribution team utilizes an external tool for building Linux packages. By offering support for Debian and RPM we can begin to remove that external dependency.
- The GitLab Distribution team also utilizes RubyGems for downloading external dependencies. They would like to speed up their build times and remove their reliance on external dependencies by caching frequently used packages. This would require an integration with RubyGems as well as the dependency proxy. [gitlab-#225](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/225) details their needs and requirements.
- The Release group would like to leverage the GitLab Package Registry to store release assets and make them available for discovery and download. Although we are still working through how to best implement this, [gitlab-#36133](https://gitlab.com/gitlab-org/gitlab/issues/36133) discusses the need and use cases.

## Top Vision Item(s)

A lot of moving our vision forward is integrating MVC support for new languages and package types:

### Linux Packages

[gitlab-#5835](https://gitlab.com/gitlab-org/gitlab/issues/5835) and [gitlab-#5932](https://gitlab.com/gitlab-org/gitlab/issues/5932), which relate to adding support for Debian and RPM respectively.

### RubyGems

[gitlab-#803](https://gitlab.com/gitlab-org/gitlab/issues/803) which will add support for a RubyGem registry.
