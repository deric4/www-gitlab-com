- title: 'Gartner Magic Quadrant for Application Security Testing'
  url: /resources/report-gartner-mq-ast
  image: /images/resources/security.png
  type: 'report'
  topics:
    - DevSecOps
    - DevOps
  solutions:
    - Security
    - Quality
  teaser: 'Security and risk management leaders are being driven by modern application design and the rise of DevSecOps.'

- title: 'Modernizing Government IT through DevSecOps'
  url: /resources/whitepaper-modernizing-government-it/
  image: /images/resources/devops.png
  type: 'Whitepaper'
  topics:
    - Public Sector
    - DevSecOps
  solutions:
    - Security and quality
    - Project compliance
  teaser: 'Discover how government agencies can spark sustainable transformation through the same DevOps practices commercial peers are using to impact their organizations.'

- title: 'How to Deploy on AWS from GitLab'
  url: /resources/whitepaper-deploy-aws-gitlab
  image: /images/resources/devops.png
  type: 'Whitepaper'
  topics:
    - Single Application
    - DevOps
    - CI
  solutions:
    - Executive visibility
    - Accelerated delivery
    - Security and quality
  teaser: 'Learn how to deploy a serverless application using GitLab and AWS SAM.'

- title: 'Deploy AWS Lambda applications with ease'
  url: /webcast/aws-gitlab-serverless
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - CI
    - Cloud Native
  solutions:
    - Accelerated delivery
  teaser: 'Learn how the industry’s leading serverless and CI solutions are making serverless possible.'

- title: 'Harnessing development to scale AppSec'
  url: /webcast/scalable-secure-ci
  image: /images/resources/security.png
  type: 'Webcast'
  topics:
    - DevSecOps
    - CI
    - CD
    - Security
  solutions:
    - Accelerated delivery
    - Security and quality
  teaser: 'An inside look at securing your GitLab CI pipeline with WhiteSource'

- title: 'The GitLab Remote Playbook'
  url: /resources/ebook-remote-playbook/
  image: /images/resources/gitlab.png
  type: 'eBook'
  topics:
    - GitLab
  solutions:
    - Executive visibility
  teaser: 'The most comprehensive remote work guide from the largest all-remote company'

- title: 'Mastering continuous software development'
  url: /webcast/mastering-ci-cd
  image: /images/resources/continuous-integration.png
  type: 'Webcast'
  topics:
    - CI
    - CD
    - GitLab
  solutions:
    - Accelerated delivery
  teaser: 'Eliminate toolchain complexity and scale with GitLab’s built-in CI/C'

- title: 'Automating Kubernetes deployments'
  url: /webcast/justcommit-to-agile-K8s-deployments
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - Cloud Native
    - Single application
    - GitLab
  solutions:
    - Accelerated delivery
  teaser: 'Commit to scalability with GitLab and Google Cloud Platform'

- title: '7 GitLab CI Hacks'
  url: /webcast/7cicd-hacks
  image: /images/resources/continuous-integration.png
  type: 'Webcast'
  topics:
    - CI
    - CD
    - GitLab
  solutions:
    - Accelerated delivery
  teaser: '7 GitLab CI advanced workflows that can help your team get to productivity faster'

- title: 'Securing your applications in a Cloud Native world'
  url: /webcast/zerotrust-cloudnative
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - Security
    - Cloud Native
  solutions:
    - Project compliance
    - Security and quality
  teaser: 'Learn how to combat modern application security challenges by leveraging Zero Trust principles'

- title: 'Debunking Serverless security myths'
  url: /webcast/securing-serverless
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - Single Application
    - DevSecOps
    - Cloud Native
  solutions:
    - Accelerated delivery
    - Project compliance
    - Security and quality
  teaser: 'Learn how to bring Dev and Sec teams together to secure your serverless applications'

- title: 'GitLab implementation tips for performance, stability and recovery'
  url: /webcast/gitlab-premium-implementation
  image: /images/resources/gitlab.png
  type: 'Webcast'
  topics:
    - GitLab
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'How GitLab Geo and HA can help improve performance, stability and disaster recovery'

- title: 'GitLab for complex CI/CD: Robust, visible pipeline'
  url: /webcast/gitlab-premium-cicd
  image: /images/resources/continuous-integration.png
  type: 'Webcast'
  topics:
    - CI
    - CD
    - GitLab
  solutions:
    - Accelerated delivery
  teaser: 'Uplevel your CI/CD experience with GitLab Premium'

- title: 'Rich change controls for building workflows you can trust'
  url: /webcast/gitlab-premium-security
  image: /images/resources/security.png
  type: 'Webcast'
  topics:
    - DevSecOps
    - DevOps
    - Security
    - GitLab
  solutions:
    - Accelerated delivery
    - Project compliance
    - Security and quality
  teaser: 'Learn how GitLab Premium enables new protections for mission-critical code'

- title: 'Shifting-left: Rethinking security within DevOps'
  url: /webcast/dev-sec
  image: /images/resources/security.png
  type: 'Webcast'
  topics:
    - DevSecOps
    - DevOps
    - Security
  solutions:
    - Accelerated delivery
    - Project compliance
    - Security and quality
  teaser: 'Learn an alternative workflow between Dev and Sec and the benefits of this approach'

- title: 'Unleashing the power of portable Serverless'
  url: /webcast/portable-serverless
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - Single Application
    - Software development
    - Cloud Native
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'How to achieve operational efficiency gains from Serverless without the vendor lock-in'

- title: 'Agile Transformation: The good, the bad, the ugly, and a plan to make it work'
  url: /webcast/agile-transformation
  image: /images/resources/gitlab.png
  type: 'Webcast'
  topics:
    - Agile
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Agile transformation is a cultural challenge, are you ready?'

- title: 'Making the case for CI/CD in your organization'
  url: /webcast/CICD-in-your-organization
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - CI
    - CD
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Strategies you can use to get CI/CD greenlit by your leadership'

- title: 'Creating and managing critical workloads on Kubernetes clusters (with Portworx)'
  url: /webcast/critical-workloads-k8-clusters
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - Single Application
    - Software development
    - Cloud Native
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Learn how to move your workloads between Kubernetes clusters without manual, complex, and frustrating processes'

- title: 'What you need to know about going Cloud Native'
  url: /resources/video-going-cloud-native
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - Single Application
    - Software development
    - DevOps
    - Cloud Native
    - Toolchain
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Learn the advantages and challenges of becoming Cloud Native'

- title: 'From wishes to workflows: How to truly shift-left'
  url: /webcast/wishes-to-workflows
  image: /images/resources/devops.png
  type: 'Webcast'
  topics:
    - Single Application
    - Software development
    - DevOps
    - Cloud Native
    - Toolchain
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Learn the challenges with shifting left and practical solutions to tackle these challenges'

- title: 'The weeds of Cloud Native'
  url: /webcast/weeds-cloud-native
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - Single Application
    - Software development
    - DevOps
    - Cloud Native
    - Toolchain
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Protips on how to avoid the pitfalls of Cloud Native to fully realize its value'

- title: 'Scalable app deployment with GitLab and Google Cloud Platform'
  url: /webcast/scalable-app-deploy
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - Single Application
    - Software development
    - GitLab
    - Cloud Native
  solutions:
    - Accelerated delivery
  teaser: 'Learn how to deploy applications at scale using GKE and GitLab’s robust Auto DevOps capabilities'

- title: 'Starting and Scaling DevOps: a tactical framework for implementing DevOps principles'
  url: /webcast/starting-scaling-devops
  image: /images/resources/devops.png
  type: 'Webcast'
  topics:
    - DevOps
  solutions:
    - Accelerated delivery
  teaser: 'A tactical framework for implementing DevOps principles'

- title: 'Release Radar: Auto DevOps'
  url: /webcast/auto-devops
  image: /images/resources/devops.png
  type: 'Webcast'
  topics:
    - DevOps
  solutions:
    - Accelerated delivery
  teaser: 'Learn how to go from code to production in just two steps'

- title: 'DevOps Discussion with Gary Gruver'
  url: /webcast/gary-gruver-discussion
  image: /images/resources/devops.png
  type: 'Webcast'
  topics:
    - DevOps
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'An insightful discussion on the details of a DevOps transformation'

- title: 'DevOps: Powering your Speed to Mission '
  url: /webcast/devops-speed-to-mission
  image: /images/resources/devops.png
  type: 'Webcast'
  topics:
    - DevOps
    - Public sector
  solutions:
    - Accelerated delivery
    - Executive visibility
    - Project compliance
  teaser: 'A panel discussion on DevOps in the Public Sector'

- title: 'Keys to DevOps Success'
  url: /webcast/keys-to-devops-success
  image: /images/resources/devops.png
  type: 'Webcast'
  topics:
    - Software development
    - Single Application
    - DevOps
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Learn key findings from The 2018 Accelerate State of DevOps Report'

- title: 'Livestream: What`s next for GitLab?'
  url: /webcast/whats-next-for-gitlab
  image: /images/resources/gitlab.png
  type: 'Webcast'
  topics:
    - Single Application
    - DevOps
  solutions:
    - Accelerated delivery
    - Executive visibility
    - Distributed teams
  teaser: 'Watch a special #GitLabLive event featuring company updates, tips on how to scale a remote culture, and special guests'

- title: 'A Cloud Native Transformation (with Ask Media Group)'
  url: /webcast/cloud-native-transformation
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - CI
    - CD
    - Cloud Native
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'How Ask Media Group modernized their architecture and development with microservices, containers, and Kubernetes'

- title: 'Mission Mobility: A DevSecOps Discussion'
  url: /webcast/mission-mobility
  image: /images/resources/security.png
  type: 'Webcast'
  topics:
    - DevSecOps
    - Software development
    - Single Application
    - Public Sector
  solutions:
    - Accelerated delivery
    - Executive visibility
    - Project compliance
    - Security and quality
  teaser: 'Building and delivering secure mobile apps in a regulated environment'

- title: 'Keys to accelerating software delivery'
  url: /webcast/justcommit-reduce-cycle-time
  image: /images/resources/software-development.png
  type: 'Webcast'
  topics:
    - DevOps
    - Toolchain
    - Single Application
    - Software development
    - Agile
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Informative discussion on how to enable your organization to accelerate delivery'

- title: 'The Hidden Cost of DevOps Toolchains (with Forrester)'
  url: /webcast/simplify-to-accelerate
  image: /images/resources/devops.png
  type: 'Webcast'
  topics:
    - DevOps
    - Toolchain
    - Single Application
    - Software development
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Practical insights on how to overcome the toolchain challenge and get to productivity faster.'

- title: 'The Hidden Cost of DevOps Toolchains (with Forrester)'
  url: /webcast/simplify-to-accelerate
  image: /images/resources/devops.png
  type: 'Webcast'
  topics:
    - DevOps
    - Toolchain
    - Single Application
    - Software development
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Practical insights on how to overcome the toolchain challenge and get to productivity faster.'

- title: '11.0 to 12.0: How we got to DevSecOps in a single application'
  url: /webcast/devsecops-livestream
  image: /images/resources/security.png
  type: 'Webcast'
  topics:
    - DevSecOps
    - Software development
    - Single Application
  solutions:
    - Accelerated delivery
    - Executive visibility
    - Project compliance
    - Security and quality
  teaser: 'Bridging Development, Security and Operations in the first single application for the entire DevSecOps lifecycle.'

- title: 'An Agile iteration with GitLab'
  url: /resources/whitepaper-agile-iteration
  image: /images/resources/gitlab.png
  type: 'Whitepaper'
  topics:
    - Agile
  solutions:
    - Accelerated delivery
    - Executive visibility
    - Project compliance
  teaser: 'Learn how GitLab can seamlessly tie into your Agile methodology'

- title: 'Executive briefing series: digital transformation'
  url: /resources/whitepaper-exec-briefing-series-digital-transformation
  image: /images/resources/gitlab.png
  type: 'Whitepaper'
  topics:
    - Public Sector
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Learn about enterprise development efforts in the federal government'

- title: 'GitLab Developer Report 2019'
  url: /developer-survey
  image: /images/resources/gitlab.png
  type: 'Report'
  topics:
    - DevSecOps
    - DevOps
    - Security
  solutions:
    - Accelerated delivery
    - Security and quality
    - Distributed teams
  teaser: 'Read the 2019 Global Developer Report: DevSecOps'

- title: 'A Guide to the Cloud'
  url: /resources/guide-to-the-cloud
  image: /images/resources/cloud-native.png
  type: 'Report'
  topics:
    - Cloud Native
  solutions:
    - Accelerated delivery
  teaser: 'Insights on how to harness the power of the cloud'

- title: 'GitLab security capabilities demo'
  url: /resources/video-gitlab-security-demo
  image: /images/resources/security.png
  type: 'Webcast'
  topics:
    - Security
    - DevSecOps
  solutions:
    - Security and quality
  teaser: 'Watch this 6-minute demo to learn how GitLab approaches application testing'

- title: 'GitLab security overview demo'
  url: /resources/video-gitlab-security-demo
  image: /images/resources/security.png
  type: 'Webcast'
  topics:
    - Security
    - DevSecOps
  solutions:
    - Security and quality
  teaser: 'Watch this 7-minute demo to learn more about GitLab’s application security testing capabilities'

- title: '10 Steps every CISOs should take to secure next-gen software'
  url: /resources/ebook-ciso-secure-software
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - Security
    - DevSecOps
  solutions:
    - Security and quality
  teaser: 'How to bring your security team and programs to the speed of software development'

- title: 'Agile delivery: a guide to finding the right model'
  url: /resources/ebook-agile-delivery-models
  image: /images/resources/gitlab.png
  type: 'eBook'
  topics:
    - Agile
  solutions:
    - Accelerated delivery
    - Executive visibility
    - Project compliance
  teaser: 'The five core models of Agile delivery, helping you determine the best approach for your team'
  
- title: 'Gary Gruver eBook: Starting and Scaling DevOps in the Enterprise'
  url: /resources/scaling-enterprise-devops
  image: /images/resources/devops.png
  type: 'eBook'
  topics:
    - DevOps
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Pioneering insights on how organizations can transform software development and delivery processes'

- title: 'The benefits of single application CI/CD'
  url: /resources/ebook-single-app-cicd
  image: /images/resources/continuous-integration.png
  type: 'eBook'
  topics:
    - CI
    - CD
  solutions:
    - Accelerated delivery
  teaser: 'Building better, faster software without the toolchain'

- title: 'Modernize your CI/CD'
  url: /resources/ebook-fuel-growth-cicd
  image: /images/resources/continuous-integration.png
  type: 'eBook'
  topics:
    - CI
    - CD
  solutions:
    - Accelerated delivery
  teaser: 'How modernizing your CI/CD can give you a competitive advantage'

- title: 'Agile Project Management with GitLab'
  url: /resources/demo-agile-delivery
  image: /images/resources/gitlab.png
  type: 'Webcast'
  topics:
    - Agile
  solutions:
    - Accelerated delivery
    - Project compliance
    - Executive visibility
  teaser: 'Learn how to use Gitlabs Project management capabilities to build out your agile teams'

- title: 'DevSecOps Methodology Assessment'
  url: /resources/devsecops-methodology-assessment
  image: /images/resources/security.png
  type: 'Assessment'
  topics:
    - DevSecOps
  solutions:
    - Security and quality
  teaser: '20 Capabilities needed for DevSecOps success'

- title: '10 Steps Every CISO Should Take to Secure Next-Gen Software'
  url: /resources/ebook-ciso-secure-software/
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - DevSecOps
  solutions:
    - Security and quality
  teaser: 'Learn the three shifts of next-gen software and how they impact security.'
  
- title: 'Manage your toolchain before it manages you'
  url: /resources/whitepaper-forrester-manage-your-toolchain/
  image: /images/resources/gitlab.png
  type: 'Report'
  topics:
    - Software development
    - DevOps
    - Toolchain
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'Reduce toolchain complexity for improved software delivery and quality.'

- title: 'Reduce cycle time to deliver value'
  url: /resources/whitepaper-reduce-cycle-time/
  image: /images/resources/gitlab.png
  type: 'Whitepaper'
  topics:
    - CI
    - CD
    - Software development
    - Git
    - GitLab
    - DevOps
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'Learn how to stay competitive in a rapidly changing technological landscape.'

- title: 'Avoiding the DevOps tax'
  url: /webcast/avoid-devops-tax/
  image: /images/resources/devops.png
  type: 'Webcast'
  topics:
    - DevOps
    - Software development
  solutions:
    - Accelerated delivery
  teaser: 'Watch now to simplify your toolchain and realize a faster DevOps lifecycle.'

- title: 'Planning for success'
  url: /resources/whitepaper-planning-for-success/
  image: /images/resources/security.png
  type: 'Whitepaper'
  topics:
    - CI
    - CD
    - Software development
    - Git
    - GitLab
    - DevOps
    - Agile software development
  solutions:
    - Accelerated delivery
    - Distributed teams
    - Executive visibility
  teaser: 'Learn how to quickly iterate and ship code in the DevOps lifecycle.'

- title: 'Gartner Magic Quadrant for ARO'
  url: /resources/gartner-aro/
  image: /images/resources/security.png
  type: 'Report'
  topics:
    - CI
    - CD
    - Software development
    - Git
    - GitLab
    - DevOps
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'Download the Gartner Magic Quadrant for Application Release Orchestration (ARO).'

- title: 'Speed to Mission: the strategic value of velocity'
  url: /resources/whitepaper-speed-to-mission/
  image: /images/resources/gitlab.png
  type: 'Report'
  topics:
    - DevOps
    - Software development
    - GitLab
    - Agile software development
    - Public sector
  solutions:
    - Accelerated delivery
  teaser: 'discover how to accelerate software innovation and meet your mission challenge.'

- title: 'A seismic shift in application security'
  url: /resources/whitepaper-seismic-shift-application-security/
  image: /images/resources/security.png
  type: 'Whitepaper'
  topics:
    - Security
    - DevSecOps
  solutions:
    - Security and quality
    - Distributed teams
  teaser: 'Learn how to integrate and automate security in the DevOps lifecycle.'

- title: 'Bridging the divide between developers and management'
  url: /resources/whitepaper-bridging-the-divide/
  image: /images/resources/gitlab.png
  type: 'Whitepaper'
  topics:
    - GitLab
    - Git
    - Software development
    - DevOps
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Explore methods to align perceptions, expectations, and outcomes.'

- title: 'Concurrent DevOps'
  url: /resources/whitepaper-concurrent-devops/
  image: /images/resources/devops.png
  type: 'Whitepaper'
  topics:
    - GitLab
    - Software development
    - DevOps
  solutions:
    - Accelerated delivery
  teaser: 'Find out how Concurrent DevOps can help you fully realize the benefits of digital transformation.'

- title: 'Scaled continuous integration and delivery (CI/CD)'
  url: /resources/whitepaper-scaled-ci-cd/
  image: /images/resources/continuous-integration.png
  type: 'Whitepaper'
  topics:
    - CI
    - CD
    - DevOps
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Deliver what customers want, faster, with an integrated CI/CD tool.'

- title: 'Moving to Git'
  url: /resources/whitepaper-moving-to-git/
  image: /images/resources/git.png
  type: 'Whitepaper'
  topics:
    - Git
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Learn how to implement Git and take the first steps towards a more collaborative and efficient software development lifecycle.'

- title: 'What you need to know about going Cloud Native'
  url: /resources/video-going-cloud-native/
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - Cloud native
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'Find out how going Cloud-Native can make your business more flexible, reduce costs, increase collaboration, and improve security.'

- title: 2018 Global developer report
  url: /developer-survey/2018
  image: /images/resources/devops.png
  type: 'Report'
  topics:
    - Software development
    - DevOps
  teaser: 'Explore the findings of our global developer report from 2018.'

- title: 'GitLab capabilities statement'
  url: /images/press/gitlab-capabilities-statement.pdf
  image: /images/resources/gitlab.png
  type: 'One-pager'
  topics:
    - GitLab
    - Public sector
  solutions:
    - Distributed teams
    - Accelerated delivery
  teaser: 'Read the capabilities of GitLab as a single solution for DevOps.'

- title: 'GitLab datasheet'
  url: /images/press/gitlab-data-sheet.pdf
  image: /images/resources/gitlab.png
  type: 'One-pager'
  topics:
    - GitLab
    - DevOps
    - Single application
  solutions:
    - Project compliance
  teaser: 'Read how GitLab is a complete DevOps platform, delivered as a single application.'
  

